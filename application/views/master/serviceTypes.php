<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Service Type Master</h5>
                                                 </div>
                                                <div class="card-block"> 
                                                <form action="<?php echo base_url(); ?>index.php/saveserviceTypes" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
                                                <input id="service_type_id" name="service_type_id"  value="<?php if(!empty($serviceTypes)) { echo $serviceTypes[0]['serviceTypeID']; } ?>"  type="hidden">
                                                    <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Service Type Name <span class="required">*</span></label>
                                                                <div class="col-sm-6">
                                                                <input id="service_type" class="form-control" name="service_type" required  type="text" value="<?php if(!empty($serviceTypes)) { echo $serviceTypes[0]['serviceType']; } ?>">
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Service Type Code  <span class="required">*</span></label>
                                                                <div class="col-sm-6">
                                                                <input id="service_type_code" class="form-control"  name="service_type_code" required  type="text"  value="<?php if(!empty($serviceTypes)) { echo $serviceTypes[0]['serviceTypeCode']; } ?>">
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-3"></label>
                                                                <div class="col-sm-6">
                                                                <?php if(!empty($serviceTypes)) 
                                                                {
								                                	?>
										<button class="btn btn-sm btn-primary m-b-0" type="submit" name="update_type_btn" value="update_type_btn">Update</button> 
									<?php } else { ?>
								 <button class="btn btn-sm btn-primary m-b-0" type="submit" name="add_type_btn" value="add_type_btn">Submit
									</button>
									 <?php } ?>
                                                                </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div> 
                                                    <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Service Type Details</h5>
                                                 </div>
                                                <div class="card-block">  
                                                  
                                                    <div class="dt-responsive table-responsive">
                                                            <table id="dom-table" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Service Type</th> 
                                            <th> Code</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cnt = 1;
									for ($i = 0; $i < count($serviceTypeList);$i++) {
									 
									?>
									
									<tr class="odd gradeX">
									<td><?php echo $cnt++; ?></td>
                                        <td><?php echo $serviceTypeList[$i]->serviceType;   ?></td> 
                                        <td><?php echo $serviceTypeList[$i]->serviceTypeCode;   ?></td> 
                                        
                                      
                                        <td> <div class="btn-group btn-group-sm" style="float: none;">
                                         <a href="<?php echo base_url('index.php/editserviceTypes/' . $serviceTypeList[$i]->serviceTypeID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
									 
                                         <a <?php  if($serviceTypeList[$i]->cnt==1) { echo "disabled"; } ?> onclick="delete_data('<?php echo $serviceTypeList[$i]->serviceTypeID; ?>');" class="tabledit-delete-button btn btn-danger waves-effect waves-light <?php  if($serviceTypeList[$i]->cnt==1) { echo "btn-disabled"; } ?>" > <span class="icofont icofont-ui-delete"></span></a>
									
									</div>
                                    	</td>
										
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
						</div>
						</div>
					</div>			

				  </div>
                  </div>
           </div> 
          </div>
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 
 <script>
    $(function() {
        $("#block-validate").validate({
             rules: {
                service_type: {
                    required: true
                } 
            },
            //For custom messages
            messages: {
                service_type: {
					required: "Enter a service Type Name"
                } 
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            }
        });
    });
    </script>
    <script>
    function delete_data(deleteid){ 
					bootbox.confirm("Are you sure,You want to Delete", function (msg) {
					 if (msg == true) {
							location='<?php echo base_url('index.php/deleteserviceTypes/'); ?>'+deleteid;
						}
						
					}); 
					
				 
	}
    </script>