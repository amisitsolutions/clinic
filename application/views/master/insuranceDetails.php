<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Insurance Details</h5>
                                                 </div>
													
                                                <div class="card-block"> 
                          <form action="<?php echo base_url(); ?>index.php/saveinsurance" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
							<input id="insurance_id" name="insurance_id"  value="<?php if(!empty($edit_insuranceData)) { echo $edit_insuranceData[0]['insuranceID']; } ?>"  type="hidden">
									
                                                    <div class="col-md-6">
                                                    <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Insurance Name  <span class="required">*</span></label>
                                                                <div class="col-sm-8">
                                                                   <input id="insurance_name" name="insurance_name" class="form-control" required  type="text" value="<?php if(!empty($edit_insuranceData)) { echo $edit_insuranceData[0]['insuranceName']; } ?>" >
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            </div>
															 <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Insurance Provider  <span class="required">*</span></label>
                                                                <div class="col-sm-8">
													<input id="insurance_provider" name="insurance_provider" class="form-control"  required  type="text" value="<?php if(!empty($edit_insuranceData)) { echo $edit_insuranceData[0]['insuranceProvider']; } ?>" >
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div>
													<table border="1" class="table table-striped table-bordered nowrap dataTable no-footer ser" style="width:100%;">
											
													<thead>
														<tr><th>
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="colored-blue check_all">
																	<span class="text"></span>
																</label>
															</div>
														</th>	
														<th  style="width:40px;"> Sr. No.
															</br>
														</th>
														<th style="width:100px;">Service Type</th>
														<th style="width:100px;">Service Name</th>
														<th  style="width:100px;">Charges</th>
														</tr>
													</thead>
													<tbody> 
														
														<?php  
															 if(!empty($edit_insuranceData))
															{
																$edit_insuranceData[0]['typeID'];
																$type=explode(",",$edit_insuranceData[0]['typeID']); 
																
																$service_id=explode(",",$edit_insuranceData[0]['serviceID']); 
																
																for ($i = 0; $i <count($type); $i++)
																{ 
																	$charge=explode(",",$edit_insuranceData[0]['serviceCharges']);
																	  
																?>
																
																<tr>
																	<td style="width: 1% !important;">
																		
																		<input type="hidden" class="form-control no_of_products"  name="no_of_products[]" id="no_of_products0"  value="0"/>
																		<div class="checkbox">
																		 <label>
																			 <input type="checkbox" class="colored-blue case chk">
																				
																				<span class="text"></span>
																				
																			</label>
																			
																		</div>
																		
																	</td>
																	<td >1</td>
																	<td><select name="type_id[]"  id="type_id0" onchange="get_services(this.value,'0');">
																		<option value="" selected disabled>Select Service Type </option>
																		<?php
																			for ($n = 0; $n < count($serviceTypeList); ++$n) {
																			?>
																			<option  <?php if(in_array($serviceTypeList[$n]->serviceTypeID,$type))  { echo "selected"; } ?> value="<?php echo $serviceTypeList[$n]->serviceTypeID; ?>"><?php echo $serviceTypeList[$i]->serviceType; ?></option>
																		<?php } ?>	
																		
																	</select></td>
																	
																	<td><select name="service_name[]"  id="service_name0"  >
																		<option value="" disabled >Select Service Name </option>
																		<?php
																			if(!empty($edit_insuranceData))
																			{
																				for ($k = 0; $k < count($servicesList); $k++) {
																				?>
																				<option  <?php if(in_array($servicesList[$k]->serviceID,$service_id))  { echo "selected"; } ?> value="<?php echo $servicesList[$k]->serviceID; ?>"><?php echo $servicesList[$k]->serviceName; ?></option>
																			<?php } } ?>	
																	</select></td>
																	<td><input  onkeypress="return isNumber(event);"  id="service_charges" name="service_charges[]" required  type="text" value="<?php if(!empty($edit_insuranceData)) { echo $charge[$i]; } ?>" ></td>
																</tr>	
																<?php } 
																
															} else { ?>
															<tr>
																	<td style="width: 1% !important;">
																		
																		<input type="hidden" class="form-control no_of_products"  name="no_of_products[]" id="no_of_products0"  value="0"/>
																		<div class="checkbox">
																		 <label>
																			 <input type="checkbox" class="colored-blue case chk">
																				
																				<span class="text"></span>
																				
																			</label>
																			
																		</div>
																		
																	</td>
																	<td >1</td>
																	<td><select name="type_id[]"  id="type_id0" onchange="get_services(this.value,'0');">
																		<option value="" selected disabled>Select Service Type </option>
																		<?php
																			for ($n = 0; $n < count($serviceTypeList); ++$n) {
																			?>
																			<option  value="<?php echo $serviceTypeList[$n]->serviceTypeID; ?>"><?php echo $serviceTypeList[$n]->serviceType; ?></option>
																		<?php } ?>	
																		
																	</select></td>
																	
																	<td><select name="service_name[]"  id="service_name0"  >
																		<option value="" disabled >Select Service Name </option>
																		<?php
																			if(!empty($edit_insuranceData))
																			{
																				for ($k = 0; $k < count($servicesList); $k++) {
																				?>
																				<option   value="<?php echo $servicesList[$k]->serviceID; ?>"><?php echo $servicesList[$k]->serviceName; ?></option>
																			<?php } } ?>	
																	</select></td>
																	<td><input  onkeypress="return isNumber(event);"  id="service_charges" name="service_charges[]" required  type="text" value="" ></td>
																</tr>	
																				<?php } ?>
													</tbody>
													<tfoot>
													<tr>
													<td colspan="5">	
													<a  class='addmore label label-sm label-info'>+ Add More</a>
													<a  class='delete label label-sm label-danger'>-Delete</a>
														</td>
													
													</tr>
													</tfoot>
												</table>		
												
											</div>  
								
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Remarks</label>
                                                                <div class="col-sm-6">
													<textarea id="remarks" name="remarks"  class="form-control"  class="materialize-textarea"  ><?php if(!empty($edit_insuranceData)) { echo $edit_insuranceData[0]['remark']; } ?></textarea>
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-2"></label>
                                                                <div class="col-sm-6">
                                                                   <?php if(!empty($edit_insuranceData)) {
																	?>
																	<button class="btn btn-info" type="submit" name="update_btn" value="update_btn">Update</button> 
																	<?php } else { ?>
																	<button class="btn btn-info" type="submit" name="add_btn" value="add_btn">Submit
																	</button>
																	<?php } ?>
																	
                                                                </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div> 
                                                    <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Insurance Details</h5>
                                                 </div>
                                                <div class="card-block">  
                                                  
                                                    <div class="dt-responsive table-responsive">
                                                    <table id="dom-table" class="table table-striped table-bordered nowrap">
                        
                                                    <thead>
									<tr>
										
										<th>Sr.No.</th> 
										<th> Insurance Name</th> 
										<th> Insurance Provider</th>  
										<th> Service Types</th>   
										<th> Services</th>   
										<th> Service Charges</th>   
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$cnt = 1;  
										for ($k = 0; $k < count($InsuranceList);$k++) {
 
											$service_charges=explode(",",$InsuranceList[$k]->serviceCharges);
										?>
										
										<tr class="odd gradeX">
											<td><?php echo $cnt++; ?></td> 
											<td><?php echo $InsuranceList[$k]->insuranceName;   ?></td> 
											<td><?php echo $InsuranceList[$k]->insuranceProvider;   ?></td>     
											<td><?php  
												
												$serviceBranches=explode(",",$InsuranceList[$k]->typeID);
												for ($p = 0; $p< count($serviceBranches);$p++) 
												{	 
													$result = $model_obj->editserviceTypes($serviceBranches[$p]);
												 
													foreach($result as $key=>$row){ 
														echo $row['serviceType']; 	 
													}
													echo "</br>";
												
												}  
												
												?>  </td>
                                            
                                            <td><?php  
												  $InsuranceList[$k]->serviceID;
												$services=explode(",",$InsuranceList[$k]->serviceID);
												for ($pp = 0; $pp< count($services);$pp++) 
												{	 
													$result1 = $model_obj->editservices($serviceBranches[$pp]);
												 
													foreach($result1 as $key1=>$row1){ 
														echo $row1['serviceName']; 	 
													}
													echo "</br>";
												
												}  
												
												?></td>   
											<td><?php for ($j = 0; $j < count($service_charges);$j++) { echo  $service_charges[$j]; echo ","; }  ?></td>   
											<td>
											<div class="btn-group btn-group-sm" style="float: none;">
                                        
												<a href="<?php echo base_url('index.php/editInsurance/'.$InsuranceList[$k]->insuranceID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
												<a 	 onclick="delete_data('<?php  echo $InsuranceList[$k]->insuranceID; ?>');"   class="tabledit-delete-button btn btn-danger waves-effect waves-light  " > <span class="icofont icofont-ui-delete"></span></a>  
											  </div>
											</td>
											
										</tr>
									<?php } ?>
                                    </tbody>
                                </table>
						</div>
						</div>
					</div>			

				  </div>
                  </div>
           </div> 
          </div>
          </div>
          </div>
          </div>
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 
<script type="text/javascript">
	$(document).ready(function() {	
		
		 
		var i=2;  
		$(".addmore").on('click',function(){				
			var data='<tr><td style="width: 1% !important;"> <input type="hidden" class="form-control no_of_products"  name="no_of_products[]" id="no_of_products0"  value="0"/>';
					 data +='<div class="checkbox">  <label>';
						  data +='<input type="checkbox" class="colored-blue case chk"> <span class="text"></span>';
					 data +='</label> </div> </td><td>'+i+'</td><td class="form-group">';
			data +='<select name="type_id[]"  id="type_id'+i+'" onchange="get_services(this.value,'+i+');"> <option value=""  selected disabled>Select Service Type </option> <?php for ($i = 0; $i < count($serviceTypeList); $i++) { 	?> <option   value="<?php echo $serviceTypeList[$i]->serviceTypeID; ?>"><?php echo $serviceTypeList[$i]->serviceType; ?></option> <?php } ?>';
			data += '</select> </td>';
			data +='<td class="form-group"> <select name="service_name[]"  id="service_name'+i+'"  ><option value=""  selected disabled >Select Service Name </option>';  
			data +='</select>';
			data +='</td> <td class="form-group"><input type="text" name="service_charges[]" id="service_charges'+i+'" class="rate" onblur="cal_amount('+i+');" value="" >';
			data +='</td> </tr>';
			 
			$('.ser').append(data);
		 
			i++;	
		});
		$(".delete").on('click', function() {
			$('.case:checkbox:checked').parents("tr").remove();
			$('.check_all').prop('checked',false);
		});
		$(".check_all").on('click', function() {				
			if($('.check_all').is(":checked")==false){
				$('.chk').prop('checked',false);
				}else{
				$('.chk').prop('checked',true); 
			}
		});
		$(".chk").on('click', function() {				
			if($('.chk').is(":checked")==true){
				$('.check_all').prop('checked',false);
			}
		}); 
	}); 
	
</script>	
<script>
    $(function() {
        $("#block-validate").validate({
			rules: {
                service_type: {
                    required: true
				} 
			},
            //For custom messages
            messages: {
                service_type: {
					required: "Enter a service Type "
				} 
			},
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
					} else {
                    error.insertAfter(element);
				}
			},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
				}
			}
		});
	});
</script>
<script>
	function get_services(type_id,pval)
	{
		var base_url='<?php echo base_url(); ?>';
		//var type_id=$('#type_id'+pval).val();
		console.log(type_id);
		$.ajax({
			url: base_url+'index.php/getservices/',
			type: 'post',
			data: {type_id:type_id},
			success: function(result){  
				console.log(result);
				
				$('#service_name'+pval).html(result); 
			}
			
		});
	}
	/* function get_services_charges(service_id,pval)
	{
		var base_url='<?php echo base_url(); ?>'; 
		console.log(service_id);
		$.ajax({
			url: base_url+'index.php/getservicescharges/',
			type: 'post',
			data: {service_id:service_id},
			success: function(result){  
				console.log(result);
				var obj = $.parseJSON(result); 
				$.each(obj,function(key,entry){ 
					$('#rate'+pval).html(entry.consultant_fee); 
				});
			}
			
			
		});
	} */
</script>
