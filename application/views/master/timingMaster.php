<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12"> 
							<div class="card">
								<div class="card-header">
									<h5>Timing Details</h5>
								</div>
								<div class="card-block"> 
									<form action="<?php echo base_url(); ?>index.php/saveTiming" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
										<input id="master_timingval" name="master_timingval"  value="master"  type="hidden">
										
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Doctor Name <span  class="required"> * </span></label>
											<div class="col-sm-6">
												<select name="doc" id="doc"   class="form-control"  > 
													<option value="">Select Doctor</option>
													<?php
														for ($k = 0; $k < count($doctorData); $k++) {
														?>
														<option <?php if(!empty($edit_timingdata)) {  if($edit_timingdata[0]['doctorID']==$doctorData[$k]->doctorID)
														{ echo "selected"; } }  ?> value="<?php echo $doctorData[$k]->doctorID; ?>"><?php echo $doctorData[$k]->doctorName; ?></option>
													<?php } ?>
												</select> 
												<span class="messages"></span>
											</div>
										</div>
										
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Days <span  class="required"> * </span></label>
											<div class="col-sm-6">
												<select type="text" name="day" class="form-control" id="days0"  >
													<option value="">Select Days</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Monday') { echo "selected"; } } ?> value="Monday"  >Monday</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Tuesday') { echo "selected"; } } ?> value="Tuesday"  >Tuesday</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Wednesday') { echo "selected"; } } ?> value="Wednesday"  >Wednesday</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Thursday') { echo "selected"; } } ?> value="Thursday"  >Thursday</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Friday') { echo "selected"; } } ?> value="Friday"  >Friday</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Saturday') { echo "selected"; } } ?> value="Saturday" >Saturday</option>
													<option <?php if(!empty($edit_timingdata)) { if($edit_timingdata[0]['timingDay']=='Sunday') { echo "selected"; } } ?> value="Sunday"  >Sunday</option>
												</select>
												<span class="messages"></span>
											</div>
										</div>
									 	<div class="form-group row">
											<label class="col-sm-2 col-form-label">Schdule <span  class="required"> * </span></label>
											<div class="col-sm-6">
												<div class="clone-rightside-btn-1">
												<?php
											 if(!empty($edit_timingdata)) 
											 {
												for ($kk = 0; $kk < count($edit_timingdata); $kk++)
												{
											 
												?>
													<table class="table table-bordered table-hover table-striped ser" > 
												<thead class="bordered-darkorange">
													<tr> 
														<th>
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="colored-blue check_all">
																	<span class="text"></span>
																</label>
															</div>
														</th>
														<th>From Time</th>
														<th>To Time</th>									
													</tr>
												</thead>
												<?php
											 if(!empty($edit_timingdata)) 
											 {
												for ($kk = 0; $kk < count($edit_timingdata); $kk++)
												{
											 
												?>
												
												<tr>
													<td><div class='checkbox'><label> 
														<input id="timing_id" name="timing_id[]"  value="<?php if(!empty($edit_timingdata)) { echo $edit_timingdata[$kk]['timingID']; } ?>"  type="hidden">
														<input type='checkbox' class='colored-blue case chk'> <span class='text'>	</span>  </label></div></td>
													<td class="form-group">
														<input type="text" name="from_time[]" class="form-control timePicker" id="from_time0"  value="<?php echo $edit_timingdata[$kk]['fromTime']; ?>">
													</td>
													<td class="form-group">
														<input type="text" name="to_time[]" id="to_time0" class="form-control timePicker" value="<?php echo $edit_timingdata[$kk]['toTime']; ?>" >
													</td>
												</tr>
											 <?php } 
											 }?>
											</table>									
										
														
														<button type="button" class='addmore label label-sm label-info'>+ Add More</button>
											<button type="button" class='delete label label-sm label-danger'>- Delete</button>	
												 

												<?php } 
											 }
											 else{
												?>
											 	<table class="table table-bordered table-hover table-striped ser" > 
												<thead class="bordered-darkorange">
													<tr> 
														<th>
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="colored-blue check_all">
																	<span class="text"></span>
																</label>
															</div>
														</th>
														<th>From Time</th>
														<th>To Time</th>									
													</tr>
												</thead>
												<?php
											 if(!empty($edit_timingdata)) 
											 {
												for ($kk = 0; $kk < count($edit_timingdata); $kk++)
												{
											 
												?>
												
												<tr>
													<td><div class='checkbox'><label> 
														<input id="timing_id" name="timing_id[]"  value="<?php if(!empty($edit_timingdata)) { echo $edit_timingdata[$kk]['timing_id']; } ?>"  type="hidden">
														<input type='checkbox' class='colored-blue case chk'> <span class='text'>	</span>  </label></div></td>
													<td class="form-group">
														<input type="text" name="from_time[]" class="form-control timePicker" id="from_time0"  value="<?php echo $edit_timingdata[$kk]['from_time']; ?>">
													</td>
													<td class="form-group">
														<input type="text" name="to_time[]" id="to_time0" class="form-control timePicker" value="<?php echo $edit_timingdata[$kk]['to_time']; ?>" >
													</td>
												</tr>
											 <?php } 
											 }?>
											</table>									
										
														<button type="button" class='addmore label label-sm label-info'>+ Add More</button>
											<button type="button" class='delete label label-sm label-danger'>- Delete</button>	
													</div>
<?php
											 }
												?>
										 
												
												<span class="messages"></span>
											</div>
										</div>
										
										
										<div class="row">
											<label class="col-sm-2"></label>
											<div class="col-sm-6">
												<?php if(!empty($edit_timingdata)) {
												?>
												<button type="submit" name="update" id="update"    	 class="btn btn-primary m-b-0" value="Update"  />Update</button>
												<?php } else { ?>
											<button type="submit" name="add_time" id="add_time"    	 class="btn btn-primary m-b-0" value="Submit"  />Submit</button>
										<?php } ?>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div> 
				<div class="col-sm-12"> 
					<div class="card">
						<div class="card-header">
							<h5>Timing Details</h5>
						</div>
						<div class="card-block">  
							
							<div class="dt-responsive table-responsive">
								<table id="dom-table" class="table table-striped table-bordered nowrap">
                                    <thead>
										<thead>
											<tr>
												<th>Sr.No.</th>
												<th>Available Day</th>
												<th>Available Time</th> 
												<th style="width: 341px !important;">Schdule</th> 
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$cnt = 1;
												for ($i = 0; $i < count($timing_Data);$i++) {
												 
															//$newarr=$timing_Data[$i]->daysData;
															//$n=1;
														/*	for ($k = 0; $k < count($newarr);$k++) 
															{
															?>
															<b>Shift <?php echo $n++; ?>:</b> <?php echo $newarr[$k]->fromTime." - ". $newarr[$k]->toTime;  
																echo "</br>";
																
															}*/ 
 
												?>
												
												<tr class="odd gradeX">
													<td><?php echo $cnt++; ?></td>
													<td><?php echo $timing_Data[$i]->doctorName;   ?></td>
													<td><?php echo $timing_Data[$i]->timingDay;   ?></td>
													<td> 
														<b>Shift  </b> <?php echo  $timing_Data[$i]->fromTime." - ".  $timing_Data[$i]->toTime;  ?>
														 
													</td>
													
													
													<td><div class="btn-group btn-group-sm" style="float: none;">
														<a href="<?php echo base_url('index.php/editTiming/'. $timing_Data[$i]->doctorID.'/'.$timing_Data[$i]->timingDay); ?>"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
														
														<a onclick="delete_data('<?php echo $timing_Data[$i]->timingID; ?>');" class="tabledit-delete-button btn btn-danger waves-effect waves-light" > <span class="icofont icofont-ui-delete"></span></a>
													</div>
													
													</td>
													
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>			
						
					</div>
				</div>
			</div> 
		</div>
		</div>
		</div>
		
		<?php
			$this->load->view("admin_includes/footer");
		?>				
	</div>  

<script type="text/javascript">
	$(document).ready(function() {	
		$('.timePicker').timepicker({
    timeFormat: 'h:i A',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
		 
	var i=1; 
	
	$(".addmore").on('click',function(){				
		var data="<tr><td><div class='checkbox'><label>	<input id='timing_id' name='timing_id[]'  value=''  type='hidden'> <input type='checkbox' class='colored-blue case chk'> <span class='text'></span>  </label></div></td>";
		
		data +='<td class="form-group"><input type="text" name="from_time[]" class="form-control timePicker" id="from_time'+i+'" value=" "></td>';
		data +='<td class="form-group"><input type="text" name="to_time[]" id="tos_time'+i+'" class="form-control timePicker" value="" ></td></tr>';
		//	ddatefor();
		$('.ser').append(data);
		$('.timePicker').timepicker({
    timeFormat: 'h:i A',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
		i++;	
	});
	$(".delete").on('click', function() {
		$('.case:checkbox:checked').parents("tr").remove();
		$('.check_all').prop('checked',false);
	});
	$(".check_all").on('click', function() {				
		if($('.check_all').is(":checked")==false){
			$('.chk').prop('checked',false);
			}else{
			$('.chk').prop('checked',true); 
		}
	});
	$(".chk").on('click', function() {				
		if($('.chk').is(":checked")==true){
			$('.check_all').prop('checked',false);
		}
	}); 
 
	/* function get_speciality(sp_id)
	{ 
		var base_url='<?php echo base_url(); ?>';
	  
			$.ajax({
				url: base_url+"index.php/getDoctorsByspecSess/",
				type: "POST",
				data: {  sp_id:sp_id },
				success: function(res)
				{
			 	console.log(res);
					$('#doctor_id').html(res);
					   $('select').not('.disabled').formSelect();
				}	 
				});
                 
		} */
});
</script>	
	<script>
		$(function() {
			
	
			$("#block-validate").validate({
				rules: {
					doc: {
						required: true
					},
					day: {
						required: true
					},
				},
				//For custom messages
				messages: {
					day: {
						required: "Select Day"
					},
					doc: {
						required: "Enter a Specialist Doctor Name"
					}
				},
				errorElement: 'div',
				errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
						$(placement).append(error)
						} else {
						error.insertAfter(element);
					}
				},
				invalidHandler: function(e, validator) {
					var errors = validator.numberOfInvalids();
					if (errors) {
						$('.error-alert-bar').show();
					}
				}
			});
		});
	</script>	