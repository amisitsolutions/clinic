<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Prefix Master</h5>
                                                 </div>
                                                <div class="card-block"> 
												<form action="<?php echo base_url(); ?>index.php/savePrefix" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" > <input id="service_type_id" name="service_type_id"  value="<?php if(!empty($serviceTypes)) { echo $serviceTypes[0]['serviceTypeID']; } ?>"  type="hidden">
                                                    <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Clinic Type  <span class="required">*</span></label>
                                                                <div class="col-sm-6">
																<select name="clinic_type"  class="form-control"  onchange="check_type(this.value);">
											<option  value="Centralised">Centralised</option>
											<option value="Branches">Branches</option>
											</select>      <span class="messages"></span>
                                                                </div>
															</div>
                                                            <div class="chkbranch_panel" style="display:none;">
															<div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Branch  <span class="required">*</span></label>
                                                                <div class="col-sm-6">
																<select id="branch_id" class="form-control"  name="branch_id" type="text" onchange="getbranchcode(this.value);"> 
											<option value="">Select Branch</option>
								<?php
								for ($i = 0; $i < count($branchList); $i++) {
								?>
									<option data-target="<?php echo $branchList[$i]->branchCode; ?>" value="<?php echo $branchList[$i]->branchID; ?>"><?php echo $branchList[$i]->branchName; ?></option>
								<?php } ?>	    
								</select>  <span class="messages"></span>
                                                                </div>
                                                                </div>
                                                             
                                                                <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Branch Code <span class="required">*</span></label>
                                                                <div class="col-sm-6">
																<input id="prefix2"  name="branch_code"   class="form-control"  class="form-control"  type="text">
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
															</div>
															
										
                                                            <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Prefix </label>
                                                                <div class="col-sm-6">
																<input id="prefix1" name="prefix1"  class="form-control"  type="text">
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
														
															<div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Year <span class="required">*</span></label>
                                                                <div class="col-sm-6">
																<?PHP
										 if (date('m') <= 3) {
																		 
												  $year = date('y') -1;
													$bill_year = $year .'-'.(date('y'));
											}		
											else
											{
												$bill_year = date('y').'-'.(date('y')+1);
											}
											 
										?>
											<input id="year" name="year" VALUE="<?PHP echo $bill_year;?>"  class="form-control"   type="text" >
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
															<div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Start From <span class="required">*</span></label>
                                                                <div class="col-sm-6">
																<input id="start_from" name="start_from"  class="form-control"  required  type="text">
											<p>Ex.1000</p>
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-3"></label>
                                                                <div class="col-sm-6">
                                                                <?php if(!empty($serviceTypes)) 
                                                                {
								                                	?>
										<button class="btn btn-sm btn-primary m-b-0" type="submit" name="update_pre_btn" value="update_pre_btn">Update</button> 
									<?php } else { ?>
								 <button class="btn btn-sm btn-primary m-b-0" type="submit" name="add_pre_btn" value="add_pre_btn">Submit
									</button>
									 <?php } ?>
                                                                </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div> 
                                                    <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Prefix Details</h5>
                                                 </div>
                                                <div class="card-block">  
                                                  
                                                    <div class="dt-responsive table-responsive">
                                                            <table id="dom-table" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
											<th>Clinic Type</th> 
                                            <th>Prefix</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cnt = 1;
									for ($i = 0; $i < count($prefixData);$i++) {
									 
										?> 
										<tr class="odd gradeX">
										<td><?php echo $cnt++; ?></td>
											<td><?php echo $prefixData[$i]->prefixType;   ?></td> 
											<td><?php
                                            if($prefixData[$i]->branchID==0)
                                            {
                                                     if($prefixData[$i]->prefix1!='' && $prefixData[$i]->yearWise!='')
													{
											 
														$prename=$prefixData[$i]->prefix1."/".$prefixData[$i]->yearWise."/".$prefixData[$i]->startFrom;
													}
													else if($prefixData[$i]->prefix1==''){
																$prename=$prefixData[$i]->yearWise."/".$prefixData[$i]->startFrom;
													} 
													else if($prefixData[$i]->prefix1=='' && $prefixData[$i]->yearWise==''){
																$prename= $prefixData[$i]->startFrom;
													}
                                            }
                                            else{
                                                if($prefixData[$i]->prefix1!='' && $prefixData[$i]->yearWise!='')
                                                {
                                         
                                                    $prename=$prefixData[$i]->branchCode."/".$prefixData[$i]->prefix1."/".$prefixData[$i]->yearWise."/".$prefixData[$i]->startFrom;
                                                }
                                                else if($prefixData[$i]->prefix1==''){
                                                            $prename=$prefixData[$i]->branchCode."/".$prefixData[$i]->yearWise."/".$prefixData[$i]->startFrom;
                                                } 
                                                else if($prefixData[$i]->prefix1=='' && $prefixData[$i]->yearWise==''){
                                                            $prename= $prefixData[$i]->branchCode."/".$prefixData[$i]->startFrom;
                                                }

                                            }
													
														echo $prename;
												
												?></td> 
										
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
						</div>
						</div>
					</div>			

				  </div>
                  </div>
           </div> 
          </div>
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 
 <script>
    $(function() {
        $("#block-validate").validate({
             rules: {
                service_type: {
                    required: true
                } 
            },
            //For custom messages
            messages: {
                service_type: {
					required: "Enter a service Type Name"
                } 
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            }
        });
    });
	</script>
	<script>
		function check_type(id){			
					if(id=='Centralised')
					{
						$('.chkbranch_panel').hide();
						$('#prefix2').val('');
					}
					else{
						$('.chkbranch_panel').show();
					}
             
		 }	
		 function getbranchcode(chkval){			
           var v= $('#branch_id  option:selected').data('target');
   
			$('#prefix2').val(v); 
		 }		
		</script>