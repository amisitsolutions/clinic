<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Department Details</h5>
                                                 </div>
                                                <div class="card-block"> 
                                                    <form action="<?php echo base_url(); ?>index.php/saveRoleRegistration" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
                                                    <input id="role_id" name="role_id"  value="<?php if(!empty($edit_data)) { echo $edit_data[0]['roleID']; } ?>"  type="hidden">
				
									
                                                    <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Department Name <span class="required">*</span></label>
                                                                <div class="col-sm-6">
                                                                <input id="role_name" name="role_name" class="form-control"  value="<?php if(!empty($edit_data)) { echo $edit_data[0]['roleName'];  } ?>"  required  type="text">
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-2"></label>
                                                                <div class="col-sm-6">
                                                                <?php 
									if(!empty($edit_data))
									{
									?>
									<button class="btn btn-primary m-b-0" type="submit" name="update_role_btn" value="update_role_btn">Update
									</button>
									<?php }else { ?>
									<button class="btn btn-primary m-b-0" type="submit" name="add_role_btn" value="add_role_btn">Submit
									</button>
									<?php } ?>
	 
                                                                </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div> 
                                                    <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Department Details</h5>
                                                 </div>
                                                <div class="card-block">  
                                                  
                                                    <div class="dt-responsive table-responsive">
                                                            <table id="dom-table" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Department</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php 
									$cnt = 1;
									for ($i = 0; $i < count($roleList);$i++) {
									 
									?>
									
									<tr class="odd gradeX">
									<td><?php echo $cnt++; ?></td>
                                        <td><?php echo $roleList[$i]->roleName;   ?></td> 
                                         <td>
                                        <div class="btn-group btn-group-sm" style="float: none;">
                                         <a href="<?php echo base_url('index.php/editRole/'.$roleList[$i]->roleID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
								        	
										 <a <?php  if($roleList[$i]->cnt==1) { echo "disabled"; } ?> onclick="delete_data('<?php echo $roleList[$i]->roleID; ?>');" class="tabledit-delete-button btn btn-danger waves-effect waves-light <?php  if($roleList[$i]->cnt==1) { echo "btn-disabled"; } ?>" > <span class="icofont icofont-ui-delete"></span></a>
													
											 
										</div>
										</td>
										
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
						</div>
						</div>
					</div>			

				  </div>
                  </div>
           </div> 
          </div>
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 
 <script>
    $(function() {
        $("#block-validate").validate({
             rules: {
                role_name: {
                    required: true
                } 
            },
            //For custom messages
            messages: {
                role_name: {
					required: "Enter a Department Name"
                } 
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            }
        });
    });
    </script>

    <script>
    function delete_data(deleteid){ 
					bootbox.confirm("Are you sure,You want to Delete", function (msg) {
					 if (msg == true) {
							location='<?php echo base_url('index.php/deleteRole/'); ?>'+deleteid;
						}
						
					}); 
					
				 
	}
    </script>