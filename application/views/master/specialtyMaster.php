<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
    $this->load->view("admin_includes/sidebar");
 
?>
	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Speciality Master</h5>
                                                 </div>
                                                <div class="card-block">
                                                    <form action="<?php echo base_url(); ?>index.php/saveSpecialityRegistration" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
                                                    <input type="hidden" name="spec_id" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['specialityID']; } ?>" >
				
                                                    <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Speciality Name <span class="required">*</span></label>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control" name="speciality_name" id="speciality_name" placeholder="Enter speciality Name"  value="<?php if(!empty($edit_data)) { echo $edit_data[0]['specialityName']; } ?>" >
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-2"></label>
                                                                <div class="col-sm-6">
                                                                <?php if(!empty($edit_data)) 
                                                                {
								                                	?>
										<button class="btn btn-sm btn-primary m-b-0" type="submit" name="update_spec_btn" value="update_spec_btn">Update</button> 
									<?php } else { ?>
								 <button class="btn btn-sm btn-primary m-b-0" type="submit" name="add_spec_btn" value="add_spec_btn">Save
									</button>
									 <?php } ?>
                                                                  </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div> 
                                                    <div class="col-sm-12"> 
                                                        
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Speciality Details</h5>
                                                 </div>
                                                <div class="card-block">  
                                                  
                                                    <div class="dt-responsive table-responsive">
                                                            <table id="dom-table" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Specialty</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cnt = 1;
									for ($i = 0; $i < count($specalist);$i++) {
									 
									?>
									
									<tr class="odd gradeX">
									<td><?php echo $cnt++; ?></td>
                                        <td><?php echo $specalist[$i]->specialityName;   ?></td> 
                                        <td>

                                        <div class="btn-group btn-group-sm" style="float: none;">
                                          <a href="<?php echo base_url('index.php/editSpeciality/'. $specalist[$i]->specialityID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
									 
										<a <?php  if($specalist[$i]->cnt==1) { echo "disabled"; } ?> onclick="delete_data('<?php echo $specalist[$i]->specialityID; ?>');" class="tabledit-delete-button btn btn-danger waves-effect waves-light <?php  if($specalist[$i]->cnt==1) { echo "btn-disabled"; } ?>" > <span class="icofont icofont-ui-delete"></span></a>
										</td>
										
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
						</div>
						</div>
					</div>			

				  </div>
                  </div>
           </div> 
          </div>
    
<?php
//	$this->load->view("admin_includes/footer");
?>		
	 <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery\js\jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\popper.js\js\popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\bootstrap\js\bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\modernizr\js\modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\modernizr\js\css-scrollbars.js"></script>  <script src="..\files\assets\js\pcoded.min.js"></script>
    <script src="..\files\assets\js\vartical-layout.min.js"></script>
    <script src="..\files\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="..\files\assets\js\script.js"></script>

	<script src="<?php echo base_url(); ?>\files\assets\js\jquery.validate.min.js"></script>

<link href="<?php echo base_url(); ?>\files\assets\js\datepicker.css" rel="stylesheet" type="text/css" />  
<script src="<?php echo base_url(); ?>\files\assets\js\bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\js\jquery.timepicker.js"></script> 
 		
</div> 
 <script>
    $(function() {
        $("#block-validate").validate({
            rules: {
                speciality_name: {
                    required: true
                },
                 specialist_dr: {
                    required: true
                },
            },
            //For custom messages
            messages: {
                speciality_name: {
					required: "Enter a Speciality Name"
                },
                 specialist_dr: {
					required: "Enter a Specialist Doctor Name"
                }
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            }
        });
    });
    </script>
    
    <script>
    function delete_data(deleteid){ 
					bootbox.confirm("Are you sure,You want to Delete", function (msg) {
					 if (msg == true) {
							location='<?php echo base_url('index.php/deleteSpeciality/'); ?>'+deleteid;
						}
						
					}); 
					
				 
	}
    </script>