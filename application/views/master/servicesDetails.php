<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Service Details</h5>
                                                 </div>
                                                <div class="card-block"> 
                                                    <form action="<?php echo base_url(); ?>index.php/saveservices" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
                                                    <input id="service_id" name="service_id"  value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceID']; } ?>"  type="hidden">
													  <div class="col-sm-6"> 
                                                    <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Service Code <span class="required">*</span></label>
                                                                <div class="col-sm-8">
                                                                <input id="service_code" class="form-control" name="service_code" required  type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceCode']; } ?>"  >      <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Service Name<span class="required">*</span></label>
                                                                <div class="col-sm-8">
                                                                <input id="service_name"  class="form-control"  name="service_name" required  type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceName']; } ?>" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Service Type<span class="required">*</span></label>
                                                                <div class="col-sm-8">
                                                                <select name="type_id"  class="form-control"  required  id="type_id" onchange="get_custom_fields(this.value);">
														<option value="" disabled selected >Select Service Type </option>
														<?php
															for ($i = 0; $i < count($serviceTypeList); ++$i) {
															?>
															<option <?php if(!empty($editserviceData)) { if($editserviceData[0]['serviceTypeID']==$serviceTypeList[$i]->serviceTypeID) { echo "selected"; } } ?> value="<?php echo $serviceTypeList[$i]->serviceTypeID; ?>"><?php echo $serviceTypeList[$i]->serviceType; ?></option>
														<?php } ?>	
														
														
													</select>
													</div>
													</div>
                                                  
												  
												  <div id="lab_panelCls" style="<?php if(!empty($editserviceData)) { if($editserviceData[0]['serviceTypeID']==1)  { echo "display:block;"; }   }  else { echo  "display:none;"; } ?>">
												  
												   <div class="form-group row">
													<label class="col-sm-4 col-form-label">Unit: </label>
														 <div class="col-sm-8"><input id="unit" name="unit"   class="form-control"    type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceUnit']; } ?>" >
														
													</div>
												</div>
												</div>
											 
												
												   <div class="form-group row">
													<label class="col-sm-4 col-form-label">Multiply: </label>
													   <div class="col-sm-8">
														<input id="multiply" name="multiply"  class="form-control"  onkeypress="return isNumber(event);"   type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceMultiply']; } ?>" >
												
													</div>
												</div>
												  
													  <div class="form-group row"> <label class="col-sm-4 col-form-label">Male Range: </label>
													 <div class="col-sm-8">	<input id="male_range" name="male_range"  class="form-control"    onkeypress="return isNumber(event);"   type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceMaleRange']; } ?>" >
														
													</div>
												</div>	  
													<div class="form-group row">
													<label class="col-sm-4 col-form-label">Female Range: </label>
													 <div class="col-sm-8">	
													 
													 <input id="female_range"  class="form-control"  onkeypress="return isNumber(event);"  name="female_range"    type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceFemaleRange']; } ?>" >
														 
													</div>
												</div> 
													   <div class="form-group row"> <label class="col-sm-4 col-form-label">Kid Range: </label>
														  <div class="col-sm-8"><input id="kid_range"  class="form-control"  onkeypress="return isNumber(event);"  name="kid_range"    type="text" value="<?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceKidRange']; } ?>" >
														
													</div>
												</div>
											</div> 
											
											<div class="form-group row">
													<label class="col-sm-3 col-form-label">Service charges for all branch </label>
															
						<div class="col-md-9">
											<table border="1" class="table table-striped  ser table-bordered nowrap dataTable no-footer" style="width:100%;">
												 <thead>
											<tr>
												<th>Branch</th>
												<th>Charges</th>
											</tr>
											</thead>
											<tbody>
											<?php
										 
												for ($i = 0; $i < count($branch_list); ++$i)
												{
													if(!empty($editserviceData)) 
												{ 
													$charge=explode(",",$editserviceData[0]['branchCharges']);
													}
													?>
													<tr>
													
												<th><input id="branches<?php  echo $branch_list[$i]->branchID ?>" name="branches[]" required  type="hidden" value="<?php echo $branch_list[$i]->branchID ?>" ><?php echo $branch_list[$i]->branchName; ?></th>
												
												<th><input  onkeypress="return isNumber(event);"  id="branch_charges" name="branch_charges[]" required  type="text" value="<?php if(!empty($editserviceData)) { echo $charge[$i]; } ?>" ></th>
											</tr>			
															<?php } ?>
											 </tbody>
											</table>		
												 
											</div>  
											</div> 
												  <div class="form-group row">
												  <label class="col-sm-3 col-form-label">Description <span  class="required"> * </span></label>
													   <div class="col-sm-9">	<textarea id="description"  name="description" required class="form-control"  ><?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceDesc']; } ?></textarea>
											
												</div>
												</div>
												
											
												<div class="form-group row"><label class="col-sm-3 col-form-label">Notes: </label>
												   <div class="col-sm-9">	
												   <textarea id="notes" name="notes"    class="form-control" ><?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceNote']; } ?></textarea>
													
												</div>
												</div>
												  <div class="form-group row">
												  <label class="col-sm-3 col-form-label">Remarks:</label>
										   <div class="col-sm-9">
												
													<textarea id="remarks" name="remarks"   class="form-control"  ><?php if(!empty($editserviceData)) { echo $editserviceData[0]['serviceRemark']; } ?></textarea>
													
												</div>
												</div>
											</div> 
										
										 <div class="form-group row"><label class="col-sm-3 col-form-label"></label>
										   <div class="col-sm-9">
												<?php if(!empty($editserviceData)) {
												?>
												<button class="btn btn-primary m-b-0" type="submit" name="update_ser_btn" value="update_ser_btn">Update</button> 
												<?php } else { ?>
												<button class="btn btn-primary m-b-0" type="submit" name="add_ser_btn" value="add_ser_btn">Submit
												</button>
												<?php } ?>
                                                    </form>
</div>
</div> 
</div> 
                                                    
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Speciality Details</h5>
                                                 </div>
                                                <div class="card-block">  
                                                  
                                                    <div class="dt-responsive table-responsive">
                                                            <table id="dom-table" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>Sr.No.</th> 
                                            <th> Service Type</th> 
                                            <th> Service Name</th> 
                                            <th> Service Code</th>   
                                            <th> Branches</th>   
                                            <th> Branch-wise Charges</th>   
                                            <th>Action</th>
										</tr>
									</thead>
                                    <tbody>
										<?php
											$cnt = 1; 
											for ($k = 0; $k < count($servicesList);$k++) {
												$branchcharges=explode(",",$servicesList[$k]->branchCharges);
												
											?>
											
											<tr class="odd gradeX">
												<td><?php echo $cnt++; ?></td> 
												<td><?php echo $servicesList[$k]->serviceType;   ?></td> 
												<td><?php echo $servicesList[$k]->serviceName;   ?></td> 
												<td><?php echo $servicesList[$k]->serviceCode;   ?></td>   
												<td><?php
												
												$serviceBranches=explode(",",$servicesList[$k]->serviceBranches);
												for ($p = 0; $p< count($serviceBranches);$p++) 
												{	 
													$result = $model_obj->getbranchesById($serviceBranches[$p]);
												 
													foreach($result as $key=>$row){ 
														echo $row['branchName']; 	 
													}
													echo "</br>";
												
												}  
												
												?></td>   
												<td><?php for ($j = 0; $j < count($branchcharges);$j++) { echo  $branchcharges[$j]; echo ",";}  ?></td>   
											 	<td>
												 <div class="btn-group btn-group-sm" style="float: none;"><a href="<?php echo base_url('index.php/editservices/'.$servicesList[$k]->serviceID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
													 	<a 	 onclick="delete_data('<?php echo $servicesList[$k]->serviceID; ?>');"  class="tabledit-delete-button btn btn-danger waves-effect waves-light  " > <span class="icofont icofont-ui-delete"></span></a> 
													</div> 
													
												</td>
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
						</div>
						</div>
					</div>			

				  </div>
                  </div>
           </div> 
          </div>
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 
<script>
    $(function() {
        $("#block-validate").validate({
			rules: {
                service_type: {
                    required: true
				} 
			},
            //For custom messages
            messages: {
                service_type: {
					required: "Enter a service Type "
				} 
			},
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
					} else {
                    error.insertAfter(element);
				}
			},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
				}
			}
		});
	});
</script>
<script>
	function delete_data(catid){	
		
			location='<?php echo base_url('index.php/deleteservices/'); ?>'+catid;
             
		 }
	function get_custom_fields(typeval)
	{
		if(typeval=='1')
		{
			$('#lab_panelCls').show();
		}
		else{
			$('#lab_panelCls').hide();
		}
	}
</script>

</script>