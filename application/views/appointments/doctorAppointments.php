<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				<!-- Page-header start -->
				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							<div class="page-header-title">
								<div class="d-inline">
									<h4>Todays Appointments Details</h4>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- Page-header end -->
				
				<!-- Page body start -->
				<div class="page-body invoice-list-page">
					<div class="row">
						
						<div class="col-md-12 card filter-bar"> 
							
							<nav class="navbar-light bg-faded p-10">
								<ul class="nav navbar-nav">
									<li class="nav-item active">
										
										<a class="nav-link"><b>Date</b>: </a>
									</li>
									<li class="nav-item dropdown">
										<form autocomplete="off" action="<?php echo base_url(); ?>index.php/getdoctorAppointments"  class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
											<div class="input-group">
												<input type="text" class="form-control datepicker" name="start_date" id="start_date" value="<?php  echo date("d-m-Y");  ?>" >
												<select name="doc" id="doc"   class="form-control"  > 
													<option value="">Select Doctor</option>
													<?php
														if(!empty($doctorData)) {
															for ($k = 0; $k < count($doctorData); $k++) {
															?>
															<option <?php if(!empty($appoint_DocData)) {  if($appoint_DocData[0]->doctorID==$doctorData[$k]->doctorID)
															{ echo "selected"; } }  ?> value="<?php echo $doctorData[$k]->doctorID; ?>"><?php echo $doctorData[$k]->doctorName; ?></option>
															<?php }
														} ?>
												</select> 
												<button type="submit" name="btn-search" class="input-group-addon" id="basic-addon1"><i class="icofont icofont-search"></i>
												</button></div>
										</form>
									</li>
									
								</ul>
								<div class="nav-item nav-grid">
									<span><b>Doctor : <b><?php  if(!empty($appoint_DocData)) { echo $appoint_DocData[0]->doctorName;  } ?></span>
										
									</div>
									<!-- end of by priority dropdown -->
									
									</nav>
								</div>	
								
								<!-- Navigation end  -->
								<div class="row">
									<?php if(!empty($appoint_DocData)) 
										{
											$cnt = 1;	
											$cn = 1;	
											foreach ($appoint_DocData as $key => $data) {
												$time1 = strtotime($data->fromTime);
												$time2 = strtotime($data->toTime);
												$difference = round(abs($time2 - $time1) / 3600); 
												$minutes = ($difference * 60)/ $data->doctorPatientTime; 
												foreach ($appoint_patientData as $key => $kdata) { 
													
													$st='';
													if($kdata->patientType=='New')
													{
														//$st='<img src="'.base_url().'assets/images/new.png" height="50px;">';
														$color="card-border-primary";
														$headercolor="text-primary";	 
													} else  if($kdata->patientType=='Existing')
													{
														//	$st='<img src="'.base_url().'assets/images/existing.jpg" height="50px;">';
														$color="card-border-warning";	 
														$headercolor="text-warning";	 
													}
													if($kdata->appoStatusFlag==2)
													{
														$color="card-border-danger";	 
														$headercolor="text-danger";	 
													} 
													
												?> 
												<div class="col-sm-6">
													<div class="card widget-card-1 <?php echo $color; ?> "><i class="feather icon-user bg-simple-c-blue card1-icon"></i>
														<div class="card-header">
															<h5 style="font-size: 16px;" class="text <?php echo $headercolor; ?>"><?php echo ucfirst($kdata->fullName); ?>
																-<small><i>
																<b class="text-semibold"><?php if($kdata->age!='-1') { ?>  <?php echo $kdata->age;  } ?> / <?php echo $kdata->gender; ?>    </b> </i> </small>
																
															</h5> 
															<div class="dropdown-secondary dropdown f-right">
												<a  onclick="finish_meeting(<?php echo $kdata->appoID; ?>);"   class="label label-info "  > Finish Meeting</a>  
															</div>
														</div> 
														<div class="card-block">
															<div class="row">
																<div class="col-sm-6">
																	<ul class="list list-unstyled">
																		<!-- <li>Patient Type:<b> &nbsp;<?php echo $kdata->patientType; ?></b></li> -->
																		<li>Registration No..: <b> <span class="text-semibold"><?php if(!$kdata->mrdNO='NULL' || !$kdata->mrdNO='') { ?> ( <?php  echo  $prefix[0]['prefix1']."".$prefix[0]['yearWise']."". $prefix[0]['startFrom']; ?> / <?php echo $kdata->mrdNO; ?> ) <?php } ?></b></span></li>
																		<li><i class="icofont icofont-ui-email"></i>   <b>  <span class="text-semibold"><a href="@mailto:<?php echo $kdata->emailID; ?>" ><u><?php echo $kdata->emailID; ?></u></a> </b></span></li>
																		<li>  <i class="icofont icofont-iphone"></i> <b>  <span class="text-semibold"> <a href="<?php echo base_url(); ?>index.php/newRegistration/<?php echo $kdata->appoID; ?>" > <u><?php echo $kdata->contactNO; ?></u> </a></b></span></li>
																	</ul>
																</div>
																<div class="col-sm-6">
																	<ul class="list list-unstyled text-right">
																		<li><i class="icofont icofont-ui-clock"></i> <b>(<?php echo $kdata->appoTime; ?>)</b></li>
																		
																	</ul>
																</div>
															</div>
															
														</div>
														<div class="card-footer">
															<div class="task-list-table">
																<p class="task-due">  
																	<a class="btn btn-warning  btn-sm "    <?php  if($kdata->meeting_link!='') { echo "disabled1"; } ?> data-toggle="modal" data-target="#modal_status_popup" <?php  if($kdata->meeting_link!='') { ?>  onclick="set_status('prepone',<?php echo $kdata->appoID; ?>);" <?php } ?> ><i class="icofont icofont-attachment"  ></i>Schdule</a>
																	<?php if($kdata->patientType=='New')
																		{ ?>
																		<a class="btn btn-inverse  btn-sm"  href="<?php echo base_url(); ?>index.php/newRegistration/<?php echo $kdata->appoID; ?>" ><i class="icofont icofont-attachment"  ></i>Register</a><?php 
																		} else  
																		{ ?>
																		<a class="btn btn-warning  btn-sm"  href="<?php echo base_url(); ?>index.php/addPrescription" ><i class="icofont icofont-attachment"  ></i>Prescription</a><?php 
																		} ?>
																</p>
															</div>
															<div class="task-board m-0">
																<a  <?php if($kdata->meeting_link=='')  { echo "disabled"; } ?>  class="btn btn-primary btn-sm <?php  if($kdata->meeting_link=='') { echo "btn-disabled"; } ?>"   <?php  if($kdata->meeting_link=='') { ?>  target="_blank" href="<?php echo base_url(); ?>index.php/joinMeeting/<?php echo $kdata->meeting_link; ?>" <?php } ?>><i class="icofont icofont-refresh"></i>Join Meeting</a> 
																
															</div>
															<!-- end of pull-right class -->
														</div>
														
													</div>
												</div>
											<?php }   }
										} ?>
								</div>
							</div>
						</div>
					</div>
					<!-- Page body end -->
				</div>		
			</div>
		</div>
		
		
		
		<?php
			$this->load->view("admin_includes/footer");
		?>				
	</div> 
	
	
	
	
	<script type="text/javascript">
		$(document).ready(function() {	
			get_new_order_report();
			$('.datepicker').datepicker({format: 'dd-mm-yyyy'}).on('changeDate',  function (ev) {	
				$(this).datepicker('hide');
			});		
			
			$('.schduledatepicker').datepicker({format: 'dd-mm-yyyy',startDate:new Date() }).on('changeDate',  function (ev) {	
				$(this).datepicker('hide');
			});	
			
		});
	</script>
	
	
	<script type="text/javascript">	
		
		
		function set_status(pval,app_id)
		{
			$('.schduledatepicker').datepicker({format: 'dd-mm-yyyy',startDate:new Date() }).on('changeDate',  function (ev) {	
				$(this).datepicker('hide');
			});	
			$('.timePicker').timepicker({
				timeFormat: 'h:i A',
				dynamic: false,
				dropdown: true,
				scrollbar: true
			});
			$('#reschdule_val').val(pval); 
			$('#app_id').val(app_id);
			if(pval=='cancel')
			{ 
				$('#app_date').removeAttr("required");
				$('#app_time').removeAttr("required");
			}
			else{
				$('#app_date').attr("required"); 
				$('#app_time').attr("required"); 
			}
			
		}
	</script>
	
    <script>
    function finish_meeting(deleteid){ 
					bootbox.confirm("Are you sure,You want to Delete", function (msg) {
					 if (msg == true) {
							location='<?php echo base_url('index.php/finish_meeting/'); ?>'+deleteid;
						}
						
					}); 
					
				 
	}
    </script>
	
	<div class="modal" id="modal_status_popup" >
		<div class="modal-dialog" role="document" style="max-width: 45%;">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Reschdule Appointment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body"> 
					<form action="<?php echo base_url(); ?>index.php/saveOnlineAppointment" autocomplete="off"  class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
						<div class="modal-body"  STYLE="height:100%">
							<div class="form-body">	
								
								<input   name="reschdule_val" id="reschdule_val"  type="hidden" >
								<input   name="app_id" id="app_id"  type="hidden" >
								<div class="divider"></div>
								<div class="card-content"> 
									<div class="row ">
										
										<div id="schdule_panel">
											<div class="col-md-6">				
												<div class="form-group">				
													<label for="bio">Date:  <span  class="required"> * </span>	</label>
													<input name="app_date" class="form-control schduledatepicker" required id="app_date" placeholder="Appointmente Date" type="text"    >											
													
												</div>
											</div>		
											<div class="col-md-6">				
												<div class="form-group">				
													<label for="bio">Time:<span  class="required"> * </span>  	</label>	
													
													<select class="form-control" placeholder="" name="app_time" id="app_time" value="">
														<option value="12.00 AM">12.00 AM</option>
														<option value="12.30 AM">12.30 AM</option>
														<option value="01.00 AM">01.00 AM</option>
														<option value="01:30 AM">01.30 AM</option>
														<option value="02:00 AM">02.00 AM</option>
														<option value="02:30 AM">02.30 AM</option>
														<option value="03:00 AM">03.00 AM</option>
														<option value="03:30 AM">03.30 AM</option>
														<option value="04:00 AM">04.00 AM</option>
														<option value="04:30 AM">04.30 AM</option>
														<option value="05:00 AM">05.00 AM</option>
														<option value="05:30 AM">05.30 AM</option>
														<option value="06:00 AM">06.00 AM</option>
														<option value="06:30 AM">06.30 AM</option>
														<option value="07:00 AM">07.00 AM</option>
														<option value="07:30 AM">07.30 AM</option>
														<option value="08:00 AM">08.00 AM</option>
														<option value="08:30 AM">08.30 AM</option>
														<option value="09:00 AM">09.00 AM</option>
														<option value="09:30 AM">09.30 AM</option>
														<option value="10:00 AM">10.00 AM</option>
														<option value="10:30 AM">10.30 AM</option>
														<option value="11:00 AM">11.00 AM</option>
														<option value="11:30 AM">11.30 AM</option>
														<option value="12:00 PM">12.00 PM</option>
														<option value="12:30 PM">12.30 PM</option>
														<option value="01:00 PM">01.00 PM</option>
														<option value="01:30 PM">01.30 PM</option>
														<option value="02:00 PM">02.00 PM</option>
														<option value="02:30 PM">02.30 PM</option>
														<option value="03:00 PM">03.00 PM</option>
														<option value="03:30 PM">03.30 PM</option>
														<option value="04:00 PM">04.00 PM</option>
														<option value="04:30 PM">04.30 PM</option>
														<option value="05:00 PM">05.00 PM</option>
														<option value="05:30 PM">05.30 PM</option>
														<option value="06:00 PM">06.00 PM</option>
														<option value="06:30 PM">06.30 PM</option>
														<option value="07:00 PM">07.00 PM</option>
														<option value="07:30 PM">07.30 PM</option>
														<option value="08:00 PM">08.00 PM</option>
														<option value="08:30 PM">08.30 PM</option>
														<option value="09:00 PM">09.00 PM</option>
														<option value="09:30 PM">09.30 PM</option>
														<option value="10:00 PM">10.00 PM</option>
														<option value="10:30 PM">10.30 PM</option>
														<option value="11.30 PM">11.30 PM</option>
														<option value="11.30 PM">11.30 PM</option>
													</select>	
													
												</div>	
												<p id="error_text" class="error"></p>
												<p id="succ_text" class="text-success"></p>
											</div>	 	
										</div>	 	
										
									</div>
									
									
									
								</div>
								
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary mobtn" data-dismiss="modal">Close</button>
							<button type="submit" name="add_app_btn" class="modal-action modal-close  btn btn-primary">Save changes</button>
						</div>
						
					</form> 
				</div>
			</div> 
		</div> 
	</div> 
