<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
	
?>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12"> 
							<div class="card">
								<div class="card-header">
									<h5>Appointment Details
									</h5><a href="<?php echo base_url(); ?>index.php/doctorAppointments" class="btn btn-primary btn-sm pull-right">
								 <i class="material-icons"></i>Online Consults List</a> 
								</div>
								<div class="card-block"> 
									<form  class="form-horizontal"  autocomplete="off" id="block-validate" method="POST" enctype="multipart/form-data" >	
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Date</label>
											<div class="col-sm-6">
												<input type="text" class="form-control datepicker" name="start_date" id="start_date" onchange="get_new_order_report();" value="<?php  echo date("d-m-Y");  ?>" >
												<span class="messages"></span>
											</div>
										</div>
										
										
										
									</form>
									<div class="col s7">
										<b>
											<span class="label label-info">
												N
											</span>&nbsp;&nbsp;: &nbsp;New </b>		&nbsp; &nbsp;	<b>
												<span class="label label-warning">
													E
												</span>&nbsp;&nbsp;: &nbsp; Existing</b>
												&nbsp; &nbsp; 						
												<b>
													<span class="label label-success">
														D
													</span>&nbsp;&nbsp;: &nbsp; Done</b>
													&nbsp;  &nbsp; 		
													<b><span class="label label-danger">
														C
													</span>&nbsp;&nbsp;: &nbsp; Cancelled</b>
													&nbsp; &nbsp; 		
													<b><span class="label label-success">
														A
													</span>&nbsp;&nbsp;: &nbsp; Available</b>
									</div>	
								</div>
								
							</div>
							
							
							
							<div class="row">
								<div class="col-sm-12"> 
									<div class="card">
										
										<div id="order_report"></div>
										
									</div>							
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div> 
	
	
	<?php
		$this->load->view("admin_includes/footer");
	?>				
</div><!-- ./wrapper -->	


<script type="text/javascript">
	$(document).ready(function() {	
		get_new_order_report();
		$('.datepicker').datepicker({format: 'dd-mm-yyyy'}).on('changeDate',  function (ev) {	
			$(this).datepicker('hide');
		});		
		
		$('.schduledatepicker').datepicker({format: 'dd-mm-yyyy',startDate:new Date() }).on('changeDate',  function (ev) {	
			$(this).datepicker('hide');
		});	
		
		$("input[type='radio']"). click(function(){
			var radioValue = $("input[name='reschdule']:checked"). val();
			if(radioValue=='reschdule'){
				$('#schdule_panel').show();
			}
			else if(radioValue=='cancel'){
				$('#schdule_panel').hide();
			}
		});		
	});
</script>


<script type="text/javascript">	
	
	function get_new_order_report()
	{	
		var start_date=$('#start_date').val();   
		
		var base_url='<?php echo base_url(); ?>';
		$.ajax({
			url: base_url+'index.php/get_PatientData/',
			type: 'post',
			data: {start_date:start_date},
			success: function(result){ 
			 
				$('#order_report').html(result);			
				
			}
		});
	}
	
	function checkavailability()
	{ 
		var doctor_id=$('#doctor_id').val(); 
		var app_date=$('#app_date').val(); 
		var app_time=$('#app_time').val(); 
		var base_url='<?php echo base_url(); ?>'; 
		console.log(doctor_id+""+app_date+""+app_time);
		$.ajax({
			url: base_url+"index.php/AppointmentAvailability/",
			type: "POST",
			data: { doctor_id:doctor_id, app_date:app_date, app_time:app_time },
			success: function(result)
			{ 
				if(result==0)
				{
					$('#error_text').html('Time Slot Is not Available');
					$('#succ_text').html('');
					$('#btn_add').attr("disabled");
				}
				else
				{
					$('#succ_text').html('Time Slot Is Available');
					$('#error_text').html('');
					$('#btn_add').removeAttr("disabled");
				}
				
			}	 
		});
		
	} 
	
	function checkDoctorAvailbility()
	{ 
		var doctor_id=$('#doctor_id').val();   
		var app_date=$('#app_date').val(); 
		var base_url='<?php echo base_url(); ?>'; 
		
		$.ajax({
			url: base_url+"index.php/timingAvailability/",
			type: "POST",
			data: { doctor_id:doctor_id,app_date:app_date},
			success: function(result)
			{ 
				console.log(result);
				$('#app_time').html(result);
			 
			}	 
		});
		
	}
	
	function set_status(pval,app_id)
	{
		 
		$('#reschdule_val').val(pval); 
		$('#app_id').val(app_id);
		if(pval=='cancel')
		{ 
			$('#doctor_id').removeAttr("required");
			$('#app_date').removeAttr("required");
			$('#app_time').removeAttr("required");
		}
		else{
			$('#doctor_id').attr("required"); 
			$('#app_date').attr("required"); 
			$('#app_time').attr("required"); 
		}
		
	}
</script>

<script>
    $(function() {
		$.validator.setDefaults({
			ignore: []
		});
        $("#block-validate").validate({
            rules: {
				doctor_id: {
                    required: true
				},  
				app_date: {
                    required: true
				},
				app_time: {
                    required: true
				},
                remarks: {
                    required: true 
				}
			},
            //For custom messages
            messages: {
                doctor_id: {
					required: "Select Doctor"
				}, 
				app_date: {
					required: "Enter Date"
				}, 
				app_time: {
                    required: "Select time"
				},  
				remarks: {
                    required: "Enter Detail Description"
				}
				
			},
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
					} else {
                    error.insertAfter(element);
				}
			},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
				}
			},
		});
	});
</script>
 
	<div class="modal" id="modal_status_popup" >
		<div class="modal-dialog" role="document" style="max-width: 45%;">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Reschdule Appointment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body"> 
					<form action="<?php echo base_url(); ?>index.php/saveAppointment" autocomplete="off"  class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
						<div class="modal-body"  STYLE="height:100%">
							<div class="form-body">	
					
					<input   name="reschdule_val" id="reschdule_val"  type="hidden" >
					<input   name="app_id" id="app_id"  type="hidden" >
					<div class="divider"></div>
					<div class="card-content"> 
						<div class="row ">
							<div class="col-md-12" style="margin-top: 2em;"> 
								<div class=" ">
									<div class="h-form-label">
										<label>Patient Type</label>
									</div>
								</div>
								<div class=" ">
									<label>
										<input class="with-gap" name="reschdule" id="reschdule" value="reschdule" type="radio" checked="">
										<span>Reschdule</span>
									</label>
									<label>
										<input class="with-gap" name="reschdule" id="cancel" value="cancel" type="radio">
										<span>Cancel anyway</span>
									</label> 
								</div>  
							</div> 
							<div id="schdule_panel">
								<div class="col-md-6" >
									<div class="form-group">
									<label for="f-name1">Doctor Name :<span  class="required"> * </span></label>	
										<select name="doctor_id" id="doctor_id" class="form-control" required onchange="checkDoctorAvailbility(this.value);"> 
											
											<?php
												for ($i = 0; $i < count($docData); $i++) {
												?>
												<option value="<?php echo $docData[$i]->doctorID; ?>"><?php echo $docData[$i]->doctorName; ?></option>
											<?php } ?>	
										</select> 
										
									</div>
								</div>
								<div class="col-md-6" style="margin-bottom: 0.3em;">				
									<div class="form-group">				
											<label for="bio">Date:  <span  class="required"> * </span>	</label>
										<input name="app_date" class="form-control schduledatepicker" required id="app_date" placeholder="Appointmente Date" type="text"    >											
									
									</div>
								</div>		
								<div class="col-md-6">				
									<div class="form-group">				
										<label for="bio">Time:<span  class="required"> * </span>  	</label>	
										<select name="app_time" class="form-control"  id="app_time" required onchange="checkavailability();" > 
											<option value=""  disabled selected>Select time Slot </option>
											 
										</select> 		
															
									</div>	
									<p id="error_text" class="error"></p>
									<p id="succ_text" class="text-success"></p>
								</div>	 	
							</div>	 	
							<div class="col s12 m6 l12">				
								<div class="form-group">				
									<label for="bio">Remarks: <span  class="required"> * </span> 	</label>
									<input name="remarks" required class="form-control" id="remarks" placeholder="Enter Reschdule Details" type="text"    >											
									
								</div>
							</div>	 
						</div>
						
						
						
					</div>
					
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary mobtn" data-dismiss="modal">Close</button>
				<button type="submit" name="add_app_btn" class="modal-action modal-close  btn btn-primary">Save changes</button>
			</div>
			
		</form> 
	</div>
</div> 
</div> 
</div> 
