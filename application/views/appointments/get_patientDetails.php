<div class="dt-responsive table-responsive"> 
	<table   class="fold-table responsive-table display table table-striped table-bordered nowrap" style="width:100%">
		<thead>
			<tr>
				<th style="width:50px;" >Sr.No.</th> 
				<th class="col-md-3">Doctor Details</th>  
				<th class="col-md-1">Speciality</th>  
				<th class="col-md-3">Availability</th>  
				<th class="col-md-4"> Slots </th> 
			</tr>
		</thead>
		<tbody>
			<?php
				$cnt = 1;	
				$cn = 1;	
				foreach ($appoint_DocData as $key => $data) {
					$time1 = strtotime($data->fromTime);
					$time2 = strtotime($data->toTime);
					$difference = round(abs($time2 - $time1) / 3600); 
				 	$minutes = ($difference * 60)/ $data->doctorPatientTime; 
				?> 
			 <tr class="view odd gradeX collapsible">
					<td><?php echo $cn++; ?></td> 
					<td><?php echo $data->doctorName; ?> 
						<td><?php  
							for ($i = 0; $i < count($specalist); $i++)
							{
								
								$sp=explode(",",$data->doctorSpeciality);
								
								if(in_array($specalist[$i]->specialityID,$sp))
								{
								  echo   $specalist[$i]->specialityName."</br>"; 
								}
							}
						?>
						
						<td>From : <?php echo $data->fromTime; ?></br>
							<font color="red">	<?php echo $data->doctorPatientTime; ?> Minute Slot</font>
							<br>
						To : <?php echo $data->toTime; ?></td>
						<td class="form-group">	 
							<?php
								$n=count($data->appoint_patientData);
								
								for($j=0;$j<$minutes-$n;$j++)	
								{
								?>
								<b>
									<span class="label label-success">
										A
									</span>
									
									</b><?php
								}
								$arr=$data->appoint_patientData;
								for($j=0;$j<count($arr);$j++)	
								{ 
									if($arr[$j]->appoStatusFlag==0)
									{
									?>
									<b>
										<span class="label label-info">
											N
										</span>
										
										</b> <?php
										
									}
									else if($arr[$j]->appoStatusFlag==1)
									{
									?>
									<b>
										<span class="label label-warning">
											E
										</span>
										
									</b>
									<?php } else if($arr[$j]->appoStatusFlag==2)
									{
									?>
									<b>
										<span class="label label-danger">
											C
										</span>
										
										</b> <?php
										
									}
									else if($arr[$j]->appoFlag==1)
									{
									?>
									<b>
										<span class="label label-success">
											D
										</span>
										
										</b> <?php
										
									}
								} ?> 
						</td> 
						
						
						</tr>
						<tr class="fold">
							<td colspan="5">
								<div class="fold-content">
									
									<?php
										
										if(empty($data->appoint_patientData))
										{
										?>
										<font color="red">No Appintments</font>
										<?php }
										else
										{
										?>
										<h5>Patient Details </h5>
										<table class="inner_table">
											<thead>
												<tr>
													<th  class="col-md-1"> Type  </th>
													<th  class="col-md-3">Patient  </th>
													<th  class="col-md-3">Patient Name  </th>
													<th  class="col-md-3">Registration No.  </th>
													<th  class="col-md-2">Contact No.</th>
													<th class="col-md-2">	Gender</th> <th class="col-md-3">
														Time Slot</th> <th class="col-md-3">
													Schdule</th> 
												</tr>
											</thead>
											<tbody>
												<?php 
													
													foreach ($data->appoint_patientData as $key => $kdata) { 
														$st='';
														if($kdata->patientType=='New')
														{
															//$st='<img src="'.base_url().'assets/images/new.png" height="50px;">';
															$color="";
															$headercolor="text-info";	 
														} else  if($kdata->patientType=='Existing')
														{
														//	$st='<img src="'.base_url().'assets/images/existing.jpg" height="50px;">';
															$color="background: #34e3b9;";	 
															$headercolor="text-warning";	 
														}
														if($kdata->appoStatusFlag==2)
														{
															$color="background: #34e3b9;";	 
															$headercolor="text-success";	 
														} 
													?>
													<tr style="<?php echo $color; ?>"> 
														<td  class="text <?php echo $headercolor; ?>"><?php echo $kdata->patientType; ?></td>
														<td><img style="width: 40px;" class="img-fluid img-radius" src="<?php echo base_url(); ?>\files\assets\images\avatar-1.jpg" alt=""></td><td><?php echo $kdata->fullName; ?></td><td> <?php if(!$kdata->mrdNO) {  echo  $prefix[0]['prefix1']."".$prefix[0]['yearWise']."". $prefix[0]['startFrom']; ?> / <?php echo $kdata->mrdNO; } ?></td>
														<td><?php echo $kdata->contactNO; ?></td>
														
														<td><?php echo $kdata->gender; ?><br>Age: <?php echo $kdata->age; ?></td> 
														<td><?php echo $kdata->appoTime; ?></td> 
													 
															<td class="text-center">
																<a class="dropdown-toggle addon-btn" data-toggle="dropdown" aria-expanded="true">
																	<i class="icofont icofont-ui-settings"></i>
																</a>
																<div class="dropdown-menu dropdown-menu-right">
																	<a class="dropdown-item" data-toggle="modal" data-target="#modal_status_popup"   onclick="set_status('prepone',<?php echo $kdata->appoID; ?>);"><i class="icofont icofont-attachment"  ></i>Reschdule</a>
																	<?php if($kdata->patientType=='New')
																{ ?>
																		<a class="dropdown-item" href="<?php echo base_url(); ?>index.php/newRegistration/<?php echo $kdata->appoID; ?>"><i class="icofont icofont-ui-edit"></i>Register</a><?php 
																} ?>
																	<div role="separator" class="dropdown-divider"></div>
																	<a  data-toggle="modal" data-target="#modal_status_popup"  onclick="set_status('cancel',<?php echo $kdata->appoID; ?>);" class="dropdown-item" href="#"><i class="icofont icofont-refresh"></i>Cancel</a>
																</div>
															 
														
														</td>
														
													</tr>
													<?php }
												?>
											</tbody>
										</table>    
									<?php } ?>
								</div>
							</td>
						</tr>
						
					<?php $cnt++; } ?>
				</tbody>
			</table> 
		</div> 
		
		<style>
			@import url('https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css');
			
			
			// Table
			table {
			width: 100%;
			}
			table>th { text-align: left; border-bottom: 1px solid #000;}
			table>th, td { padding: .4em; }
			}
			.inner_table
			{
			width:100%;
			}
			table.fold-table > tbody > tr.view {
			table.fold-table > tbody > tr.view td, th {cursor: pointer;}
			table.fold-table > tbody > tr.view td:first-child, 
			table.fold-table > tbody > tr.view  th:first-child { 
			position: relative;
			padding-left:20px;
			}
			table.fold-table > tbody > tr.view::before {
			position: absolute;
			top:50%; left:5px;
			width: 9px; height: 16px;
			margin-top: -8px;
			font: 16px fontawesome;
			color: #000;
			content: "\f0d7";
			transition: all .3s ease;
			}
			}
			
			&:hover { background: #ddd; }
			table.fold-table > tbody > tr.view.open { 
			color: #404e67;
			}
			
			
			
			table.fold-table > tbody > tr.fold {
			display: none; 
			}
			table.fold-table > tbody > tr.fold.open { display:table-row;  }
			}
			
			
			
			.fold-content { 
			padding: 2em;
			}
			.fold-content h3 { margin-top:0; }
			
			.fold-content> table {
			border: 2px solid #ccc;
			}
			.inner_table
			{
			margin: 1em;
			width: 97%;
			}
			.inner_table thead {
			color: #080505;
			background-color: #999;
			}
		</style>
		<script>
			$(function(){
				$(".fold-table tr.view").on("click", function(){
					$(this).toggleClass("open").next(".fold").toggleClass("open");
				});
			});
		</script>
		
		
		