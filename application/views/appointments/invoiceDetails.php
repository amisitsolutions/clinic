<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				
				<div class="page-body">
					<div class="row">
						<div class="col-xl-8">
                                        
                                                <div class="card">
                                                      <div class="card-header">
                                                        <h5>Invoice Details</h5>
                                                        <div class="card-header-right">
                                                            <i class="icofont icofont-spinner-alt-5"></i>
                                                        </div>
														  <a href="<?php echo base_url(); ?>index.php/patientInvoice" class="btn btn-sm btn-success ">  New Invoice </a>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="table-responsive">
                                                            <div id="issue-list-table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-xs-12 col-sm-12 col-sm-12 col-md-6"><div class="dataTables_length" id="issue-list-table_length"><label>Show <select name="issue-list-table_length" aria-controls="issue-list-table" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-xs-12 col-sm-12 col-md-6"></div></div><div class="row"><div class="col-xs-12 col-sm-12"><table id="issue-list-table" class="table dt-responsive width-100 dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="issue-list-table_info" style="width: 643px;">
                                                                <thead class="text-left">
                                                                    <tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 49px;">Type</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">#ID</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 196px;">Description</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Start date</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 70px;">Priority</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px; display: none;">Assigned</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px; display: none;">Status</th></tr>
                                                                </thead>
                                                                <tbody class="text-left">
                                                                    
                                                           <tr role="row" class="odd">
                                                                        <td class="txt-primary" tabindex="0" style="">Bug</td>
                                                                        <td>PI:198756541080</td>
                                                                        <td>Software Run Failure</td>
                                                                        <td>2015/01/10</td>
                                                                        <td><span class="label label-danger">Highest</span></td>
                                                                        <td style="display: none;"><a href="#">Katerina larson</a></td>
                                                                        <td style="display: none;"><span class="label label-primary">Open</span></td>
                                                                    </tr><tr role="row" class="even">
                                                                        <td class="txt-primary" tabindex="0" style="">Bug</td>
                                                                        <td>PI:198756897280</td>
                                                                        <td>Server Randering</td>
                                                                        <td>2015/04/22</td>
                                                                        <td><span class="label label-success">Normal</span></td>
                                                                        <td style="display: none;"><a href="#">Mitchell Jones</a></td>
                                                                        <td style="display: none;"><span class="label label-danger">Close</span></td>
                                                                    </tr> </tbody>
                                                            </table>
															
															</div></div>
															 </div></div></div>
                                                        </div>
                                                        <!-- end of table -->
                                                    </div>
                                                
								<div class="col-xl-4">
                                                <!-- Overall Progress card start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <!-- <p>.col-sm-4</p> -->
                                                        <div class="issue-list-progress">
                                                            <h6>Overall Progress</h6>
                                                            <div class="issue-progress">
                                                                <div class="progress">
                                                                    <span class="issue-text1 txt-primary" style="left: 70%; top: -20px;">70%</span>
                                                                    <div class="issue-bar1 bg-primary" style="width: 70%;"></div>
                                                                </div>
                                                                <!-- end of progress -->
                                                            </div>
                                                            <!-- end of issue progress -->
                                                        </div>
                                                        <!-- end of issue list progress -->

                                                        <!-- end of matric progress -->
                                                        <table class="table matrics-table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Assigned Hours</strong>
                                                                    </td>
                                                                    <td class="txt-primary">70 Hours</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Time Consumed</strong>
                                                                    </td>
                                                                    <td class="txt-danger">49 Hours</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Issues</strong>
                                                                    </td>
                                                                    <td class="txt-primary">19</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <strong>Bugs</strong>
                                                                    </td>
                                                                    <td class="txt-primary">16</td>
                                                                </tr>
                                                               
                                                            </tbody>
                                                            <!-- end of tbody -->
                                                        </table>
                                                        <!-- end of table -->
                                                    </div>
                                                </div>
                                                <!-- Overall Progress card stendart -->
                                            </div></div>
					<!-- Page body end -->
				</div>
			</div>
			
		</div>
	</div>
	
	
	
	<?php
		$this->load->view("admin_includes/footer");
	?>				
</div> 