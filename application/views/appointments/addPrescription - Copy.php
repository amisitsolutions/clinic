<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	//$this->load->view("admin_includes/sidebar");
 	
?>
<style>
	label.input-field {
    position: relative;
    margin-top: 0;
    margin-bottom: 0; 
	}
	table tr>th,tr>td
	{
	border: 1px solid #ccc;
	padding-left: 1em !important;
	padding-right: 1em !important;
	padding: 0;
	}
	@media (min-width: 768px)
	{
	.col-md-4 {
	-webkit-box-flex: 0;
	-ms-flex: 0 0 33.333333%;
	flex: 0 0 33.333333%;
	max-width: 33.333333%;
	float: left;
	}
	}
	
	.pdetails_table td, .pdetails_table th {
    padding: 0.2rem !Important;  
}
.pdetails_table th { 
    color: chocolate;
    font-weight: 400;
    font-size: 14px;
}
 .pdetails_table td { 
    color: #333 !important;
}
	.pdetails_table td, .pdetails_table th {
			padding: 0.2rem !Important;
	}
	.table td, .table th {
    padding: 0;
    vertical-align: middle;
}
.form-group {
    margin-bottom: .5em;
}
</style> 
    <div id="pcoded" class="pcoded">
        <div class="pcoded-container">
	<div class="pcoded-content">
		<div class="pcoded-inner-content">
			<!-- Main-body start -->
			<div class="main-body">
				<div class="page-wrapper">
					
					<div class="page-body">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div class="card user-card-full">
									<div class="row m-l-0 m-r-0">
										<div class="col-sm-2 bg-c-lite-green user-profile">
											<div class="text-center text-white">
												<div class="m-b-25">
													<img style="width: 154px;" src="..\files\assets\images\avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
												</div>
												<h6 class="f-w-600"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->fullName; } ?></h6>
												 <p><?php if(!empty($getpatientData)) { echo $getpatientData[0]->contactNO; } ?></p> 
												
											</div>
										</div>
										<div class="col-sm-10">
											<div class="card-block_cls filter-bar">
												<nav class="navbar-light bg-faded p-10">
                                                    <ul class="nav navbar-nav">
                                                        <li class="nav-item active">
															
                                                            <a class="nav-link"  ><b>MRD No.</b>: <span class="sr-only">(current)</span></a>
														</li>
                                                        <li class="nav-item dropdown">
															<form action="<?php echo base_url(); ?>index.php/getMRDData"  class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
																<div class="input-group">
																	<input type="text" name="mrdNo" class="form-control" value="<?php echo @$_POST['mrdNo']; ?>" placeholder="Enter MRD No. ....">
																	<button type="submit" name="btn-search" class="input-group-addon" id="basic-addon1"><i class="icofont icofont-search"></i></span>
																</div>
															</form>
														</li>
														
													</ul>
													<div class="nav-item nav-grid">
														<span class="m-r-15">Other Details </span>
														<button type="button" class="btn btn-sm btn-primary waves-effect waves-light m-r-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Previous">
															<i class="icofont icofont-ui-previous"></i>
														</button>
														<button type="button" class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Next">
															<i class="icofont icofont-ui-next"></i>
														</button>
													</div>
													<!-- end of by priority dropdown -->
													
												</nav>
												<h6 class="m-b-20 p-b-5 b-b-default f-w-600">Patient Details</h6>
												<div class="row">
													
													<div class="col-lg-4 col-md-12">      
														<form>
															<table class="pdetails_table table table-responsive m-b-0">
																<tbody> 
																
																<tr>
																	<th class="social-label b-none">Birth Date</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->dob; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none">Gender</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->gender; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none">Marital Status</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->marritalStatus; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none p-b-0">Email</th>
																	<td class="social-user-name b-none p-b-0 text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->emailID; } ?></td>
																</tr><tr>
																	<th class="social-label b-none p-b-0">Address</th>
																	<td class="social-user-name b-none p-b-0 text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->relativeAddress; } ?></td>
																</tr>
																</tbody></table>
														</form>
													</div>
														<div class="col-lg-4 col-md-12">      
														<form>
															<table class="pdetails_table table table-responsive m-b-0">
																<tbody><tr>
																	<th class="social-label b-none p-t-0">Patient Registration Date
																	</th>
																	<td class="social-user-name b-none p-t-0 text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->createdDate; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none p-t-0">Patient Type
																	</th>
																	<td class="social-user-name b-none p-t-0 text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->patientType; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none">Reffered by</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->lastAppoDate; } ?></td>
																</tr>
															 <tr>
																	<th class="social-label b-none">Last Visit Date</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->lastAppoDate; } ?></td>
																</tr>
															 
																</tbody>
															</table>
														</form>
													</div>
												
													<div class="col-lg-4 col-md-12">      
														<form>
															<table class="pdetails_table table table-responsive m-b-0">
																<tbody><tr>
																	<th class="social-label b-none p-t-0">Relation with Attendar
																	</th>
																	<td class="social-user-name b-none p-t-0 text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->relWithPatient; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none">Relative Name</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->relativeName; } ?></td>
																</tr>
																<tr>
																	<th class="social-label b-none">Relative Contact</th>
																	<td class="social-user-name b-none text-muted"><?php if(!empty($getpatientData)) { echo $getpatientData[0]->relativeContact; } ?></td>
																</tr>
																 
																</tbody></table>
														</form>
													</div>
													
													
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="card">
									<div class="card-block">
										<h4 class="sub-title">Prescription Details</h4>
										<form action="<?php echo base_url(); ?>index.php/save_prescription" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
								<input type="hidden" name="patient_id" id="patient_id" class="form-control" value="<?php if(!empty($getpatientData)) { echo $getpatientData[0]->patientID; } ?>">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Type of DM</label>
												<div class="col-sm-8">
													<input type="text" name="type_of_dm[]" id="type_of_dm" class="form-control tagsinput" data-role="tagsinput">
												
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Complication Of DM</label>
												<div class="col-sm-8">
													<input type="text" name="complication_of_dm[]" id="complication_of_dm"  class="form-control tagsinput" data-role="tagsinput"  >
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Assocaiated Illness</label>
												<div class="col-sm-8">
													<input type="text"  name="associated_illness[]" id="associated_illness"    class="form-control tagsinput" data-role="tagsinput"  >
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Present Complaints</label>
												<div class="col-sm-8">
													<input type="text"  name="present_complaints[]" id="present_complaints"  class="form-control tagsinput" data-role="tagsinput"   >
												</div>
											</div> 
										
										
										<h6><b>Prescription</b></h6>
										<div class="col-md-12 col-sm-12 col-xs-12 form-group ">
											<table class="jsgrid-table table table-striped table-hover ser" > 
												<thead class="bordered-darkorange">
													<tr><th>
														<div class="checkbox">
															<label>
																<input type="checkbox" class="colored-blue check_all">
																<span class="text"></span>
															</label>
														</div>
													</th>  
													<th> Type</th>
													<th>Medicine Name</th>
													<th>Usage</th>									
													<th>Dosage</th>									
													<th>No.Of Days </th>									
													</tr>
													</thead>
												<tbody>
													<tr>
														<td style="width: 1% !important;">
													 
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="colored-blue case chk">
																	
																	<span class="text"></span>
																	
																</label>
																
															</div>
															
														</td> 
														<td class="form-group">
															<div class="input-field">
																<select name="type_id[]"  id="type_id0">
																	<option value="" selected  >Select Medicine Type </option>
																	<?php
																		for ($i = 0; $i < count($medtypeData); ++$i) {
																		?>
																		<option   value="<?php echo $medtypeData[$i]->medicineTypeID; ?>"><?php echo $medtypeData[$i]->medicineType; ?></option>
																	<?php } ?>	
																	
																</select> 
															</div> 
															
														</td>
														<td class="form-group">
														 
															 	<select name="medicine_name[]"  id="medicine_name0" class="js-example-tags">
																	<option value="" selected disabled>Select Medicine </option>
																	<?php
																	/* 	for ($kk = 0; $kk < count($medicineData); $kk++) {
																		?>
																		<option   value="<?php echo $medicineData[$kk]->medicineID; ?>"><?php echo $medicineData[$kk]->medicineName; ?></option>
																	<?php } */ ?>	
																	<?php
																		for ($i = 0; $i < count($medtypeData); ++$i) {
																		?>
																		<option   value="<?php echo $medtypeData[$i]->medicineTypeID; ?>"><?php echo $medtypeData[$i]->medicineType; ?></option>
																	<?php } ?>
																</select>
														
															  
														</td>
														<td class="form-group">
															 	<input type="text" name="usage[]" id="usage0"   value="" >
														</td>
														<td class="form-group">
															<input type="text" name="dosage[]" id="dosage0" class= value=" " >
															</td><td class="form-group">
															<input type="text" name="no_of_days[]" id="no_of_days0"   value="" >
														</td> 
													</tr>
												</tbody> 
												<tfoot> 
													<tr>
														<td colspan="4">	
															
															<a  class='addmore label label-sm label-info'>+ Add More</a>
															<a  class='delete label label-sm label-danger'>-Delete</a>
														</td>
													 
													</tr>
												</tfoot>
											</table>
										</div>
										
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Advice</label>
											<div class="col-sm-8">
												<input type="text" name="advice[]"  class="form-control tagsinput" data-role="tagsinput">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Target</label>
											<div class="col-sm-8">
												<input type="text"  name="target[]" class="form-control tagsinput" data-role="tagsinput">
											</div>
											</div><div class="col-sm-4" style="float:left;">
											<label >RMP</label>
											<label  class="form-control">Consulat name</label>
											
										</div>
										<div class="col-sm-4" style="float:left;">
											<label>Qualification</label>
											<label class="form-control">Qualification</label>
										</div>
										<div class="col-sm-4" style="float:left;">
											<label>Signature File</label>
											<label  class="form-control">
												<img style="width: 154px;" src="..\files\assets\images\avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
												
												</label>
											
										</div>
									</div> 
									<div class="form-group row"><label class="col-sm-4 col-form-label"></label>
										   <div class="col-sm-7">
												<button class="btn btn-primary m-b-0" type="submit" name="add_ser_btn" value="add_ser_btn">Submit
												</button>
												                                                    
</div>
</div>
								</div>
							</div> 
							<div class="col-md-4">
								
								<div class="card">
									<div class="card-block"> 
										<div class="form-group row">
											<label class="col-sm-12 col-form-label">Consultant Charges</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="consultant_charges" id="consultant_charges">
											</div>
										</div>
										<h4 class="sub-title">Present Medications</h4>
										
										<table class="jsgrid-table table table-striped table-hover ser" > 
											<thead class="bordered-darkorange">
												<tr>
													<th>Sr#	</th> 
													<th>Medicine Name</th>
													<th>Dosage</th>	  							
													<th> </th>	  							
												</tr>
											</thead>
											<tbody>
												<tr>
													<td >1</td>
													<td>
														
													</td>
													<td>
														
													</td>
													<td class="action-icon">
                                                                                    <a href="#!" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="icofont icofont-ui-edit"></i></a>
                                                                                    <a href="#!" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="icofont icofont-delete-alt"></i></a>
                                                                                </td>
													<!-- <td><span class="ibtn_container right"><a class="label label-primary btn-sm waves-effect waves-light ">
														<span><i class="icofont icofont-ui-check"></i></span></a>
														<a class="label label-danger btn-sm waves-effect waves-light "><span><i class="icofont icofont-ui-close"></i></span></a></span>
													</td> -->
												</tr>
											</tbody> 
										</table>  
										
										<h4 class="sub-title">General Investigations</h4>
										
										<table class="jsgrid-table table table-striped table-hover ser" > 
											<thead class="bordered-darkorange">
												<tr>
													<th>Sr#	</th> 
													<th>Investigation</th>
													<th>Result</th>	 							
												</tr>
											</thead>
											<tbody>
												<tr>
													<td >1</td>
													<td class="form-group">
														
													</td>
													<td class="form-group">
														
													</td> 
												</tr>
											</tbody> 
											
										</table>
										
										<h4 class="sub-title">Attachments</h4>
										
										<table class="jsgrid-table table table-striped table-hover ser" > 
											<thead class="bordered-darkorange">
												<tr>
													<th>Sr#	</th> 
													<th>Date</th>
													<th>Attachments</th>	  					
												</tr>
											</thead>
											<tbody>
												<tr>
													<td >1</td>
													<td class="form-group">
														
													</td>
													<td class="form-group">
														
													</td> 
												</tr>
											</tbody> 
											
										</table>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
		
		
		<?php
			$this->load->view("admin_includes/footer");
		?>				
	</div>
	    
	<script>
	
	</script>
	<script>
		$(document).ready(function() {
			 
			var k=1; 				
			$(".addmore_spec").on('click',function(){		
				var data ='<tr><td>';
				data +='  <select name="type_id[]" id="type_id'+i+'">';
				data +='<option value="" selected disabled>Select Medicine Type </option> <?php for ($i = 0; $i < count($medtypeData); ++$i) { ?> <option   value="<?php echo $medtypeData[$i]->medicineTypeID; ?>"><?php echo $medtypeData[$i]->medicineType; ?></option> <?php } ?>	 </select> ';
				data +='  </td><td>';
				data +='  <select name="medicine_name[]"  id="medicine_name'+i+'"   class="js-example-tags">';
				data +='<option value="" selected disabled>Select Medicine Type </option> <?php for ($i = 0; $i < count($medtypeData); ++$i) { ?> <option   value="<?php echo $medtypeData[$i]->medicineTypeID; ?>"><?php echo $medtypeData[$i]->medicineType; ?></option> <?php } ?>	 </select> ';
				data +='  </td>  ';   
				   
				data +=' <td class="form-group"> <input type="text" name="usage[]" id="usage'+i+'"  value="" >';
				data +='</td> <td class="form-group">';
				data +='<input type="text" name="dosage[]" id="dosage'+i+'"  value=" " >';
				data +='</td><td class="form-group">';
				data +='<input type="text" name="no_of_days[]" id="no_of_days'+i+'" value="" >';
				data +='</td>';   
			 	   
				data +=' </tr>';
				
				$('.ser').append(data);
				
				$('#btn_delete').show();
				k++;		
			});	
			
		});
	</script>	  
	<script>
		$(function() {
			
			$("#block-validate").validate({
				rules: {
					doctor_id: {
						required: true
					},
					fees: {
						required: true
					},
					app_time: {
						required: true
					}
				},
				//For custom messages
				messages: {
					doctor_id: {
						required: "Select a Doctor"
					},
					fess: {
						required: "Enter a Fees  Details"
					},
					app_time: {
						required: "Enter a App Time  Details"
					}
				},
				errorElement: 'div',
				errorPlacement: function(error, element) {
					var placement = $(element).data('error');
					if (placement) {
						$(placement).append(error)
						} else {
						error.insertAfter(element);
					}
				},
				invalidHandler: function(e, validator) {
					var errors = validator.numberOfInvalids();
					if (errors) {
						$('.error-alert-bar').show();
					}
				}
			});
		});
	</script>			