<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				<!-- Page-header start -->
				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							<div class="page-header-title">
								<div class="d-inline">
									<h4>Meeting</h4>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<!-- Page-header end -->
				
				<!-- Page body start -->
				<div class="page-body invoice-list-page">
					<div class="row">
						<div >
						<?php echo $meeting; ?>
						 <div id="meet">
	
						</div>

						</div>
		 				</div>
				</div>
				<!-- Page body end -->
			</div>		
		</div>
	</div>
	
	
	
	<?php
		$this->load->view("admin_includes/footer");
	?>				
</div>  
	
	<script src='https://meet.jit.si/external_api.js'></script>

<script type="text/javascript">
	$(document).ready(function() {	
		myFunction();
		 
	});
</script> 
<script type="text/javascript">	
	 
	function myFunction(){
    const domain = 'meet.jit.si';
const options = {
    roomName:<?php echo $meeting; ?>,
    width: 1100,
    height: 700,
    parentNode: document.querySelector('#meet')
};
const api = new JitsiMeetExternalAPI(domain, options);
	} 
	</script>