<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
<style>
	table tr>th,tr>td
	{
		border: 1px solid #ccc;
		padding-left: 1em !important;
		padding-right: 1em !important;
		padding: 0;
	}
	.input-field {
    position: relative;
    margin-top: 1em !important;
    margin-bottom: 1em !important;
}
</style>
<div class="page-wrapper">
	
	<div class="container-fluid">
		
		<div class="row"> 
			<div class="col s12 m12"><h5 class="card-title">Patient Invoice View:</h5>
				<div class="card">
					
					<div class="row">
						
						<div class="card-content"> 
							<h5 class="card-title"> 
								
							<span class="pull-right"><b>Date </b><?php echo  date("d-m-Y"); ?></span></h5>
							</br>
							<form action="<?php echo base_url(); ?>index.php/savePatientInvoice" class="form-horizontal"  autocomplete="off" id="block-validate" method="POST" enctype="multipart/form-data" >
								
							 	<div class="row" >	
									<div class="panel panel-default"> 
										<div class="panel-body">
											<div class="col s12" >
												<div class="col s12 m6 l3" >
													<div class="input-field">
														<select name="filter_by" class="form-control"  onchange="get_filter(this.value);" id="filter_by"   >
															<option value="mrdNo">MRD No.</option> 
															<option value="contact">Contact No.</option>
														</select>
														<label for="f-name1">Filter By:<span  class="required"> * </span></label>
													</div>
												</div>
											</div>
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="mrd_no" class="form-control"  onchange="check_Patient();" id="mrd_no"   >
													<label for="f-name1">MRD No.:<span  class="required"> * </span></label>
												</div>
											</div>
											<input type="hidden" name="patient_id" class="form-control"    id="patient_id"   >
											
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" readonly name="fullname" class="form-control read_only_cls"  onchange="check_Patient();" id="fullname"   >
													
													<label for="f-name1">Patient Name :<span  class="required"> * </span></label>
												</div>
											</div>
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="contact_no" class="form-control read_only_cls" id="contact_no"   >
													<label for="f-name1">Contact No. :<span  class="required"> * </span></label>
												</div>
											</div>
											<div class="col s12 m6 l2" >
												<div class="input-field">
													<input type="text" readonly name="gender" class="form-control read_only_cls" id="gender"   >
													<label for="f-name1">Gender </label>
												</div>
											</div>
											<div class="col s12 m6 l1" >
												<div class="input-field">
													<input type="text" readonly name="age" class="form-control read_only_cls" id="age"   >
													<label for="f-name1">Age  </label>
												</div>
											</div>
											
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="email" readonly name="email" class="form-control read_only_cls" id="email"   > 
													<label for="f-name1">Email:<span  class="required"> * </span></label>
												</div>
											</div>
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="patient_type" class="form-control" id="patient_type"   >
													<label for="f-name1">Patient Type :<span  class="required"> * </span></label>
												</div>
											</div>
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="address" class="form-control" id="address"   >
													<label for="f-name1">Address :<span  class="required"> * </span></label>
												</div>
												</div>	<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="visit_date" class="form-control datepicker" id="visit_date"   > 
													<label for="f-name1">Last Visit Date:<span  class="required"> * </span></label>
												</div>
											</div>
											<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="visit_no" class="form-control " id="visit_no"   > 
													<label for="f-name1">Visit No.:<span  class="required"> * </span></label>
												</div>
											</div>
											<div class="col s12 m6 l4" >
												<div class="input-field">
													<select name="doctor_name" id="doctor_name"  readonly  onchange="get_consultant_charges(this.value);"> 
														<option value="">Select Doctor</option>
														<?php
															for ($k = 0; $k < count($doctorData); $k++) {
															?>
															<option <?php if(!empty($edit_timingdata)) {  if($edit_timingdata[0]['tbl_doc_id']==$doctorData[$k]->tbl_doctor_master_id)
															{ echo "selected"; } }  ?> value="<?php echo $doctorData[$k]->tbl_doctor_master_id; ?>"><?php echo $doctorData[$k]->doctor_name; ?></option>
														<?php } ?>
													</select> 
													<label for="f-name1"> Consultant Name<span  class="required"> * </span></label>
												</div>
											</div>
											
											<div class="col s12 m6 l3" style="margin-bottom:1em;">
												<div class="input-field">
													<select name="doc" id="doc" > 
														<option value="">Select Doctor</option>
														<?php
															for ($k = 0; $k < count($doctorData); $k++) {
															?>
															<option <?php if(!empty($edit_timingdata)) {  if($edit_timingdata[0]['tbl_doc_id']==$doctorData[$k]->tbl_doctor_master_id)
															{ echo "selected"; } }  ?> value="<?php echo $doctorData[$k]->tbl_doctor_master_id; ?>"><?php echo $doctorData[$k]->doctor_name; ?></option>
														<?php } ?>
													</select> 
													
													<label for="l-name2">Ref By : </label>
												</div>
											</div>
											
											
											<div class="insurance_panel_cls" style="display:none;" >
												<div class="col s12 m6 l4" >
													<div class="input-field">
														<select name="insurance_id" id="insurance_id"  > 
															<option value="">Select Insurance</option>
															<?php
																for ($k = 0; $k < count($insuranceData); $k++) {
																?>
																<option   value="<?php echo $insuranceData[$k]->tbl_insurance_id; ?>"><?php echo $insuranceData[$k]->insurance_name; ?></option>
															<?php } ?>
														</select> 
														<label for="f-name1"> Insurance  Name<span  class="required"> * </span></label>
													</div>
												</div>
												
												<div class="col s12 m6 l3" >
													<div class="input-field">
														<input type="text" name="ref_no" class="form-control" id="ref_no"   > 
														<label for="f-name1">Ref No.: </label>
													</div>
												</div>
												<div class="col s12 m6 l3" >
												<div class="input-field">
													<input type="text" name="insurance_deatials" class="form-control" id="insurance_deatials"   > 
													<label for="f-name1">Insurance Details: </label>
												</div>
											</div>	
											</div>
										</div>
									</div>
								</div>
								<div class="row">	
									<div class="panel panel-default"> 
										<div class="panel-body">
											<div class="col-md-6 col-sm-6 col-xs-12 form-group ">
												<table class="table table-bordered table-hover table-striped ser" > 
													<thead class="bordered-darkorange">
														<tr><th>
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="colored-blue check_all">
																	<span class="text"></span>
																</label>
															</div>
														</th> 
														<th>
															Sr. No. 
														</th>
														<th>Service Type</th>
														<th>Service Name</th>
														<th>Qty</th>									
														<th>Rate</th>									
														<th>Total </th>									
														</tr>
													</thead>
													<tbody>
														<tr><td style="width: 1% !important;">
															
															<input type="hidden" class="form-control no_of_products"  name="no_of_products[]" id="no_of_products0"  value="0"/>
															<div class="checkbox">
																<label>
																	<input type="checkbox" class="colored-blue case chk">
																	
																	<span class="text"></span>
																	
																</label>
																
															</div>
															
														</td>
														<td >1</td>
														<td class="form-group">
															<div class="input-field">
																<select name="type_id[]"  id="type_id0" onchange="get_speciality(this.value,'0');">
																	<option value="" selected disabled>Select Service Type </option>
																	<?php
																		for ($i = 0; $i < count($serviceTypeList); ++$i) {
																		?>
																		<option   value="<?php echo $serviceTypeList[$i]->service_type_id; ?>"><?php echo $serviceTypeList[$i]->service_type; ?></option>
																	<?php } ?>	
																	
																</select> 
															</div> 
															
														</select>
														</td>
														<td class="form-group">
															<select name="speciality[]"  id="speciality0" onchange="get_services_charges(this.value,'0');" >
																<option value="" disabled >Select Service Name </option>
																
															</select>
														</td>
														<td class="form-group">
															<input type="text" name="qty[]" id="qty0" class="form-control qty" onblur="cal_amount(0);" value="<?php //echo $rows['tos_times']; ?>" >
															</td><td class="form-group">
															<input type="text" name="rate[]" id="rate0" class="form-control rate" onblur="cal_amount(0);" value="" >
															</td><td class="form-group">
															<input type="text" name="total[]" id="total0" class="form-control amt" value="" >
														</td>
													</tr>
												</tbody> 
											<tfoot> 
												<tr>
													<td colspan="4">	
														
														<a  class='addmore label label-sm label-info'>+ Add More</a>
														<a  class='delete label label-sm label-danger'>-Delete</a>
													</td>
													<td colspan="2"> <p style="text-align:right;"> Total</p></td>
													<td > 
														<input type="text" name="subtotal" id="subtotal" class="form-control" value="" >
													</td>
												</tr>
											</tfoot>
											</table>
											<div class="col s12 m12" >
												<div class="col s12 m6 l2" >
													<div class="input-field">
														<input type="text" name="discount_type" class="form-control" id="discount_type"   > 
														<label for="f-name1"> Discount  Type<span  class="required"> * </span></label>
													</div>
												</div> 
												<div class="col s12 m6 l2" >
													<div class="input-field">
														<input type="text" name="discount_ref_no" class="form-control" id="discount_ref_no"   > 
														<label for="f-name1"> Discount  Ref No.<span  class="required"> * </span></label>
													</div>
												</div> 
												<div class="col s12 m6 l2" >
												<div class="input-field">
												 <input type="text" name="discount" id="discount" class="form-control" onchange="get_total();" value="" >
												
														<label for="f-name1"> Discount Charges<span  class="required"> * </span></label>
													</div>
												</div> 
												</div> 
												
											<div class="col s12 m12" >
												<div class="col s12 m6 l8" >
													<div class="input-field col s4">
															<label for="f-nameh"> Consultant  Charges</label>
														</div>
															<div class="col s5">
														<input type="text" name="service_charges" readonly id="service_charges" class="form-control read_only_cls" onchange="get_total();" value="" > 
														 
													</div>
													</div> 
												<div class="col s12 m6 l4" >
												<div class="input-field col s3">
															<label for="f-nameh">Net Total</label>
														</div>
														<div class="col s7">
															<input type="text" readonly name="final_total" id="final_total" class="form-control"   >
														</div> 
												</div> 
												</div> 
											 
											
											<div class="col s12 m12 m-t-40">
											<h6><b>Payment Option</b></h6>
												<div class="col s12 l6">
													<div class="row"> 
														<div class="input-field col s3">
															<label for="f-nameh">Cash</label>
														</div>
														<div class="col s3">
															<input id="cash" name="cash" type="text" placeholder="eg.100">
														</div>
														<div class="col s5">
															<input id="cash_details"  name="cash_details" type="text" placeholder="Cash Details">
														</div>
													</div>
												</div>
											</div>
											<div class="col s12 m12">
												<div class="col s12 l6">
													<div class="row"> 
														<div class="input-field col s3">
															<label for="f-nameh">CC</label>
														</div>
														<div class="col s3">
															<input id="cc" name="cc" type="text" placeholder="eg.100">
														</div>
														<div class="col s5">
															<input id="cc_details" name="cc_details" type="text" placeholder="CC Details">
														</div>
													</div>
												</div>
												
											</div>	<div class="col s12 m12">
												<div class="col s12 l6">
													<div class="row"> 
														<div class="input-field col s3">
															<label for="f-nameh">Online</label>
														</div>
														<div class="col s3">
															<input id="online" name="online" type="text" placeholder="Eg.100">
														</div>
														<div class="col s5">
															<input id="online_details"  name="online_details" type="text" placeholder="Online Details">
														</div>
													</div>
												</div>
												
											</div>
											<div class="col s12 m12">
												<div class="col s12 l6">
													<div class="row"> 
														<div class="input-field col s3">
															<label for="f-nameh">Credit</label>
														</div>
														<div class="col s3">
															<input id="credit" name="credit" type="text" placeholder="Eg.100">
														</div>
														<div class="col s5">
															<input id="credit_details"  name="credit_details" type="text" placeholder="Credit Details">
														</div>
													</div>
												</div>
											</div>
										</div> 
											<div class="col s12 m12 m-t-40 m-b-40">
												<div class="col s12 l6"> 
														<div class="input-field col s3">
															<label for="f-nameh"><b>Total Paid</b></label>
														</div>
														<div class="col s6">
															<input type="text" readonly name="paid_total" id="paid_total" class="form-control read_only_cls"   >
														</div>  
												</div>	
												<div class="col s12 l6"> 
														<div class="input-field col s3">
															<label for="f-nameh"><b>Total Unpaid</b></label>
														</div>
														<div class="col s6">
															<input type="text" readonly name="unpaid_total" id="unpaid_total" class="form-control read_only_cls"   >
														</div>  
												</div>
											</div>
										
											 <div class="col s12 m6 l12" >
												<div class="input-field">
													<input type="text" name="remarks" class="form-control" id="remarks"   > 
													<label for="f-name1">Remarks Details: </label>
												</div>
											</div>	
									 
								</div> 
							<div class="col m12 m-t-20 m-b-20">
								<div class="col m6 form-group">
									<input type="submit" name="btnAdd" id="btnAdd"    	 class="btn cyan waves-effect waves-light" value="Submit"  />
									
								</div>
							</div>
						</div>
					</form> 
					
				</div>
				
			</div>
			
		</div>
	</div>
</div>
</div>
</div>
</div>


</div>


<?php
	$this->load->view("admin_includes/footer");
?>				
</div>


<script type="text/javascript">
	$(document).ready(function() {	
		
		
		$('select').not('.disabled').formSelect();
		var i=2;  
		$(".addmore").on('click',function(){				
			var data='<tr><td style="width: 1% !important;">';
			
			data +='<input type="hidden" class="form-control no_of_products"  name="no_of_products[]" id="no_of_products0"  value="0"/>';
			data +='<div class="checkbox"> <label>';
			data +='<input type="checkbox" class="colored-blue case chk">		<span class="text"></span>';
			
			data +='</label></div> </td><td>'+i+'</td><td class="form-group">';
			data +='<select name="type_id"  id="type_id'+i+'" onchange="get_speciality(this.value,'+i+');"> <option value=""  disabled>Select Service Type </option> <?php for ($i = 0; $i < count($serviceTypeList); ++$i) { 	?> <option   value="<?php echo $serviceTypeList[$i]->service_type_id; ?>"><?php echo $serviceTypeList[$i]->service_type; ?></option> <?php } ?>';
			data += '</select> </td>';
			data +='<td class="form-group"> <select name="speciality"  id="speciality'+i+'" onchange="get_services_charges(this.value,'+i+');" ><option value=""  disabled >Select Service Name </option>';  
			data +='</select>';
			data +='</td><td class="form-group">';
			data +='<input type="text" name="qty[]" id="qty'+i+'" onblur="cal_amount('+i+');" class="form-control qty" value="" >';
			data +='</td><td class="form-group"><input type="text" name="rate[]" id="rate'+i+'" class="form-control rate" onblur="cal_amount('+i+');" value="" >';
			data +='</td><td class="form-group">';
			data +='<input type="text" name="total[]" id="total'+i+'" class="form-control amt" value="" >';
			data +='</td></tr>';
			//	ddatefor();
			$('.ser').append(data);
			$('select').not('.disabled').formSelect();
			i++;	
		});
		$(".delete").on('click', function() {
			$('.case:checkbox:checked').parents("tr").remove();
			$('.check_all').prop('checked',false);
		});
		$(".check_all").on('click', function() {				
			if($('.check_all').is(":checked")==false){
				$('.chk').prop('checked',false);
				}else{
				$('.chk').prop('checked',true); 
			}
		});
		$(".chk").on('click', function() {				
			if($('.chk').is(":checked")==true){
				$('.check_all').prop('checked',false);
			}
		}); 
	}); 
	
</script>	
<script>	
	function get_filter(fval)
	{
		if(fval=='mrdNo')
		{
			$('#mrd_no').prop('readonly',false); 
			$('#contact_no').prop('readonly',true);
			
			$('#contact_no').addClass('read_only_cls');
			$('#mrd_no').removeClass('read_only_cls');
			
			
			
		} 
		else if(fval=='contact'){
			$('#mrd_no').prop('readonly',true); 
			$('#contact_no').prop('readonly',false);
			
			$('#mrd_no').addClass('read_only_cls');
			$('#contact_no').removeClass('read_only_cls');
		}
	}
	
	function get_speciality(type_id,pval)
	{
		var base_url='<?php echo base_url(); ?>';
		//var type_id=$('#type_id'+pval).val();
		console.log(type_id);
		$.ajax({
			url: base_url+'index.php/getservices/',
			type: 'post',
			data: {type_id:type_id},
			success: function(result){  
				console.log(result);
				
				$('#speciality'+pval).html(result);
				$('select').not('.disabled').formSelect();
			}
			
		});
	}
	function get_services_charges(service_id,pval)
	{
		var base_url='<?php echo base_url(); ?>'; 
		console.log(service_id);
		$.ajax({
			url: base_url+'index.php/getservicescharges/',
			type: 'post',
			data: {service_id:service_id},
			success: function(result){     
				console.log(result+"-"+pval); 
				$('#rate'+pval).val(result.trim()); 
				
			}
			
			
		});
	}
	function check_Patient()
	{ 
		var base_url='<?php echo base_url(); ?>';
		
		var mrd_no=$('#mrd_no').val();  
		var contact_no=$('#contact_no').val(); 
		if(mrd_no) {
			$.ajax({
				url: base_url+"index.php/PatientDataMRD/",
				type: "POST",
				data: { mrdNo:mrd_no,contact_no:contact_no },
				success: function(result)
				{ 
					
					if(result==0)
					{
						$("#fullname").val('');
						$("#contact_no").val('');
						$("#email").val('');
						$("#dob").val('');
						$("#age").val('');							 
						$("#patient_id").val('');
						$("#patient_type").val('');
					}
					else{
						console.log(result);
						var obj = $.parseJSON(result); 
						$.each(obj,function(key,entry){ 
							console.log(entry.fullName);
							$("#fullname").val(entry.fullName);
							$("#contact_no").val(entry.contact_no);
							$("#email").val(entry.email); 
							$("#age").val(entry.age);							 
							$("#patient_id").val(entry.patient_id); 
							$("#gender").val(entry.gender); 
							$("#address").val(entry.address); 
							
							$("#doctor_name").val(entry.tbl_doc_id); 
							$("#visit_date").val(entry.appointment_date); 
							$("#patient_type").val(entry.ptype); 
							if(entry.ptype=='Self')						
							{
								$('.insurance_panel_cls').hide();
							}
							else{
								$('.insurance_panel_cls').show();
							}
							//get_consultant_charges(entry.tbl_doctor_master_id);
						});
					}
					
				} 
			});
			
		}  
	}
	function get_consultant_charges(doc_id)
	{
		var base_url='<?php echo base_url(); ?>'; 
		console.log(doc_id);
		$.ajax({
			url: base_url+'index.php/get_consultant_charges/',
			type: 'post',
			data: {doc_id:doc_id},
			success: function(result){  
				console.log(result);
				
				console.log(result);  
				$('#service_charges').val(result.trim()); 
				
			}
			
		});
	}
	function get_total()
	{
		var subtotal=$('#subtotal').val();
		var discount=$('#discount').val();
		var t=parseFloat(subtotal) - parseFloat(discount);
		$('#final_total').val(Math.round(parseFloat(t)).toFixed(2));
		console.log(t);
	}
	function cal_amount(id)
	{
		var qty=parseFloat(document.getElementById('qty'+id).value);
		var rate=document.getElementById('rate'+id).value;
		var amount=parseFloat(qty)*parseFloat(rate);
		if(isNaN(amount))
		amount=0;
		document.getElementById('total'+id).value=amount.toFixed(2);
		cal_total_bill_values();
	}
	
	function cal_total_bill_values()
	{				
		
		var quantity	=	new Array();
		$(".qty").each(function() 
		{
			var qval = $(this).val();
			if (qval) {
				quantity.push(qval);
			} 
		});
		
		var rate	=	new Array();
		$(".rate").each(function() 
		{
			var rval = $(this).val();
			if (rval) {
				rate.push(rval);
			} 
		});
		var amt	=	new Array();
		$(".amt").each(function() 
		{
			var rval = $(this).val();
			if (rval) {
				amt.push(rval);
			} 
		});
		var amount_of_all_product = 0;
		
		for (var i  = 0; i < amt.length; i++)
		{
			
			var new_rate = amt[i];
			amount_of_all_product	+=parseFloat(amt[i]);	//parseFloat((quantity[i] * new_rate)); 
			
		} 
		if(isNaN(amount_of_all_product))
		{
			amount_of_all_product=0;
			$('#subtotal').val(amount_of_all_product);
		}
		else
		{
			$('#subtotal').val(amount_of_all_product.toFixed(2));
			$('#final_total').val(amount_of_all_product.toFixed(2));
		}
	}	
</script>  
<script>
    $(function() {
		
        $("#block-validate").validate({
			rules: {
                doctor_id: {
                    required: true
				},
				fees: {
                    required: true
				},
				app_time: {
                    required: true
				}
			},
            //For custom messages
            messages: {
                doctor_id: {
					required: "Select a Doctor"
				},
				fess: {
					required: "Enter a Fees  Details"
				},
				app_time: {
					required: "Enter a App Time  Details"
				}
			},
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
					} else {
                    error.insertAfter(element);
				}
			},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
				}
			}
		});
	});
    </script>		