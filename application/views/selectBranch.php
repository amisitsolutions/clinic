<?php 
	$this->load->view("admin_includes/head");

?> 
 

<body class="fix-menu">
    <!-- Pre-loader start -->
    <div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>
</div>
    <!-- Pre-loader end -->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row ">
                <div class="col-sm-12 login_panel">
                    <!-- Authentication card start -->
                    <form id="login_form" method="POST" class="form-horizontal form-signin" action="<?php echo base_url(); ?>index.php/branchLogin" >    <div class="text-center">
                            <!-- <img src="<?php echo base_url(); ?>\files\assets\images\logo.png" alt="logo.png"> -->
                        <h3>Clinic Management</h3>
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center txt-primary">Select Branch</h3>
                                    </div>
                                </div>
							 
                                <div class="form-group form-primary">
                                <select name="branch_id" required id="branch_id" class="form-control">
										<option value="" >Select Branch </option> 
									<?php
									foreach($Branchdata as $key=>$branch)
									{
										foreach($branch['branch_list'] as $data)
										{ 
										 
										?>
										<option value="<?php echo  $data->branchID; ?>"><?php echo  $data->branchName; ?></option>
									<?php }
									} ?>	

										
										</select>
                                    <span class="form-bar"></span>
                                </div>
                               
            
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Continue</button>
                                        
                                    </div>
                                </div>
                                <p class="text-inverse text-left">Don't have Assign Branch contact us for free!</p>
                            </div>
                        </div>
                    </form>
                        <!-- end of form -->
                    </div>
                    <!-- Authentication card end -->
                </div>
              
         
            </div>
         
    </section>
    <?php
        $this->load->view("admin_includes/footer");
    ?>
    
<script>
    $(function() {
        $("#login_form").validate({
            rules: {
				
                reg_no: { 
                    required: true
				},
                username: { 
                    required: true
				},
                password: {
                    required: true
					} ,reg_no: {
                    required: true
				}
			},
            messages: {
                reg_no: {
                    required: "Enter a Reg No."
					},
				uname: {
                    required: "Enter a username"
					},Password: {
                    required: "Enter a Password"
					},reg_no: {
                    required: "Enter a Reg No."
				}
			} ,
			errorElement : 'div',
			errorPlacement: function(error, element) {
				var placement = $(element).data('error');
				if (placement) {
					$(placement).append(error)
					} else {
					error.insertAfter(element);
				}
			}
			
		});
	});
</script>