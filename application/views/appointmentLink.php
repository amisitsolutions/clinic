<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head"); 
?>
<style>
	.col-md-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
    float: left;
	}
</style>

<!-- Sidebar inner chat end-->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		
		
		
		<body class="fix-menu">
			<!-- Pre-loader start -->
			<div class="theme-loader">
				<div class="ball-scale">
					<div class='contain'>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
						<div class="ring"><div class="frame"></div></div>
					</div>
				</div>
			</div>
			<!-- Pre-loader end -->
			<section class="login-block">
				<!-- Container-fluid starts -->
				<div class="container-fluid">
					<div class="row ">
						<div class="col-sm-9 login_panel bounceInLeft animated">
							<div class="card">
								<div class="card-header text-center">
									<h5>Appointment Details</h5>
								</div>
								<div class="card-block"> 
									<form name="block-validate" id="block-validate" autocomplete="off" action="<?php echo base_url(); ?>index.php/saveAppointmentLink" method="post"  class="bv-form">
										<input type="hidden" name="check_id" placeholder="New Password" value="<?php echo  $check_id; ?>"/>
										 <input type="hidden" name="check_flag" placeholder="New Password" value="<?php  echo  $check_flag; ?>"/>
										 <input type="hidden" name="clinic_id" placeholder="New Password" value="<?php echo  $clinic; ?>"/>	
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<div class="form-radio">
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input type="radio" checked name="appointmentType" name="Person" id="Person" value="Person"  />
																<i class="helper"></i>Person 
															</label>
														</div>
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input type="radio" name="appointmentType" name="Online" id="Online" value="Online"   />
																<i class="helper"></i>Online
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6"> 
												<div class="form-group">
														<label class="col-sm-4 col-form-label">Speciality <span  class="required"> * </span> </label>	
														
													<div class="form-group">
													<select name="branch_id" required class="form-control " id="branch_id" >
														<option value="" disabled  >Select Branch </option>
														<?php
															for ($j = 0; $j < count($branch_list); $j++) {
															?>
															<option value="<?php echo $branch_list[$j]->branchID; ?>"><?php echo $branch_list[$j]->branchName; ?></option>
														<?php } ?>	
														
														
													</select>
												</div>
												</div>
											</div>	
										</div>	
										<div class="row">
											<div class="col-sm-6">
												<label class="col-sm-4 col-form-label">Speciality <span  class="required"> * </span> </label>
												<div class="form-group">
													<select name="speciality" required  class="form-control" id="speciality" onchange="get_speciality(this.value);">
														<option value=""  disabled selected>Select Speciality </option>
														<?php
															for ($i = 0; $i < count($spec_data); $i++) {
															?>
															<option value="<?php echo $spec_data[$i]->specialityID; ?>"><?php echo $spec_data[$i]->specialityName; ?></option>
														<?php } ?>	
														
													</select> 
												</div>
											</div>
											<div class="col-sm-6">
												<label>Doctor <span  class="required"> * </span> </label> 
												<div class="form-group">
													
													<select name="doctor_id" required class="form-control"  id="doctor_id"  > 
														
													</select> 
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">	 
											<label>Appointment Date <span  class="required"> * </span> </label> 
												<div class="form-group">
													<input name="app_date"  required class="form-control datepicker" id="app_date" onchange="checkDoctorAvailbility();" placeholder="Appointmente Date" type="text" required=""   >
													
													
												</div>
											</div>
											<div class="col-sm-6">
												<label>Appointment Date <span  class="required"> * </span> </label>
												<div class="form-group">
												 
													<input name="app_time"  required class="form-control timePicker" id="app_time"  placeholder="Appointmente Date" type="text" required=""   >
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
											<label>Patient Type<span  class="required"> * </span> </label>
												<div class="form-group"> 
													<div class="form-radio">
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input type="radio" name="patient_type" value="New" data-bv-field="member">
																<i class="helper"></i>New 
															</label>
														</div>
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input type="radio" name="patient_type" value="Existing" data-bv-field="member">
																<i class="helper"></i>Existing
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6  mrd_panel" style="display:none;">
											<label>MRD No. <span  class="required"> * </span> </label>
												<div class="form-group">
													 
													<input name="mrd_no" id="mrd_no"  class="form-control"  placeholder="New mrd No" type="text" onchange="check_Patient();"     >
													
												</div>
											</div>
										</div>
										<input id="patient_id"  class="form-control"  name="patient_id"   type="hidden">
										
										<div class="row">
											<div class="col-sm-6">
											<label>Full Name <span  class="required"> * </span> </label>
												<div class="form-group">
													 
													<input id="fullname"  class="form-control"   placeholder="Enter Full Name" name="fullname" required  type="text">
												</div>
											</div>
											<div class="col-sm-6">
											<label>Email ID  </label>
												<div class="form-group">
												<input id="email"   class="form-control"  placeholder="Enter Email" name="email" type="text">
												
											</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<label>Contact No. <span  class="required"> * </span> </label>
												<div class="form-group">
													<input id="contact_no"  class="form-control"  placeholder="Enter Contact No."  onkeypress="return isNumber(event);" name="contact_no" onchange="getdata();" required maxlength="10" minlength="10" type="text">
												</div>
											</div>
											<div class="col-sm-6"><label>Gender <span  class="required"> * </span> </label>
												<div class="form-group"> 
													<div class="form-radio">
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input class="form-check-input" type="radio" name="gender" id="gender-1" value="Male">
																<i class="helper"></i>Male 
															</label>
														</div>
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input class="form-check-input" type="radio" name="gender" id="gender-2" value="Female"> 
																<i class="helper"></i>Female
															</label>
														</div>
													</div>
												</div>
												
											</div>
											</div>	<div class="row">
											<div class="col-sm-6"><label>DOB <span  class="required"> * </span> </label>
												<div class="form-group">
													 
													<input id="dob"   class="form-control dobdatepicker"  placeholder="Select DOB"  name="dob" required onchange="getAge();"   type="text">
												</div>
											</div>
											<div class="col-sm-6">	<label>Age</label>
												<div class="form-group">
												
												<input id="age"    class="form-control"  placeholder="Age" readonly name="age" type="text">
												
											</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
											<label>Remarks </label>
												<div class="form-group">
												<textarea id="remarks" id="remarks"  placeholder="Enter Remarks" class="form-control"  ></textarea>
												
											</div>
											</div>
										</div>
										
										<div class="row online_panel m-t-30 m-b-20" style="display:none;">
											<div class="col-sm-6"><input type="radio"   id="registration_for_neft" name="payment_type" value="other" checked="">
											<label class="col-sm-4 col-form-label"> NEFT/RTGS/IMPS <span  class="required"> * </span> </label>
												<input type="radio"   id="registration_for_online" name="payment_type" value="online"  onclick="payment_method('online');">  &nbsp; Online   &nbsp; 
												
												<div class="form-group">
															<input type="text" class="form-control" placeholder="Enter Ref No.">
												</div>
												
											</div>
											
										</div> 
									</div>
									<div class="row m-b-20">
										<div class="col-sm-12">
											<div class="text-center m-t-20">
												<button type="submit"  name="btn_add"  class="btn btn-primary waves-effect waves-light m-r-10">Submit
												</button>
												
											</div>
										</div>
									</div>
								</form>
								
							</div>
						</div> 
						
						
						<?php
						//	$this->load->view("admin_includes/footer");
						?>				
					</div> 
					<script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery\js\jquery.min.js"></script>
					<script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\popper.js\js\popper.min.js"></script>
    
	<script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\bootstrap\js\bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>\files\assets\js\jquery.validate.min.js"></script>

<link href="<?php echo base_url(); ?>\files\assets\js\datepicker.css" rel="stylesheet" type="text/css" />  
<script src="<?php echo base_url(); ?>\files\assets\js\bootstrap-datepicker.js"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\js\script.min.js"></script>
 
					<script type="text/javascript">
						$(document).ready(function() {	
								
		$('.timePicker').timepicker({
    timeFormat: 'h:i A',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
		 
							$('.datepicker').datepicker({format: 'yyyy-mm-dd'}).on('changeDate',  function (ev) {	
								$(this).datepicker('hide');
							});
							$('.dobdatepicker').datepicker({format: 'yyyy-mm-dd'}).on('changeDate',  function (ev) {	
								$(this).datepicker('hide');
							});	
						});	
					</script>   
					<script>
						$(function() {
							$.validator.setDefaults({
								ignore: []
							});
							$("#block-validate").validate({
								rules: {
									branch_id: {
										required: true
									},
									speciality: {
										required: true
									},
									doctor_id: {
										required: true
									},  
									app_date: {
										required: true
									},
									app_time: {
										required: true
									}, 
									fullname: {
										required: true
									},
									
									dob: {
										required: true 
									},  
									contact_no: {
										required: true,
										minlength: 10,
										required: true
									} 
								},
								//For custom messages
								messages: {
									branch_id: {
										required: "Select Branch"
									},
									speciality: {
										required: "Select Speciality"
									},
									doctor_id: {
										required: "Select Doctor Name"
									},  
									app_date: {
										required: "Enter Date"
									},
									app_time: {
										required: "Select time"
									}, 
									fullname: {
										required: "Enter a Full Name"
									}, 
									dob: {
										required: "Enter a DOB"
									},   
									contact_no: {
										required: "Enter a Contact No."
									}
									
								},
								errorElement: 'div',
								errorPlacement: function(error, element) {
									var placement = $(element).data('error');
									if (placement) {
										$(placement).append(error)
										} else {
										error.insertAfter(element);
									}
								},
								invalidHandler: function(e, validator) {
									var errors = validator.numberOfInvalids();
									if (errors) {
										$('.error-alert-bar').show();
									}
								},
							});
						});
					</script>		
					<script> 
						$(document).ready(function () {
							$('input[type=radio][name=patient_type]').change(function() {
								if (this.value == 'New') {
									$('.mrd_panel').hide();
								}
								else if (this.value == 'Existing') {
									$('.mrd_panel').show();
								}
							});
							
							$('input[type=radio][name=appointmentType]').change(function() {
								if (this.value == 'Person') {
									$('.online_panel').hide();
								}
								else if (this.value == 'Online') {
									$('.online_panel').show();
								}
							});
						}); 
					</script>
					<script>
						function getAge() {
							var DOB=$('#dob').val(); 
							var today = new Date();
							
							console.log(today+DOB);
							var birthDate = new Date(DOB);
							
							var age = today.getFullYear() - birthDate.getFullYear();
							var m = today.getMonth() - birthDate.getMonth();
							if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
								age = age - 1;
							}
							
							$('#age').val(age);
						}
						function get_speciality(sp_id)
						{ 
							var base_url='<?php echo base_url(); ?>';
							
							$.ajax({
								url: base_url+"index.php/getDoctorsByspecSess/",
								type: "POST",
								data: {  sp_id:sp_id },
								success: function(res)
								{
									console.log(res);
									$('#doctor_id').html(res);
									
								}	 
							});
							
						}
						function checkavailability()
						{ 
							var doctor_id=$('#doctor_id').val(); 
							var app_date=$('#app_date').val(); 
							var app_time=$('#app_time').val(); 
							var base_url='<?php echo base_url(); ?>'; 
							
							$.ajax({
								url: base_url+"index.php/AppointmentAvailability/",
								type: "POST",
								data: { doctor_id:doctor_id, app_date:app_date, app_time:app_time },
								success: function(result)
								{ 
									if(result==0)
									{
										$('#error_text').html('Time Slot Is not Available');
										$('#succ_text').html('');
										$('#btn_add').Attr("disabled");
									}
									else
									{
										$('#succ_text').html('Time Slot Is Available');
										$('#error_text').html('');
										$('#btn_add').removeAttr("disabled");
									}
									
								}	 
							});
							
						} 
						
					/* 	function checkDoctorAvailbility()
						{ 
							var doctor_id=$('#doctor_id').val();   
							var app_date=$('#app_date').val(); 
							var base_url='<?php echo base_url(); ?>'; 
							
							$.ajax({
								url: base_url+"index.php/timingAvailability/",
								type: "POST",
								data: { doctor_id:doctor_id,app_date:app_date},
								success: function(result)
								{ 
									console.log(result);
									$('#app_time').html(result);
									
								}	 
							});
							
						} */
						function check_Patient()
						{ 
							var base_url='<?php echo base_url(); ?>';
							
							var mrd_no=$('#mrd_no').val(); 
							if(mrd_no) {
								$.ajax({
									url: base_url+"index.php/getPatientDataMRD/",
									type: "POST",
									data: { mrdNo:mrd_no },
									success: function(result)
									{ 
										console.log(result);
										if(result==0)
										{
											$("#fullname").val('');
											$("#contact_no").val('');
											$("#email").val('');
											$("#dob").val('');
											$("#age").val('');							 
											$("#patient_id").val('');
										}
										else{
											
											var res=result.split("#"); 
											$("#fullname").val(res[0].trim());
											$("#contact_no").val(res[1].trim());
											$("#email").val(res[2].trim());
											$("#dob").val(res[3].trim());
											$("#age").val(res[4].trim());							 
											$("#patient_id").val(res[5].trim()); 
										}
										
									} 
								});
								
							}  
						}
					</script>																																