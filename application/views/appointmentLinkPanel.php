<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
<style>
	.col-md-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
    float: left;
}
	</style>

	
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Appointment Details</h5>
                                                 </div>
                                                <div class="card-block"> 
                                                   		<form name="block-validate" id="block-validate" autocomplete="off" action="<?php echo base_url(); ?>index.php/saveAppointmentLinkPanel" method="post"  class="bv-form"> 
													<div class="col-md-6">
                                                  			  <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Speciality <span  class="required"> * </span> </label>
                                                                <div class="col-sm-8">
                                                                <select name="speciality"  class="form-control" id="speciality" onchange="get_speciality(this.value);">
																	<option value=""  disabled selected>Select Speciality </option>
																	<?php
																		for ($i = 0; $i < count($spec_data); $i++) {
																		?>
																		<option value="<?php echo $spec_data[$i]->specialityID; ?>"><?php echo $spec_data[$i]->specialityName; ?></option>
																	<?php } ?>	
																	
																</select> <span class="messages"></span>
                                                                </div>
															</div>
														
															<div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Date <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                 	<input name="app_date"  class="form-control datepicker" id="app_date" onchange="checkDoctorAvailbility();" placeholder="Appointmente Date" type="text" required=""   >
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
															  
														 </div>
                                                            
															<div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Doctor Name <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                <select name="doctor_id"  class="form-control"  id="doctor_id"  > 
																	 
																</select>  <span class="messages"></span>
                                                                </div>
															</div>
															<div class="form-group row" style="margin-bottom: 2em;">
                                                                <label class="col-sm-4 col-form-label">Time <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                  <select name="app_time"  class="form-control"  id="app_time" onchange="checkavailability();" > 
																<option value=""  disabled selected>Select time Slot  </option>
																</select> 
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            
								</div> 
													<div>					
														 <div class="form-group row">
                                                                <label class="col-sm-2">Patient Type</label>
                                                                <div class="col-sm-8">
                                                                    <div class="form-radio">
                                                                        <div class="radio radiofill radio-primary radio-inline">
                                                                            <label>
                                                        <input type="radio" name="patient_type" value="New" data-bv-field="member">
                                                        <i class="helper"></i>New 
                                                    </label>
                                                                        </div>
                                                                        <div class="radio radiofill radio-primary radio-inline">
                                                                            <label>
                                                        <input type="radio" name="patient_type" value="Existing" data-bv-field="member">
                                                        <i class="helper"></i>Existing
                                                    </label>
                                                                        </div>
                                                                    </div>
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
														<div class="form-group row mrd_panel" style="display:none;">
                                                                <label class="col-sm-2 col-form-label">MRD No. <span  class="required"> * </span></label>
                                                                <div class="col-sm-4">
                                                                   <input name="mrd_no" id="mrd_no"  class="form-control"  placeholder="New mrd No" type="text" onchange="check_Patient();"     >
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
                                                            
															</div>
                                                            <input id="patient_id"  class="form-control"  name="patient_id"   type="hidden">
                                                                 
															<div class="row">	
															<div class="col-md-6">
															<div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Full Name <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                   <input id="fullname"  class="form-control"  name="fullname" required  type="text">
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
															<div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Email <span  class="required"> * </span> </label>
                                                                <div class="col-sm-8">
                                                                    <input id="email"   class="form-control"   name="email" type="text">
                                                                    <span class="messages"></span>
                                                                </div>
															</div>

															<div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Contact No. <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                  <input id="contact_no"  class="form-control"   onkeypress="return isNumber(event);" name="contact_no" onchange="getdata();" required maxlength="10" minlength="10" type="text">
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div class="col-md-6">
															<div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Gender <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                    <div class="form-check form-check-inline">
                                                                        <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="gender" id="gender-1" value="Male"> Male
                                                </label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="gender" id="gender-2" value="Female"> Female
                                                </label>
                                                                    </div>
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">DOB <span  class="required"> * </span></label>
                                                                <div class="col-sm-8">
                                                                    <input id="dob"   class="form-control dobdatepicker"  name="dob" required onchange="getAge();"   type="text">
                                                                    <span class="messages"></span>
                                                                </div>
															</div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label">Age</label>
                                                                <div class="col-sm-8">
                                                                   <input id="age"    class="form-control"  readonly name="age" type="text">
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            
                                                            </div>
												</div>
												<div>	
                                                         
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Remaks</label>
                                                                <div class="col-sm-8">
                                                                   <textarea id="remarks" id="remarks"  class="form-control"  ></textarea>
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-2"></label>
                                                                <div  >
                                                                  <input   type="submit" name="btn_add"  class="btn btn-primary pull-right"  id="btn_add" value="Submit">
															
                                                                </div>
                                                            </div>
                                                            </div>
                                                
														</form>
 
</div>
</div> 
                               
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 
 
<script type="text/javascript">
			$(document).ready(function() {	
			 
				$('.datepicker').datepicker({format: 'yyyy-mm-dd'}).on('changeDate',  function (ev) {	
					$(this).datepicker('hide');
				});
				$('.dobdatepicker').datepicker({format: 'yyyy-mm-dd'}).on('changeDate',  function (ev) {	
					$(this).datepicker('hide');
				});	
				});	
</script>   
 <script>
    $(function() {
	$.validator.setDefaults({
				ignore: []
			});
       $("#block-validate").validate({
            rules: {
                speciality: {
                    required: true
                },
                 doctor_id: {
                    required: true
                },  
 				 app_date: {
                    required: true
                },
                /*  app_time: {
                    required: true
                }, */
                 fullname: {
                    required: true
                },
                  
                 dob: {
                    required: true 
                },  
                contact_no: {
                    required: true,
                    minlength: 10,
				required: true
                } 
            },
            //For custom messages
            messages: {
			 speciality: {
                    required: "Select Speciality"
                },
                 doctor_id: {
                    required: "Select Doctor Name"
                },  
 				 app_date: {
                    required: "Enter Date"
                },
                /*  app_time: {
                   required: "Select time"
                }, */
                fullname: {
					required: "Enter a Full Name"
                }, 
				  dob: {
					required: "Enter a DOB"
                },   
				contact_no: {
                    required: "Enter a Contact No."
                }
				 
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            },
        });
    });
    </script>		
<script> 
    $(document).ready(function () {
        $('input[type=radio][name=patient_type]').change(function() {
		 	if (this.value == 'New') {
					$('.mrd_panel').hide();
			}
			else if (this.value == 'Existing') {
				$('.mrd_panel').show();
			}
		});
    }); 
</script>
<script>
	function getAge() {
	var DOB=$('#dob').val(); 
    var today = new Date();
	
 	 console.log(today+DOB);
    var birthDate = new Date(DOB);
		
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }

    $('#age').val(age);
}
	function get_speciality(sp_id)
	{ 
		var base_url='<?php echo base_url(); ?>';
	  
			$.ajax({
				url: base_url+"index.php/getDoctorsByspecSess/",
				type: "POST",
				data: {  sp_id:sp_id },
				success: function(res)
				{
			 	console.log(res);
					$('#doctor_id').html(res);
					    
				}	 
				});
                 
		}
  function checkavailability()
	{ 
		var doctor_id=$('#doctor_id').val(); 
		var app_date=$('#app_date').val(); 
		var app_time=$('#app_time').val(); 
		var base_url='<?php echo base_url(); ?>'; 
 
			$.ajax({
				url: base_url+"index.php/AppointmentAvailability/",
				type: "POST",
				data: { doctor_id:doctor_id, app_date:app_date, app_time:app_time },
				success: function(result)
				{ 
					if(result==0)
					{
						$('#error_text').html('Time Slot Is not Available');
						$('#succ_text').html('');
						$('#btn_add').Attr("disabled");
					}
					else
					{
						$('#succ_text').html('Time Slot Is Available');
						$('#error_text').html('');
						$('#btn_add').removeAttr("disabled");
					}
					
				}	 
				});
                  
		} 
		
	/* 	function checkDoctorAvailbility()
	{ 
		var doctor_id=$('#doctor_id').val();   
		var app_date=$('#app_date').val(); 
		var base_url='<?php echo base_url(); ?>'; 
 
			$.ajax({
				url: base_url+"index.php/timingAvailability/",
				type: "POST",
				data: { doctor_id:doctor_id,app_date:app_date},
				success: function(result)
				{ 
				  console.log(result);
					$('#app_time').html(result);
					   
				}	 
				});
                  
		} */
 function check_Patient()
	{ 
		var base_url='<?php echo base_url(); ?>';
		 
		var mrd_no=$('#mrd_no').val(); 
            if(mrd_no) {
					$.ajax({
					url: base_url+"index.php/getPatientDataMRD/",
					type: "POST",
					data: { mrdNo:mrd_no },
					success: function(result)
					{ 
					console.log(result);
						if(result==0)
						{
							$("#fullname").val('');
							$("#contact_no").val('');
							$("#email").val('');
							$("#dob").val('');
							$("#age").val('');							 
							$("#patient_id").val('');
						}
						else{
						
						 	var res=result.split("#"); 
							$("#fullname").val(res[0].trim());
							$("#contact_no").val(res[1].trim());
							$("#email").val(res[2].trim());
							$("#dob").val(res[3].trim());
							$("#age").val(res[4].trim());							 
							$("#patient_id").val(res[5].trim()); 
						}
						 
					} 
				});
                
            }  
	}
	</script>	