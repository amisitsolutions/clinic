<?php		defined('BASEPATH') OR exit('No direct script access allowed');	$this->load->view("admin_includes/head"); ?>	   
<div class="page-wrapper">            
	<div class="container-fluid">           
		<div class="row">                   
			<div class="col s12 l6">        
				<div class="card">          
					<div class="card-content">	
						<div class="modal-body modal-body-sub">
							<div class="row">		
								<div class="col-md-12 modal_body_left modal_body_left1" style="padding-right:3em;">					
									<div class="forgot_pwd_panel" >		
										<div class="control-group text-center"> <h3>
										<img src="<?php echo base_url(); ?>assets/images/logo-text.png" style=" width:55%; margin: 0 auto;" alt="Logo" /></h3></div>
										<h2 class="resp-accordion" role="tab" aria-controls="tab_item-7"><span class="resp-arrow"></span></h2><div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-3" style="display:block">			
											<div class="facts">		
												<div class="register">		
													<form name="rest_passwordform" id="rest_passwordform" action="<?php echo base_url(); ?>index.php/Login/do_reset_password" method="post"  class="bv-form">
														<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>		
														<p class="normal_text">Change New Password </p>				
														<input type="hidden" name="check_id" placeholder="New Password" value="<?php echo  $check_id; ?>"/>
														
														<input type="hidden" name="check_flag" placeholder="New Password" value="<?php echo  $check_flag; ?>"/>
														
														<input type="hidden" name="clinic_id" placeholder="New Password" value="<?php echo  $clinic; ?>"/>						
														<div class="col s12 m6 l12">							
															<div class="input-field">							
																<input type="text" id="clinic_username" required name="clinic_username">					
																<label for="bio">Username: *	</label>																
																<p> 	(Login ID you enter, lets you sign in to Clinical Establishment Application.)</p>						
															</div>					
														</div>			
														<div class="col s12 m6 l12">				
															<div class="input-field">				
																<label for="bio">Password: *	</label>
																<input name="rest_new_password" id="rest_new_password" placeholder="New Password" type="password" required="" data-bv-field="rest_new_password">											
															</div>
														</div>			
														<div class="col s12 m6 l12">			
															<div class="input-field">											
																<label for="bio">Confirm Password: *	</label>	
																<input name="rest_confirm_password" id="rest_confirm_password" placeholder="Confirm Password" type="password" required="" data-bv-field="rest_confirm_password">								
															</div>			
														</div>										
														<div class="sign-up form-group">			
															<input type="submit" name="reset_pwd_btn" id="reset_pwd_btn" value="Submit"><a href="<?php echo base_url(); ?>" id="back_tologin" class="btn btn-info pull-right" name="back_tologin" style="margin: 2em 0 0;"> Back to login</a>	
														</div>									
													</form>			
												</div>		
											</div> 	
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>       
	</div> <script src="<?php echo base_url(); ?>
	
	
	assets/libs/jquery/dist/jquery.min.js"></script>  
	<script src="<?php echo base_url(); ?>dist/js/materialize.min.js"></script>  
	<script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script> 
	
	<script src="<?php echo base_url(); ?>dist/js/app.js"></script> <script src="<?php echo base_url(); ?>dist/js/app.init.js"></script>
	
	<script src="<?php echo base_url(); ?>dist/js/app-style-switcher.js"></script> <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script> 
	
	<script src="<?php echo base_url(); ?>assets/extra-libs/prism/prism.js"></script>
	
	<script src="<?php echo base_url(); ?>dist/js/pages/forms/jquery.validate.min.js"></script>	
</div>
<script>  
	
	$(function() {    
		$("#rest_passwordform").validate({      		
			
			rules: {        
				
				rest_new_password: {   	
					required: true      
				},              
				rest_confirm_password: {     
					required: true,			
					equalTo: "#rest_new_password" 
				}         		
			},     		
			
			messages: {  
				rest_new_password: {
					required: "Enter a Password "     
				}, 							
				rest_confirm_password: {		
					required: "Enter a Password"   
				}         		
			}, 
			
			errorElement: 'div', 
			errorPlacement: function(error, element) {  
				var placement = $(element).data('error'); 
				if (placement) {              
					$(placement).append(error) 		
					} else {             		
					error.insertAfter(element); 
				}        
			},      		
			
			invalidHandler: function(e, validator) {	
				var errors = validator.numberOfInvalids();
				if (errors) {               
					$('.error-alert-bar').show();  	
				}         			
			}     
		});	
	}); 
</script>			