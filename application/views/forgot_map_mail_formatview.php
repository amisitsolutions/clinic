<!DOCTYPE html>
<html>

	<body>
		
<p>Hello, </p> </br>

<p>We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.</p></br>

<p>Click the link below to reset your password:</p>

<p><b>Password Reset Link : </b> <a href="<?php	echo $url_link=base_url().'index.php/Login/forgot_password_reset_password/'.$chkdata; ?>" > Click Here </a></p></br>

<p>If you did not request to have your password reset you can safely ignore this email. Rest assured your customer account is safe.</p></br>

<p>If clicking the link does not seem to work, you can copy and paste the link into your browsers address window, or retype it there.</p></br>
 </br>
	
</body>
</html>