<?php		defined('BASEPATH') OR exit('No direct script access allowed');
	 $this->load->view("admin_includes/head"); ?>	   

<body class="fix-menu">
    <!-- Pre-loader start -->
    <div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>
</div>
    <!-- Pre-loader end -->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row ">
                <div class="col-sm-12 login_panel">
                    <!-- Authentication card start -->
                    <form id="login_form" method="POST" class="form-horizontal form-signin" action="<?php echo base_url(); ?>index.php/checkLogin" >
                        <div class="text-center">
                            <!-- <img src="<?php echo base_url(); ?>\files\assets\images\logo.png" alt="logo.png"> -->
                        <h3>Clinic Management</h3>
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center txt-primary">Reset Password</h3>
                                    </div>
                                </div>
								<div class="col-md-12">
                                        <h3 class="text-center"><i class="feather icon-lock text-primary f-60 p-t-15 p-b-20 d-block"></i></h3>
                                    </div>
													<form name="rest_passwordform" id="rest_passwordform" action="<?php echo base_url(); ?>index.php/Login/do_reset_password" method="post"  class="bv-form">
													 <input type="hidden" name="check_id" placeholder="New Password" value="<?php //echo  $log_user_id; ?>"/>
														
														<input type="hidden" name="check_flag" placeholder="New Password" value="<?php // echo  $log_type; ?>"/>
														
														<input type="hidden" name="clinic_id" placeholder="New Password" value="<?php //echo  $clinic_id; ?>"/>	
													 
                                <div class="form-group form-primary">
                                    <input type="text" name="client_username" id="client_username" class="form-control" required="" placeholder="Username">
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" name="reset_password" id="reset_password" class="form-control" required="" placeholder="Password">
									  <div class="checkbox-fade fade-in-primary">
                                              <label>
                                                            <input type="checkbox" id="show-pass">
                                                            <span class="cr">
                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                    </span> <span>Show password</span>
                                                        </label>
                                                                            </div>
                                    <span class="form-bar"></span>
                                </div>   <div class="form-group form-primary">
                                    <input type="password" name="reset_confirm_password" id="reset_confirm_password" class="form-control" required="" placeholder="Password">
									  <div class="checkbox-fade fade-in-primary">
                                              <label>
                                                            <input type="checkbox" id="show-pass">
                                                            <span class="cr">
                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                    </span> <span>Show password</span>
                                                        </label>
                                                                            </div>
                                    <span class="form-bar"></span>
                                </div>
														 <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" id="reset_pwd_btn" name="reset_pwd_btn" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">LOGIN</button>
                                        <a href="<?php echo base_url(); ?>" id="back_tologin" class="btn btn-info pull-right" name="back_tologin" style="margin: 2em 0 0;"> Back to login</a>	
                                    </div>
                                </div>
														 								
													</form>			
												</div>		
											</div> 	
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>       
	</div> 
	
	<?php		 
	 $this->load->view("admin_includes/footer"); ?>
	 
	 
<script>  
	
	$(function() {    
		$("#rest_passwordform").validate({      		
			
			rules: {        
				
				rest_new_password: {   	
					required: true      
				},              
				rest_confirm_password: {     
					required: true,			
					equalTo: "#rest_new_password" 
				}         		
			},     		
			
			messages: {  
				rest_new_password: {
					required: "Enter a Password "     
				}, 							
				rest_confirm_password: {		
					required: "Enter a Password"   
				}         		
			}, 
			
			errorElement: 'div', 
			errorPlacement: function(error, element) {  
				var placement = $(element).data('error'); 
				if (placement) {              
					$(placement).append(error) 		
					} else {             		
					error.insertAfter(element); 
				}        
			},      		
			
			invalidHandler: function(e, validator) {	
				var errors = validator.numberOfInvalids();
				if (errors) {               
					$('.error-alert-bar').show();  	
				}         			
			}     
		});	
		
		 $("#show-pass").on("change",function(){$("#show-pass").is(":checked")?$("#password").attr("type","text"):$("#password").attr("type","password")})
	}); 
</script>			