<!DOCTYPE html>
<html>

	<body>
	
		
<p>Hello, </p> </br>

<p>We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.</p></br>

<p>Click the link below to reset your password:</p>

<p><b>Password Reset Link : </b> <a href="<?php	$url_link=base_url().'index.php/Login/reset_password_enter_password/'.$data['reset_key']; ?>" > Click Here </a></p></br>

<p>If you did not request to have your password reset you can safely ignore this email. Rest assured your customer account is safe.</p></br>

<p>If clicking the link does not seem to work, you can copy and paste the link into your browsers address window, or retype it there.</p></br>

<p>AppEat will never e-mail you and ask you to disclose or verify your AppEat Account password, credit card, or banking account number. If you receive a suspicious e-mail with a link to update your account information, do not click on the link.</p></br>
	
</body>
</html>