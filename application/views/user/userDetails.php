<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");

?> 
  
<div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>User Details
													
													</h5><a href="<?php echo base_url(); ?>index.php/userRegistration" class="btn btn-sm  btn-primary pull-right">
								 <i class="material-icons"> </i>  User Registration </a>
                                                 </div>
                                                <div class="card-block"> 
									
						 
							<div class="dt-responsive table-responsive">
                                                            <table id="dom-table" class="table table-striped table-bordered nowrap">
                                
                                    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Name</th>
                                       
                                            <th>Email/Contact</th>  
                                            <th>Qualification</th>
											<th>Department</th>                                 
											<th>Reg date</th> 
								
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cnt = 1;
	 				
									for ($i = 0; $i < count($userData);$i++) {
						 			?>
									
									<tr class="odd gradeX">
									<td><?php echo $cnt++; ?></td>
                                        <td><?php echo $userData[$i]->userName; ?></td>
                                       
                                        <td><?php echo $userData[$i]->userEmail."</br>". $userData[$i]->userContact; ?></td>
                                      	   <td><?php echo $userData[$i]->userQualification; ?></td>
                                          <td><?php echo $userData[$i]->roleName; ?></td>
                                        
									   <td><?php echo date("d-M-Y",strtotime($userData[$i]->regDate)); ?></td>
                                      
                                      
									  <td>
									  <div class="btn-group btn-group-sm" style="float: none;">
                                        <a href="<?php echo base_url('index.php/editUser/' . $userData[$i]->userID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
									 
								 
										<a onclick="delete_data('<?php echo $userData[$i]->userID; ?>');"  class="tabledit-delete-button btn btn-danger waves-effect waves-light " > <span class="icofont icofont-ui-delete"></span></a>
										</td>
										</div>
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
                      
					</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
<?php
	$this->load->view("admin_includes/footer");
?>				
</div><!-- ./wrapper -->	
 

	<script>	
		 function delete_data(deleteid){	
			bootbox.confirm("Are you sure,You want to Delete", function (msg) {
					 if (msg == true) {
							location='<?php echo base_url('index.php/deleteUser/'); ?>'+deleteid;
						}
						
					}); 
				 
		 }
		
</script>