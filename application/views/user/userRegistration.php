<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
<style>
	.col-md-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
    float: left;
	}
</style>


<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12"> 
							<div class="card">
								<div class="card-header">
									<h5>User Details</h5>
								</div>
								<div class="card-block"> 
									<form action="<?php echo base_url(); ?>index.php/saveUserRegistration" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
										<input id="tbl_user_id" name="tbl_user_id"  value="<?php if(!empty($edit_data)) { echo $edit_data[0]['userID']; } ?>"  type="hidden">
										
										
										<h5 class="card-header-text">About Me</h5>
										<div class="row m-t-30">
											<div class="col-md-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Full Name  <span  class="required"> * </span></label>
													<div class="col-sm-8">
														<input id="fullname" class="form-control" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['userName']; } ?>"  name="fullname" required  type="text">
														
														<span class="messages"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Email <span  class="required"> * </span></label>
													<div class="col-sm-8">
														<input id="email" required  class="form-control" name="email" type="text"  onchange="check_if_exists_user('email');" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['userEmail']; } ?>"  >
														<span class="messages"></span>
													</div>
												</div> 
												<div class="form-group row">
													<label class="col-sm-4">Gender</label>
													<div class="col-sm-8">
														<div class="form-radio">
															<div class="radio radiofill radio-primary radio-inline">
																<label>
																	<input <?php if(!empty($edit_data)) {  if($edit_data[0]['userGender']=='Male') { echo "selected"; } } ?> class="with-gap" name="gender" value="Male" type="radio" checked="">
																	<i class="helper"></i>Male 
																</label>
															</div>
															<div class="radio radiofill radio-primary radio-inline">
																<label>
																	<input <?php if(!empty($edit_data)) { if($edit_data[0]['userGender']=='Female') { echo "selected"; } } ?> class="with-gap" name="gender" value="Female" type="radio" checked="">
																	<i class="helper"></i>Female
																</label>
															</div>
														</div>
														<span class="messages"></span>
													</div>
												</div>
												
											</div> 
											<div class="col-md-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Contact Number <span  class="required"> * </span></label>
													<div class="col-sm-8">
														<input id="contact_no"  class="form-control" onkeypress="return isNumber(event);" onchange="check_if_exists_user('contact');" name="contact_no" required maxlength="10" minlength="10" type="text" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['userContact']; } ?>" >   <span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Address</label>
													<div class="col-sm-8">
														<textarea id="user_address" name="user_address"  class="form-control"  class="materialize-textarea" ><?php if(!empty($edit_data)) { echo $edit_data[0]['userAddress']; } ?></textarea>
														<span class="messages"></span>
													</div>
												</div>
												
												
											</div>
											 
										<div class="col-md-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Select Branch <span  class="required"> * </span> </label>
												<div class="col-sm-8">
													<select name="branch_id[]" required id="branch_id" class="js-example-placeholder-multiple  form-control" multiple>
														<option value="" disabled  >Select Branch </option>
														<?php
															$barr=explode(",",$edit_data[0]['branchID']);
															for ($i = 0; $i < count($branch_list); ++$i) {
															?>
															<option  <?php if(!empty($edit_data)) {  if(in_array($branch_list[$i]->branchID,$barr)) { echo "selected"; } }  ?>    value="<?php echo $branch_list[$i]->branchID; ?>"><?php echo $branch_list[$i]->branchName; ?></option>
														<?php } ?>	
														
														
													</select>
													<span class="messages"></span>
												</div>
												</div>
												<div class="form-group row">
												<label class="col-sm-4 col-form-label">Department <span  class="required"> * </span> </label>
												<div class="col-sm-8">
													<select name="role_id" class="form-control" required id="role_id">
														<option value="" disabled>Select Department </option>
														<?php
															for ($k = 0; $k < count($role_list); ++$k) {
															?>
															<option  <?php if(!empty($edit_data)) {  if($role_list[$k]->roleID==$edit_data[0]['roleID']) { echo "selected"; } }  ?>   value="<?php echo $role_list[$k]->roleID; ?>"><?php echo $role_list[$k]->roleName; ?></option>
														<?php } ?>	
														
														
													</select>
													<span class="messages"></span>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">ID Proof File </label>
												<div class="col-sm-8">
													<input class="file-path validate" name="user_id_proof" id="user_id_proof" type="file" placeholder="Choose File Here">
													<span class="messages"></span>
												</div>
											</div>
											
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Photo File</label>
												<div class="col-sm-8">
													<input class="file-path validate" name="photo_file" id="photo_file" type="file" placeholder="Choose File Here">
													<span class="messages"></span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group row">
													<label class="col-sm-4 col-form-label">Qualification</label>
													<div class="col-sm-8">
														<input id="qua" class="form-control"  value="<?php if(!empty($edit_data)) { echo $edit_data[0]['userQualification']; } ?>"  required name="qua" type="text">
														<span class="messages"></span>
													</div>
												</div>
											
										</div>
										
									</div>
									
									<div class="col-md-12">
										
										<div class="row">
											<label class="col-sm-2"></label>
											<div class="col-sm-6">
												<?php if(!empty($edit_data)) {
												?>
												<button class="btn btn-primary " type="submit" name="update_user" value="update_user">Update</button> 
												<?php } else { ?>
												<button class="btn btn-primary" type="submit" name="add_user" value="add_user">Submit
												</button>
												<?php } ?>
											</div>
										</div>
										
									</form>
								</div>
							</div>
						</div> 
						
					</div>
				</div>
			</div>			
			
		</div>
	</div>
</div> 
</div>

<?php
	$this->load->view("admin_includes/footer");
?>				
</div> 

<script>
	$(function() {
		$(".js-example-placeholder-multiple").select2({
			placeholder: "Select Branch"
		});
		$.validator.setDefaults({
			ignore: []
		});
		$("#block-validate").validate({
			rules: {
				fullname: {
					required: true
				},
				age: {
					required: true,					
					minlength: 2
				},
				email: {
					required: true,
					email: true
				},
				address: {
					required: true
				},
				contact_no: {
					required: true,
					minlength: 10
				},
				branch_id: {
					required: true 
				},
				role_id: {
					required: true 
				}
			},
			//For custom messages
			messages: {
				fullname: {
					required: "Enter a Full Name"
				}, 
				age: {
					required: "Enter a Age"
				}, 
				email: {
					required: "Enter a Email"
				}, 
				address: {
					required: "Enter a Address"
				},
				contact_no: {
					required: "Enter a Contact No."
				},
				branch_id: {
					required: "Select a Branch"
				},
				role_id: {
					required: "Enter a Department"
				}
			},
			errorElement: 'div',
			errorPlacement: function(error, element) {
				var placement = $(element).data('error');
				
				if (placement) {
					$(placement).append(error)
					} else {
					error.insertAfter(element);
				}
			},
			invalidHandler: function(e, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					$('.error-alert-bar').show();
				}
			},
		});
	});
</script>	
<script>	
	function check_if_exists_user(btnval) {
		if(btnval=='email')
		{
			var fieldval = $("#email").val();
		}
		else{
			var fieldval = $("#contact_no").val();
		}
		$.ajax(
		{
			type:"post",
			url: "<?php echo base_url(); ?>index.php/patientController/Patient/filename_exists",
			data:{ fieldval:fieldval,btnval:btnval,pageval:'user'},
			success:function(response)
			{
				console.log(response);
				
				if (response !=0) 
				{
					if(btnval=='email')
					{
						var msg='User is already exists';
						$('#msg').html('<span style="color:#ff0700;">'+msg+"</span>");
						$('#email').val('');
						$('#email').focus();
					}
					else{
						var msg='User Contact is already exists';
						$('#contact_nomsg').html('<span style="color:#ff0700;">'+msg+"</span>");
						$('#contact_no').val('');
						$('#contact_no').focus();
					}
				}
				else
				{
					
					if(btnval=='email')
					{
						$('#msg').html("");
					}
					else{
						$('#contact_nomsg').html("");
					}
				} 
				
				
			}
		});
	}
	
	</script>			