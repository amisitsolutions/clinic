  <nav class="pcoded-navbar">
                    <div class="pcoded-inner-navbar main-menu">
                        <div class="pcoded-navigatio-lavel">Navigation</div>
                        <ul class="pcoded-item pcoded-left-item">
                          
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/admin_dashboard">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Dashboard</span>
                                </a>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                    <span class="pcoded-mtext">Configuration</span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/addSpeciality">
                                            <span class="pcoded-mtext">Specilaity</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/addRole">
                                            <span class="pcoded-mtext">Department</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/branchPrefix">
                                            <span class="pcoded-mtext">Prefix</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/serviceTypes">
                                            <span class="pcoded-mtext">Service Type</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/servicesDetails">
                                            <span class="pcoded-mtext">Services</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/timingDetails">
                                            <span class="pcoded-mtext">Doctor Timing</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>index.php/insuranceDetails">
                                            <span class="pcoded-mtext">Insurance Details</span>
                                        </a>
                                    </li>
                                  
                                    
                                </ul>
                            </li>
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/appointment_link">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Appointment Link</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/userDetails">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">User</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/doctorDetails">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Doctor</span>
                                </a>
                            </li>
                    
                                    <li class="">
                                <a href="<?php echo base_url(); ?>index.php/patientDetails">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Patient Details</span>
                                </a>
                            </li>   
                        <li class="">
                                <a href="<?php echo base_url(); ?>index.php/appointmentsDetails">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Appointment Details</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/addPrescription">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Add Prescription Details</span>
                                </a>
                            </li>
                           <!-- <li class="">
                                <a href="<?php echo base_url(); ?>index.php/patientInvoice">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">New Invoice </span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/invoiceDetails">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Invoice Details</span>
                                </a>
                            </li> -->
                            <li class="">
                                <a href="<?php echo base_url(); ?>index.php/doctorAppointments">
                                    <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                    <span class="pcoded-mtext">Doctors Appointment Details</span>
                                </a>
                            </li> 
                            </ul>
                    </div>
                </nav>