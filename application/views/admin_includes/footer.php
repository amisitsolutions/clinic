<script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery\js\jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery-ui\js\jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\popper.js\js\popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\bootstrap\js\bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\pages\form-validation\validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\modernizr\js\modernizr.js"></script>
	
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\modernizr\js\css-scrollbars.js"></script>
     
     <script src="<?php echo base_url(); ?>\files\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\js\SmoothScroll.js"></script>
    <script src="<?php echo base_url(); ?>\files\assets\js\pcoded.min.js"></script>


    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net\js\jquery.dataTables.min.js"></script>
	 <script src="<?php echo base_url(); ?>\files\assets\pages\data-table\extensions\row-reorder\js\dataTables.rowReorder.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\assets\pages\data-table\js\jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\assets\pages\data-table\js\pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\assets\pages\data-table\js\vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net-buttons\js\buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net-buttons\js\buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>\files\assets\pages\data-table\js\data-table-custom.js"></script>
   
   
     <!-- pnotify js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\pnotify\js\pnotify.js"></script>  
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\js\bootbox.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\js\jquery.timepicker.js"></script>
  
   <!--  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>  -->
    <script src="<?php echo base_url(); ?>\files\assets\js\vartical-layout.min.js"></script>
    <!-- Select 2 js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\bootstrap-tagsinput\js\bootstrap-tagsinput.js"></script>
    <script src="<?php echo base_url(); ?>\files\assets\js\typeahead.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\select2\js\select2.full.min.js"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="<?php echo base_url(); ?>\files\bower_components\bootstrap-multiselect\js\bootstrap-multiselect.js">
 
</script> <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\pages\edit-table\jquery.tabledit.js"></script>
  
    <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\js\script.min.js"></script>
  <!--  <script type="text/javascript" src="<?php echo base_url(); ?>\files\assets\pages\form-validation\form-validation.js"></script> <script src="<?php echo base_url(); ?>\files\assets\pages\filer\jquery.fileuploads.init.js" type="text/javascript"></script> -->
 <script src="<?php echo base_url(); ?>\files\assets\js\jquery.validate.min.js"></script>

<link href="<?php echo base_url(); ?>\files\assets\js\datepicker.css" rel="stylesheet" type="text/css" />  
<script src="<?php echo base_url(); ?>\files\assets\js\bootstrap-datepicker.js"></script>
<script type="text/javascript">
jQuery(function($) {
    $('select').on('change', function() {  // when the value changes
			$(this).valid(); // trigger validation on this element
		});	
 $('.datepicker').datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true,
			todayHighlight: true
			}).on('changeDate',  function (ev) {	
			// Revalidate the date field
          
			$(this).datepicker('hide');
			$(this).valid();
			  
		});
        $('.tagsinput').css('width','100%'); 
			  
		}); 
     
    $(".js-example-tags").select2({
    tags: true,
    tokenSeparators: [',', ' ']
    });

</script>
<script>
    // Info notification
    $('#pnotify-info').on('click', function () {
        new PNotify({
            title: 'Info notice',
            text: 'Check me out! I\'m a notice.',
            icon: 'icofont icofont-info-square',
            type: 'info'
        });
    });

 
  
		function OnSuccess(Message) {
			 new PNotify({
          //  title: 'Success notice',
            text: Message,
            icon: 'icofont icofont-info-circle',
            type: 'success'
        });
		}
		
		function OnError(Message) {
			 new PNotify({
            //title: 'Danger notice',
            text:Message,
            icon: 'icofont icofont-info-circle',
            type: 'error'
        });
		}
		function isNumber(evt) {
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode > 31 && (charCode < 46 || charCode > 57)) {
				return false;
			}
			return true;
		}
		
	</script>
	
	<script>
	/*	function SuccessNotify()
		{
			
			 var success = '<?php echo $_SESSION['success'];?>';
			if(success)
			{
			 
				OnSuccess(success);
				var r='<?php unset($_SESSION['success']); ?>';
			} 
		}*/
	</script>
</body> 
</html>
