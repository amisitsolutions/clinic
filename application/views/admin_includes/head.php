<!DOCTYPE html>
<html lang="en">

<head>
    <title> Clinic </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url(); ?>\files\assets\images\favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\bootstrap\css\bootstrap.min.css">
    <!-- feather Awesome -->
 <!-- themify-icons line icon -->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\icon\themify-icons\themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\icon\icofont\css\icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\icon\feather\css\feather.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\pages\data-table\css\buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css">
     
	     <!-- notify js Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\pnotify\css\pnotify.css">  
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\switchery\css\switchery.min.css">
	 <!-- Animate.css -->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\bootstrap-tagsinput\css\bootstrap-tagsinput.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\animate.css\css\animate.css">
    <!-- Multi Select css --><link rel="stylesheet" href="<?php echo base_url(); ?>\files\bower_components\select2\css\select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\bootstrap-multiselect\css\bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\bower_components\multiselect\css\multi-select.css">
    <!-- Style.css -->  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\pages\data-table\extensions\responsive\css\responsive.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\css\style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\css\jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>\files\assets\css\custom.css">
  </head>
 