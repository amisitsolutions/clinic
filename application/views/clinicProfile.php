<?php
	defined('BASEPATH') OR exit('No direct script access allowed');	
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
 
			$this->load->view("admin_includes/sidebar");
		?>		
		<!-- Content Wrapper. Contains page content -->
		<div class="page-wrapper">
      
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Update Profile</h5>
                                                 </div>
                                                <div class="card-block"> 
                                              <form action="<?php echo base_url(); ?>index.php/save_clinic_profile" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >	
												<?php
											 
											foreach ($clinic_data as $key => $data) {
												?>	<input type="hidden" name="clinic_id" value="<?php echo $data['clinicID']; ?>" id="clinic_id">
                                                    <div class="form-group row">
													<label class="col-md-4 control-label">Clinic Reg. No. : <span class="required">*</span></label>
													<div class="col-md-4">
														<input type="text" readonly name="super_name" id="super_name" class="form-control" placeholder="Enter Name" value="<?php echo $data['clinicRegNo']; ?>">
													</div>
												</div>	
										  		<div class="form-group row">
													<label class="col-md-4 control-label">Clinic Name : <span class="required">*</span></label>
													<div class="col-md-4">
														<input type="text" required name="clinic_name" id="clinic_name" class="form-control" placeholder="Enter Name" value="<?php echo $data['clinicName']; ?>">
													</div>
												</div>	
										  
										 	<div class="form-group row">
													<label class="col-md-4 control-label">Email :<span class="required">*</span> </label>
													<div class="col-md-4">
														<input type="text" required name="clinic_email" id="clinic_email" class="form-control" placeholder="Enter Email ID" value="<?php echo $data['clinicEmail']; ?>">
													</div>
												</div>	
										 	<div class="form-group row">
													<label class="col-md-4 control-label">Contact No. :<span class="required">*</span> </label>
													<div class="col-md-4">
														<input type="text" onkeypress="return isNumber(event);"   required name="clinic_contact" id="clinic_contact" class="form-control" placeholder="Enter Contact No." value="<?php echo $data['clinicContact']; ?>">
													</div>
													</div>
												<div class="form-group row">
													<label class="col-md-4 control-label">Clinic Info :<span class="required">*</span></label>
													<div class="col-md-4">
														<input type="text"   name="clinic_info" id="clinic_info" class="form-control" placeholder="Enter Clinic Info" value="<?php echo $data['clinicInfo']; ?>">
													</div>
												</div>	
										 
												<div class="form-group row">
													<label class="col-md-4 control-label">website :<span class="required">*</span></label>
													<div class="col-md-4">
														<input type="text"   name="website" id="website" class="form-control" value="<?php echo $data['clinicWebsite']; ?>" />
													</div>
												</div>
															
															<?php } ?>
                                                            <div class="row">
                                                                <label class="col-sm-4"></label>
                                                                <div class="col-sm-6">
                                                                <?php if(!empty($clinic_data)) 
                                                                {
								                                	?>
										<button class="btn btn-sm btn-primary m-b-0" type="submit" name="update_type_btn" value="update_type_btn">Update</button> 
									<?php } else { ?>
								 <button class="btn btn-sm btn-primary m-b-0" type="submit" name="add_type_btn" value="add_type_btn">Submit
									</button>
									 <?php } ?>
									 
									 
                                                                </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div>  
        </div><!-- /.c /.con-->	
	<?php
		$this->load->view("admin_includes/footer");
	?>				
</div><!-- ./wrapper -->	

 <script>
    $(function() {
        $("#block-validate").validate({
            rules: {
                super_name: {
                    required: true
                },
                 clinic_name: {
                    required: true
                },
                super_email_id: {
                    required: true,
                    email: true
                },
                userName: {
                    required: true
                },
                  password: {
                    required: true
                },
               
                super_contact: {
                    required: true 
                }
            },
            //For custom messages
            messages: {
                super_name: {
					required: "Enter a Full Name"
                }, 
				  clinic_name: {
					required: "Enter a Clinic Name"
                }, 
				super_email_id: {
                    required: "Enter a Email"
                }, 
				userName: {
                    required: "Enter a User Name"
                },
               	password: {
                    required: "Enter a Password"
                }, 
				super_contact: {
                    required: "Enter a Contact No."
                }
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            },
        });
    });
    </script>