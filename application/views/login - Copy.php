<?php 
	$this->load->view("admin_includes/head");

?> 
 

<body class="fix-menu">
    <!-- Pre-loader start -->
    <div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>
</div>
    <!-- Pre-loader end -->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row ">
                <div class="col-sm-12 login_panel">
                    <!-- Authentication card start -->
                    <form id="login_form" method="POST" class="form-horizontal form-signin" action="<?php echo base_url(); ?>index.php/checkLogin" >
                        <div class="text-center">
                            <!-- <img src="<?php echo base_url(); ?>\files\assets\images\logo.png" alt="logo.png"> -->
                        <h3>Clinic Management</h3>
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center txt-primary">Login In</h3>
                                    </div>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="text" name="reg_no" id="reg_no" class="form-control" required="" placeholder="Reg No.">
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="text" name="username" id="username" class="form-control" required="" placeholder="Username">
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" name="password" id="password" class="form-control" required="" placeholder="Password">
                                    <span class="form-bar"></span>
                                </div>
                                <div class="row m-t-25 text-left">
                                    <div class="col-12">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label>
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                <span class="text-inverse">Remember me</span>
                                            </label>
                                        </div>
                                        <div class="forgot-phone text-right f-right">
                                            <a  id="flip-link" class="flip-link text-right f-w-600"> Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">LOGIN</button>
                                        
                                    </div>
                                </div>
                                <p class="text-inverse text-left">Don't have an account?<a href="auth-sign-up-social.htm"> <b class="f-w-600">Register here </b></a>for free!</p>
                            </div>
                        </div>
                    </form>
                        <!-- end of form -->
                    </div>
                    <!-- Authentication card end -->
                
                <div class="col-sm-12 forgot_pwd_panel" style="display:none;">
                    <!-- Authentication card start -->
    
                    <form name="rest_passwordform" id="rest_passwordform" action="<?php echo base_url(); ?>index.php/Login/do_reset_password_notify" method="post"  class="bv-form">       <div class="text-center">
                        <h3>Clinic Management</h3>
                    
                    </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-left">Recover your password</h3>
                                    </div>
                                </div>
                                <div class="form-group form-primary">
                                         <input id="new_reg_no" name="new_reg_no"   placeholder="Enter Reg.no." required type="text" class="validate"  >
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                         <input id="new_reg_no" name="clinic_username"   placeholder="Enter Reg.no." required type="text" class="validate"  >
                                    <span class="form-bar"></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Reset Password</button>
                                    </div>
                                </div>
                                <p class="f-w-600 text-right">Back to <a id="back_tologin">Login.</a></p>
                                <div class="row">
                                    <div class="col-md-10">
                                        <p class="text-inverse text-left m-b-0">Thank you.</p>
                                     </div>
                                    <div class="col-md-2">
                                        <img src="<?php echo base_url(); ?>\files\assets\images\auth\Logo-small-bottom.png" alt="small-logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                   
                </div>
                <!-- end of col-sm-12 -->
            </div>
         
    </section>
    <?php
        $this->load->view("admin_includes/footer");
    ?>
    
<script>
    $(function() {
        $("#login_form").validate({
            rules: {
				
                reg_no: { 
                    required: true
				},
                username: { 
                    required: true
				},
                password: {
                    required: true
					} ,reg_no: {
                    required: true
				}
			},
            messages: {
                reg_no: {
                    required: "Enter a Reg No."
					},
				uname: {
                    required: "Enter a username"
					},Password: {
                    required: "Enter a Password"
					},reg_no: {
                    required: "Enter a Reg No."
				}
			} ,
			errorElement : 'div',
			errorPlacement: function(error, element) {
				var placement = $(element).data('error');
				if (placement) {
					$(placement).append(error)
					} else {
					error.insertAfter(element);
				}
			}
			
		});
	});
</script>
<script>
	$(document).ready(function(){
		$(".flip-link").click(function(){
			$(".login_panel").hide("slow");
			$(".forgot_pwd_panel").show("slow");
		});  
		$("#back_tologin").click(function(){
			$(".login_panel").show("slow");
			$(".forgot_pwd_panel").hide("slow");
		});
	});
</script>