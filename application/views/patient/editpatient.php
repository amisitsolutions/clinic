<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				
				<div class="page-body">
					<div class="row">
						<!-- Left column start -->
						<div class="col-lg-12 col-xl-9">
							<!-- Flying Word card start -->
							<div class="card">
								<div class="card-header">
									<h5>Patient Details</h5>
									
								</div>
								<div class="card-block">
									<h4 class="sub-title">Personal Details</h4>
									<form action="<?php echo base_url(); ?>index.php/savepatientRegistration" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
										
										<?php 
											foreach ($edit_data as $key => $data) {
												
											?>
											<input type="hidden" name="patient_id" value="<?php echo $data['patientID']; ?>" >
											<div class="form-group row">
												<div class="col-sm-6 m-b-5">
													<label for="f-name1" >Full Name <span  class="required"> * </span></label>
													<input id="fullname" name="fullname"  value="<?php echo $data['fullName']; ?>" type="text" class="form-control" placeholder="First Name">
												</div>
												<div class="col-sm-6">
													<input id="contact_no" onkeypress="return isNumber(event);"  value="<?php echo $data['contactNO']; ?>" name="contact_no" onchange="getdata();" required maxlength="10" minlength="10" type="text"  class="form-control" placeholder="Mobile Number">
												</div>
											</div>
											<div class="form-group row">
												<div class="col-sm-12">
													<input type="email" class="form-control"  value="<?php echo $data['emailID']; ?>" placeholder="Enter email address">
												</div>
											</div>
											<div class="form-group row">
												<div class="col-sm-6 m-b-5">
													<input id="dob"  name="dob" onchange="getAge();"  value="<?php echo $data['dob']; ?>" class="dobdatepicker form-control" type="text"  placeholder="select DOB">
												</div>
												<div class="col-sm-6">
													<input id="age" readonly  name="age" class="form-control"  value="<?php echo $data['age']; ?>" placeholder="Age">
												</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<div class="form-radio">
													<div class="radio radiofill radio-inline">
														<label>
															<input type="radio" <?php if($data['gender']=='Male') { echo "selected"; }  ?> name="gender" id="male" value="Male" checked="checked">
															<i class="helper"></i>Male
														</label>
													</div>
													<div class="radio radiofill radio-inline">
														<label>
															<input type="radio"  <?php if($data['gender']=='Female') { echo "selected"; }  ?>  name="gender"  id="Female" value="Female">
															<i class="helper"></i>Female
														</label>
													</div>
													
												</div>
											</div>
										</div>
										<div class="form-group row">
											
											<div class="col-sm-4 m-b-5">
												<select name="state" required id="state" class="form-control" >
													<option value="">Select State </option>
													<?php
														for ($i = 0; $i < count($stateList); ++$i) {
														?>
														<option <?php if($data['stateID']==$stateList[$i]->stateID) { echo "selected"; } ?> value="<?php echo $stateList[$i]->stateID; ?>"><?php echo $stateList[$i]->stateName; ?></option>
													<?php } ?>	
													
													
												</select>
											</div>
											<div class="col-sm-4">
												<select name="city" required id="city" class="form-control" >
													<option value="">Select City </option>
													<?php
														for ($i = 0; $i < count($cityList); ++$i) {
														?>
														<option <?php if($data['cityID']==$cityList[$i]->cityID) { echo "selected"; } ?> value="<?php echo $cityList[$i]->cityID; ?>"><?php echo $cityList[$i]->cityName; ?></option>
													<?php } ?>	
													
													
												</select>
											</div>
											<div class="col-sm-4">
												<input id="zipcode" required   value="<?php echo $data['zipcode']; ?>" class="form-control" name="zipcode" type="text">
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<textarea id="address" id="address"   rows="5" cols="5" class="form-control" placeholder="Your Full Address"><?php echo $data['address']; ?></textarea>
											</div>
										</div>
										
										<hr>
										<h4 class="sub-title">Your Education</h4>
										
										<div class="form-group row">
											<div class="col-sm-6 m-b-5">
												<select type="text" id="patient_type"   class="form-control" name="patient_type" onchange="get_ref();">
													<option value="" selected disabled >Select Patient Type	</option> 
													<option  <?php if($data['patientType']=='Corporate') echo "selected"; ?> value="Corporate">Corporate	</option>
													<option <?php if($data['patientType']=='Insurance') echo "selected"; ?> value="Insurance">	Insurance</option>
													<option <?php if($data['patientType']=='Self') echo "selected"; ?>  value="Self">Self	</option>
													<option <?php if($data['patientType']=='Referred') echo "selected"; ?>  value="Referred">Referred	</option> 
												</select>
											</div>
											<div class="col-sm-6">
												<input type="text" id="accupation"  name="accupation" class="form-control"   value="<?php echo $data['occupation']; ?>"   placeholder="Occupation">
											</div>
										</div>
										<div class="form-group row  style="<?php if($data['patientType']!='Self')  { echo  "display:block"; } else { ?>  display:none; <?php  } ?> ">
											<div class="col-sm-6 m-b-5">
												<input id="ref_no"  name="ref_no" type="text" class="form-control" placeholder="Ref No."   value="<?php echo $data['refNO']; ?>"  >
											</div>
											
										</div>
										
										<hr>
										<h4 class="sub-title">Accompany / Attendar Details :</h4>
										
										<div class="form-group row">
											<div class="col-sm-6 m-b-5">
												<label class="col-lable">Relation  </label>
												<input type="text" required id="relation"  name="relation" class="form-control"   value="<?php echo $data['relWithPatient']; ?>" >
											</div>
											<div class="col-sm-6">
												<label class="col-lable">Attendar Name</label>
												<input type="text" required id="relative_name" class="form-control"   name="relative_name"   value="<?php echo $data['relativeName']; ?>" >
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 m-b-5">
												<label class="col-lable">Attendar Contact *
												Contact No. *</label>
												<input type="text" required id="relative_contact"  name="relative_contact" class="form-control"   value="<?php echo $data['relativeContact']; ?>" >
											</div>
											
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<label class="col-lable">Upload Photo</label>
												<input class="file-path validate form-control" name="photo_file" id="photo_file" type="file" placeholder="Choose File Here" accept="image/*" onchange="loadFile(event)">
												<img id="output" class="thumbnail"/>
												<script>
													var loadFile = function(event) {
														var output = document.getElementById('output');
														output.src = URL.createObjectURL(event.target.files[0]);
														output.onload = function() {
															URL.revokeObjectURL(output.src) // free memory
														}
													};
												</script>
											</div>
										</div>
										
										
										<hr>
										<h4 class="sub-title">Remarks</h4>
										
										<div class="form-group row">
											<div class="col-sm-12">
												<textarea rows="5" cols="5" class="form-control"  name="remarks" id="remarks"  placeholder="Any queries <?php echo base_url(); ?> ??"><?php echo $data['remark']; ?></textarea>
												
											</div>
										</div> 
									</div>
									<div class="card-footer">
										<div class="row">
											<div class="col-sm-12 text-right">
												<button type="submit" name="update" id="update" value="add" class="btn btn-primary m-r-10">Submit Details</button>
										 	</div>
										</div>
									</div>
									<?php } ?>
								</form>
							</div>
							<!-- Flying Word card end -->
						</div>
						<!-- Left column end -->
						<!-- Right column start -->
						<div class="col-lg-12 col-xl-3">
							<!-- Filter card start -->
							<div class="card">
								<div class="card-header">
									<h5><i class="icofont icofont-filter m-r-5"></i>Filter</h5>
								</div>
								<div class="card-block">
									<form action="#">
										<div class="form-group row">
											<div class="col-sm-12">
												<input type="text" class="form-control" placeholder="MRD No.">
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<input type="text" class="form-control" placeholder="Contact Number">
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<select class="form-control">
													<option>Select Job Type</option>
													<option>Full Time</option>
													<option>Part Time</option>
													<option>Remote</option>
												</select>
											</div>
										</div>
										<div class="text-right">
											<button type="submit" class="btn btn-primary">
												<i class="icofont icofont-job-search m-r-5"></i> Patient Find
											</button>
										</div> 
									</div>
								</div>
								
								<div class="card job-right-header">
									<div class="card-header">
										<h5>Existing Patient Details</h5>
										<div class="card-header-right">
											<label class="label label-danger">Add</label>
										</div>
									</div>
									<div class="card-block user-box existing_data">
										 
									</div>
								</div>
								
								
							</div>
							<!-- Right column end -->
						</div>
					</div>
					<!-- Page body end -->
				</div>
			</div>
			
		</div>
	</div>
	
	
	
	<?php
		$this->load->view("admin_includes/footer");
	?>				
</div>
<script type="text/javascript">
	$(document).ready(function() {	 
		$('.dobdatepicker').datepicker({format: 'yyyy-mm-dd'}).on('changeDate',  function (ev) {	
			$(this).datepicker('hide');
		});		
	});		
	
</script>
<script>
    $(function() {
		$.validator.setDefaults({
			ignore: []
		});
		$("#block-validate").validate({
            rules: {
                fullname: {
                    required: true
				},
				dob: {
                    required: true 
				},
                email: {
                    required: true,
                    email: true
				}, 
                contact_no: {
                    required: true,
                    minlength: 10,
					required: true
				},
				state: {
					required: true 
				},
				city: {
                    required: true 
				},
                zipcode: {
                    required: true 
				}, 
                relation: {
                    required: true 
				}, 
                relative_name: {
                    required: true 
				}, 
                relative_contact: {
                    required: true 
				}
			},
            //For custom messages
            messages: {
                fullname: {
					required: "Enter a Full Name"
				}, 
				dob: {
					required: "Enter a DOB"
				}, 
				email: {
                    required: "Enter a Email"
				},  
				contact_no: {
                    required: "Enter a Contact No."
				}
				,state: {
					required: "Select state"
				},
				city: {
                    required: "Enter  City Name"
				},
                zipcode: {
                    required: "Enter  Zipcode "
				}, 
                relation: {
                    required:  "Enter a Contact No."
				}, 
                relative_name: {
                    required: "Enter a Relative Name."
				}, 
                relative_contact: {
                    required: "Enter a Contact No."
				}
			},
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
					} else {
                    error.insertAfter(element);
				}
			},
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
				}
			},
		});
	});
</script>
<script>
	function getdata()
	{
		var contact=$('#contact_no').val();
		var base_url='<?php echo base_url(); ?>';
		 
		if(contact) {
			$.ajax({
				url: base_url+"index.php/getExistingPatientdata/",
				type: "POST",
				data: { contact:contact },
				success: function(result)
				{	 
					$('.existing_data').html(result);
				}
			});
			
		}  
	}
	function getAge() {
		var DOB=$('#dob').val();
		var today = new Date();
		var birthDate = new Date(DOB);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age = age - 1;
		}
		
		$('#age').val(age);
	}
	function get_appointment(patient_id)
	{ 
		var base_url='<?php echo base_url(); ?>';
		
		if(patient_id) {
			$.ajax({
				url: base_url+"index.php/getPatientdata/",
				type: "POST",
				data: { patient_id:patient_id },
				success: function(result)
				{
					
					var res=result.split("#"); 
					$("#fullname").val(res[0].trim());
					$("#contact").val(res[1].trim());
					var prefix=$("#prefix").val();
					$("#mrd").val(prefix+"/"+res[2].trim()); 
					$("#patient_id").val(res[3].trim()); 
					
				} 
			});
			
		}  
	}
	function get_ref()
	{
		var sel=$('#patient_type').val();
		if(sel=='Self')
		{
			$('.ref_panel').hide();
		}
		else{
			$('.ref_panel').show();
		}
	}
	
</script>