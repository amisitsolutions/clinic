<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");

?>
 
<div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Patient Details
							 </h5><a href="<?php echo base_url(); ?>index.php/patientRegistration" class="btn btn-primary btn-sm pull-right">
								 <i class="material-icons"></i> Patient Registration</a>
                                                 </div>
                                                <div class="card-block"> 
                                	
							  <div class="dt-responsive table-responsive">
                                <table id="dom-table" class="table table-striped table-bordered nowrap">
								    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>MRD No.</th>
                                            <th>Name</th>
                                            <th>Gender/Age</th>
                                            <th>Email/Contact</th>                                     
											<th>Reg date</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cnt = 1;
								
									for ($i = 0; $i < count($PatientData);$i++) {
						 			?>
									
									<tr class="odd gradeX">
									<td><?php echo $cnt++; ?></td>
                                        <td><?php  if($prefix[0]['prefix1']==''){
															$prename=$prefix[0]['yearWise']; 
												} 
												else
												{
													$prename=$prefix[0]['prefix1']."/".$prefix[0]['yearWise']; 
												}
												 
												
											echo	$prename."/".$PatientData[$i]->patientRegNO; ?></td>
                                        <td><?php echo $PatientData[$i]->fullName; ?></td>
                                         <td><?php echo $PatientData[$i]->gender."</br><b>Age :</b>". $PatientData[$i]->age; ?></td>
                                        <td><?php echo $PatientData[$i]->emailID."</br>". $PatientData[$i]->contactNO; ?></td>
                                      	
                                        
									   <td><?php echo date("d-M-Y",strtotime($PatientData[$i]->createDate)); ?></td>
                                        <td>
										<div class="btn-group btn-group-sm" style="float: none;">
                                     
                                        <a href="<?php echo base_url('index.php/editPatient/' . $PatientData[$i]->patientID); ?>"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>

									  
										<a onclick="delete_data('<?php echo $PatientData[$i]->patientID; ?>');" class="tabledit-delete-button btn btn-danger waves-effect waves-light "> <span class="icofont icofont-ui-delete"></span></a>
										</div>
										</td>
										
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
                      
					</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
<?php
	$this->load->view("admin_includes/footer");
?>				
</div><!-- ./wrapper -->	

<script type="text/javascript">
	$(document).ready(function() {	
		$('#example').dataTable();
		
	});
</script>

	<script>	
		 function delete_data(catid){	
	
					location='<?php echo base_url('index.php/deletePatient/'); ?>'+catid;
              
		 }
		
</script>