<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<div class="pcoded-content">
                      <div class="pcoded-inner-content">
                          <!-- Main-body start -->
                          <div class="main-body">
                              <div class="page-wrapper">
                              
                                    <div class="page-body">
                                        <div class="row">
                                            <!-- Left column start -->
                                            <div class="col-lg-12 col-xl-9">
                                                <!-- Flying Word card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Patient Details</h5>

                                                    </div>
                                                    <div class="card-block">
                                                        <h4 class="sub-title">Personal Details</h4>
														<form action="<?php echo base_url(); ?>index.php/savepatientRegistration" class="form-horizontal"  autocomplete="off" id="block-validate" method="POST" enctype="multipart/form-data" >
                                                          
														<input id="prefix"  type="hidden" name="prefix" value="<?php //echo  $prefix[0]['prefix1']."". $prefix[0]['prefix2']."".$prefix[0]['yearwise']."". $prefix[0]['start_from']; ?>" >	 
											<input id="mrd" readonly name="mrd"    type="hidden">
											<input id="patient_id" readonly name="patient_id"  class="form-control"   type="hidden"> 
														    <div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
																<label for="f-name1" >Full Name <span  class="required"> * </span></label>
																<input id="fullname"required name="fullname" type="text" class="form-control" placeholder="First Name">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                <label for="f-name1" >Contact No.<span  class="required"> * </span></label>
																<input id="contact_no" required onkeypress="return isNumber(event);" name="contact_no" onchange="getdata();" required maxlength="10" minlength="10" type="text"  class="form-control" placeholder="Mobile Number">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <input type="email" name="email" id="email" required class="form-control" placeholder="Enter email address">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
																<input id="dob"  name="dob" onchange="getAge();" class="dobdatepicker form-control" type="text"  placeholder="select DOB">
                                                                </div>
                                                                <div class="col-sm-6">
																<input id="age" readonly  name="age" class="form-control" placeholder="Age">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-radio">
                                                                        <div class="radio radiofill radio-inline">
                                                                            <label>
                                                                                <input type="radio" name="gender" id="male" value="Male" checked="checked">
                                                                                <i class="helper"></i>Male
                                                                            </label>
                                                                        </div>
                                                                        <div class="radio radiofill radio-inline">
                                                                            <label>
                                                                                <input type="radio" name="gender"  id="Female" value="Female">
                                                                                <i class="helper"></i>Female
                                                                            </label>
                                                                        </div>
                                                                   
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                
                                                                <div class="col-sm-4 m-b-5">
																<select name="state" required id="state" class="form-control" onchange="get_city(this.value);">
															<option value="">Select State </option>
															<?php
																for ($i = 0; $i < count($stateList); ++$i) {
																?>
																<option value="<?php echo $stateList[$i]->stateID; ?>"><?php echo $stateList[$i]->stateName; ?></option>
															<?php } ?>	
															
															
														</select>
                                                                </div>
                                                                <div class="col-sm-4">
																<select name="city" required id="city" class="form-control" >
															<option value="">Select City </option>
														  
														</select>
                                                                </div>
                                                                <div class="col-sm-4">
																<input id="zipcode" placeholder="Enter Zipcode"   class="form-control" name="zipcode" type="text">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row"> 
                                                                <div class="col-sm-12">
																<textarea id="address" name="address"   rows="5" cols="5" class="form-control" placeholder="Your Full Address"></textarea>
                                                                </div>
                                                            </div>
														 
                                                        <hr>
                                                        <h4 class="sub-title">Your Education</h4>
                                                        
                                                            <div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
																<select type="text" id="patient_type"   class="form-control" name="patient_type" onchange="get_ref();">
											<option value="" selected disabled >Select Patient Type	</option> 
											<option value="Corporate">Corporate	</option>
											<option value="Insurance">	Insurance</option>
											<option value="Self">Self	</option>
											<option value="Referred">Referred	</option> 
											</select>
                                                                </div>
                                                                <div class="col-sm-6">
																<input type="text" id="accupation"  name="accupation" class="form-control" placeholder="Occupation">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row  ref_panel" style="display:none;">
                                                                <div class="col-sm-6 m-b-5">
																<input id="ref_no"  name="ref_no" type="text" class="form-control" placeholder="Ref No.">
                                                                </div>
                                                                 
                                                            </div>
                                                            
                                                        <hr>
                                                        <h4 class="sub-title">Accompany / Attendar Details :</h4>
                                                       
                                                            <div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
                                                                    <label class="col-lable">Relation <span  class="required"> * </span> </label>
                                                                    <input type="text" required id="relation"  name="relation" class="form-control">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label class="col-lable">Attendar Name <span  class="required"> * </span></label>
                                                                    <input type="text" required id="relative_name" class="form-control"   name="relative_name">
                                                                </div>
                                                            </div>
															<div class="form-group row">
                                                                <div class="col-sm-6 m-b-5">
                                                                    <label class="col-lable">Attendar Contact<span  class="required"> * </span> </label>
																	<input type="text" required id="relative_contact"  name="relative_contact" class="form-control">
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <label class="col-lable">Upload Photo</label>
                                                                    <input class="file-path validate form-control" name="photo_file" id="photo_file" type="file" placeholder="Choose File Here" accept="image/*" onchange="loadFile(event)">
													<img id="output" class="thumbnail"/>
													<script>
													  var loadFile = function(event) {
														var output = document.getElementById('output');
														output.src = URL.createObjectURL(event.target.files[0]);
														output.onload = function() {
														  URL.revokeObjectURL(output.src) // free memory
														}
													  };
													</script>
                                                                </div>
                                                            </div>
                                                    
                                                        
                                                        <hr>
                                                        <h4 class="sub-title">Remarks</h4>
                                                        
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <textarea rows="5" cols="5" class="form-control"  name="remarks" id="remarks"  placeholder="Any queries .. ??"></textarea>
                                                               
															    </div>
                                                            </div> 
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="row">
                                                            <div class="col-sm-12 text-right">
                                                                <button type="submit" name="add" id="add" value="add" class="btn btn-primary m-r-10">Submit Details</button>
                                                                                                </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                                <!-- Flying Word card end -->
                                            </div>
                                            <!-- Left column end -->
                                            <!-- Right column start -->
                                            <div class="col-lg-12 col-xl-3">
                                                <!-- Filter card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5><i class="icofont icofont-filter m-r-5"></i>Filter</h5>
                                                    </div>
                                                    <div class="card-block">
                                                        <form action="#">
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <input type="text" class="form-control" placeholder="MRD No.">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <input type="text" class="form-control" placeholder="Contact Number">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-12">
                                                                    <select class="form-control">
                                                                        <option>Select Job Type</option>
                                                                        <option>Full Time</option>
                                                                        <option>Part Time</option>
                                                                        <option>Remote</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary">
                                                                    <i class="icofont icofont-job-search m-r-5"></i> Patient Find
                                                                </button>
                                                            </div> 
                                                    </div>
                                                </div>
                                                
                                                <div class="card job-right-header">
                                                    <div class="card-header">
                                                        <h5>Existing Patient Details</h5>
                                                        <div class="card-header-right">
                                                            <label class="label label-danger">Add</label>
                                                        </div>
                                                    </div>
													<div class="card-block user-box existing_data">
                                                                                
                                                                                </div>
                                                                            </div>
                                                </div>
                                                 
                                          
                                            </div>
                                            <!-- Right column end -->
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                           
                        </div>
                    </div>
           


     <?php
	$this->load->view("admin_includes/footer");
?>				
</div>
<script type="text/javascript">
			$(document).ready(function() {	 
				$('.dobdatepicker').datepicker({format: 'yyyy-mm-dd'}).on('changeDate',  function (ev) {	
					$(this).datepicker('hide');
				});		
				});		
				
 </script>
 <script>
    $(function() {
	$.validator.setDefaults({
				ignore: []
			});
       $("#block-validate").validate({
            rules: {
                fullname: {
                    required: true
                },
                 dob: {
                    required: true 
                },
                email: {
                    required: true,
                    email: true
                }, 
                contact_no: {
                    required: true,
                    minlength: 10,
				required: true
                },
				state: {
				required: true 
                },
                 city: {
                    required: true 
                },
                zipcode: {
                    required: true 
                }, 
                relation: {
                    required: true 
                }, 
                relative_name: {
                    required: true 
                }, 
                relative_contact: {
                    required: true 
                }
            },
            //For custom messages
            messages: {
                fullname: {
					required: "Enter a Full Name"
                }, 
				  dob: {
					required: "Enter a DOB"
                }, 
				email: {
                    required: "Enter a Email"
                },  
				contact_no: {
                    required: "Enter a Contact No."
                }
				,state: {
				required: "Select state"
                },
                 city: {
                    required: "Enter  City Name"
                },
                zipcode: {
                    required: "Enter  Zipcode "
                }, 
                relation: {
                    required:  "Enter a Contact No."
                }, 
                relative_name: {
                    required: "Enter a Relative Name."
                }, 
                relative_contact: {
                    required: "Enter a Contact No."
                }
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            },
        });
    });
    </script>
    <script>
	function getdata()
	{
		var contact=$('#contact_no').val();
		var base_url='<?php echo base_url(); ?>';
		 
            if(contact) {
					$.ajax({
					url: base_url+"index.php/getExistingPatientdata/",
					type: "POST",
					data: { contact:contact },
					success: function(result)
					{
						$('.existing_data').html(result);
					 }
				});
                
            }  
	}
    function get_city()
	{
		var state=$('#state').val();
		var base_url='<?php echo base_url(); ?>';
		 
            if(state) {
					$.ajax({
					url: base_url+"index.php/getcityData/",
					type: "POST",
					data: { state:state },
					success: function(result)
					{
						$('#city').html(result);
					 }
				});
                
            }  
	}
    
 function getAge() {
 var DOB=$('#dob').val();
    var today = new Date();
    var birthDate = new Date(DOB);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }

    $('#age').val(age);
}
	function get_appointment(patient_id)
	{ 
		var base_url='<?php echo base_url(); ?>';
		 
            if(patient_id) {
					$.ajax({
					url: base_url+"index.php/getPatientdata/",
					type: "POST",
					data: { patient_id:patient_id },
					success: function(result)
					{
							 
						 	var res=result.split("#"); 
							$("#fullname").val(res[0].trim());
							$("#contact").val(res[1].trim());
							var prefix=$("#prefix").val();
							$("#mrd").val(prefix+"/"+res[2].trim()); 
							$("#patient_id").val(res[3].trim()); 
						 
					} 
				});
                
            }  
	}
	function get_ref()
	{
		var sel=$('#patient_type').val();
		if(sel=='Self')
		{
				$('.ref_panel').hide();
		}
		else{
			$('.ref_panel').show();
		}
	}
	
    </script>