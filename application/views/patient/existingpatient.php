<hr>
<?php 
	foreach ($existingpatientData as $key => $data) {
		
	?>
	<div class="media m-b-10">
		<a class="media-left" href="#!">
			<img class="media-object img-radius" src="<?php echo base_url(); ?>\files\assets\images\avatar-1.jpg" alt="Generic placeholder image" data-toggle="tooltip" data-placement="top" title="" data-original-title="user image">
			<div class="live-status bg-danger"></div>
		</a>
		<div class="media-body"><b>MRD No. :</b><?php echo $data->patientRegNO; ?>
			<div class="chat-header"><i><?php echo $data->fullName; ?></i></div>
			<div class="text-muted social-designation"><?php echo $data->gender; ?></div>
			<div class="text-muted social-designation"><?php echo $data->contactNO; ?></div>
			<span class="time"><?php echo date("d-M-Y",strtotime($data->createdDate)); ?></span>
			
			<button type="button"  onclick="get_appointment(<?php echo $data->patientID; ?>);" class="label label-warning pull-right">Appointment</button> 
			
			
		</div>
	</div> 
	
	
	<?php
	} 	