<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>

<style>
	.doc_tabCls .doc_tab
	{
	padding-top: 4em;
	padding-bottom: 4em;
	}
</style>

<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12"> 
							<div class="card">
								<div class="card-header">
									<h5>Doctor Details</h5>
								</div>
								<div class="card-block"> 
								<form action="<?php echo base_url(); ?>index.php/saveDoctorRegistration" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >	 
									 <?php
										 
                    foreach ($edit_data as $key => $data) {
                         
					?>
					<input type="hidden" name="doc_id" value="<?php echo $data['doctorID']; ?>" >		 
											<div class="row m-t-30">
												<div class="col-md-6">
												 <div class="form-group row">
													<label class="col-sm-4 col-form-label">MCI  <span  class="required"> * </span></label>
													<div class="col-sm-8">
													<select name="mci_id" class="form-control" required id="mci_id"  >
															<option value="">Select MCI </option>
															<?php
																for ($i = 0; $i < count($stateList); ++$i) {
																?>
																<option  <?php if($stateList[$i]->stateID==$data['doctorMciID']) { echo "selected"; } ?> value="<?php echo $stateList[$i]->stateID; ?>"><?php echo $stateList[$i]->stateName; ?></option>
															<?php } ?>	
															
															
														</select>
														<span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Full Name  <span  class="required"> * </span></label>
													<div class="col-sm-8">
														<input id="fullname" class="form-control" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['doctorName']; } ?>"  name="fullname" required  type="text">
														
														<span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Email <span  class="required"> * </span></label>
													<div class="col-sm-8">
														<input id="email" required  class="form-control" name="email" type="text"  onchange="check_if_exists_user('email');" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['doctorEmail']; } ?>"  >
														<span class="messages"></span>
													</div>
												</div> 
													<div class="form-group row">
												<label class="col-sm-4">Gender</label>
												<div class="col-sm-8">
													<div class="form-radio">
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input <?php if(!empty($edit_data)) {  if($edit_data[0]['doctorGender']=='Male') { echo "selected"; } } ?> class="with-gap" name="gender" value="Male" type="radio" checked="">
																<i class="helper"></i>Male 
															</label>
														</div>
														<div class="radio radiofill radio-primary radio-inline">
															<label>
																<input <?php if(!empty($edit_data)) { if($edit_data[0]['doctorGender']=='Female') { echo "selected"; } } ?> class="with-gap" name="gender" value="Female" type="radio" checked="">
																<i class="helper"></i>Female
															</label>
														</div>
													</div>
													<span class="messages"></span>
												</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Affilation</label>
													<div class="col-sm-8">
													<textarea id="affilation" name="affilation" class="form-control" ><?php echo $edit_data[0]['doctorAffilation']; ?></textarea>	<span class="messages"></span>
													</div>
												</div>
											</div>
										  
											<div class="col-md-6">
											<div class="form-group row">
													<label class="col-sm-4 col-form-label">MCI No.  <span  class="required"> * </span></label>
													<div class="col-sm-8">
													<input id="mci_no" class="form-control" name="mci_no" required  value="<?php echo$edit_data[0]['doctorMciNo']; ?>" type="text">  <span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Contact Number <span  class="required"> * </span></label>
													<div class="col-sm-8">
														<input id="contact_no"  class="form-control" onkeypress="return isNumber(event);" onchange="check_if_exists_user('contact');" name="contact_no" required maxlength="10" minlength="10" type="text" value="<?php if(!empty($edit_data)) { echo $edit_data[0]['doctorContact']; } ?>" >   <span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Address</label>
													<div class="col-sm-8">
															<textarea id="user_address" name="user_address"  class="form-control"  class="materialize-textarea" ><?php if(!empty($edit_data)) { echo $edit_data[0]['doctorAddress']; } ?></textarea>
														<span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Qualification</label>
													<div class="col-sm-8">
														<input id="qua" class="form-control"  value="<?php if(!empty($edit_data)) { echo $edit_data[0]['doctorQualification']; } ?>"  required name="qua" type="text">
														<span class="messages"></span>
													</div>
												</div>
												
											</div>
											
											</div>
											<div class="col-md-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Select Branch <span  class="required"> * </span> </label>
													<div class="col-sm-8">
														<select name="branch_id[]" required id="branch_id" class="js-example-placeholder-multiple  form-control" multiple>
															<option value="" disabled  >Select Branch </option>
															<?php
															$barr=explode(",",$edit_data[0]['branchID']);
																for ($i = 0; $i < count($branch_list); ++$i) {
																?>
																<option  <?php if(!empty($edit_data)) {  if(in_array($branch_list[$i]->branchID,$barr)) { echo "selected"; } }  ?>    value="<?php echo $branch_list[$i]->branchID; ?>"><?php echo $branch_list[$i]->branchName; ?></option>
															<?php } ?>	
															
															
														</select>
														<span class="messages"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Department <span  class="required"> * </span> </label>
													<div class="col-sm-8">
															<select name="role_id" class="form-control" required id="role_id">
														<option value="" disabled>Select Department </option>
														<?php
															for ($k = 0; $k < count($role_list); ++$k) {
															?>
															<option  <?php if(!empty($edit_data)) {  if($role_list[$k]->roleID==$edit_data[0]['roleID']) { echo "selected"; } }  ?>   value="<?php echo $role_list[$k]->roleID; ?>"><?php echo $role_list[$k]->roleName; ?></option>
														<?php } ?>	
														
														
													</select>
														<span class="messages"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">ID Proof File </label>
													<div class="col-sm-8">
														<input class="file-path validate" name="doc_id_proof" id="doc_id_proof" type="file" placeholder="Choose File Here">
														<span class="messages"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Photo File</label>
													<div class="col-sm-8">
														<input class="file-path validate" name="photo_file" id="photo_file" type="file" placeholder="Choose File Here">
														<span class="messages"></span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label"> Speciality <span  class="required"> * </span> </label>
													<div class="col-sm-8">
														<select name="speciality[]" class="form-control js-example-placeholder-multiple" required id="speciality" multiple>
															<option value=""  disabled >Select Speciality </option>
															<?php
															$sarr=explode(",",$data['doctorSpeciality']);
																for ($i = 0; $i < count($spec_data); ++$i) {
																?>
																<option   <?php if(!empty($data)) {  if(in_array($spec_data[$i]->specialityID,$sarr)) { echo "selected"; } }  ?>  value="<?php echo $spec_data[$i]->specialityID; ?>"><?php echo $spec_data[$i]->specialityName; ?></option>
															<?php } ?>	
															
															
														</select>
														<span class="messages"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Per Patient Time <span  class="required"> * </span> </label>
													<div class="col-sm-8">
													<input id="patient_time"  class="form-control"required  name="patient_time" " value="<?php echo $data['doctorPatientTime']; ?>"  onkeypress="return isNumber(event);"  type="text" class="validate valid"> 
														<span class="messages"></span>
													</div>
												</div>
												 
											</div>
											 
										</div>
										
										<div class="col-md-12">
										 
											<div class="row">
												<label class="col-sm-2"></label>
												<div class="col-sm-6">
													<button type="submit" name="update_doc" id="update_doc" class="btn btn-primary m-b-0" value="update_doc">Update</button>
												</div>
											</div>
					<?php } ?>
										</form>
									</div>
								</div>
							</div> 
							
						</div>
					</div>
				</div>			
				
			</div>
		</div>
	</div> 
</div>
			<?php
				$this->load->view("admin_includes/footer");
			?>				
		</div>
	
<script>
		
		var k=1; 				
	
		function addmore_spec(branch)
		{
			
			var data ='<tr id="chk'+k+'" class="odd gradeX">';
			 data +=' <input id="time_id'+k+'"  name="time_id"     type="hidden" class="timepicker validate valid"> <td><a onclick="delete_spec('+k+');"    class="btn red btn-small addmore_spec" ><i class="fa fa-minus"></a></td>';
			 data +='<td> <select name="day" id="day'+k+'" onchange="getBranches(this.value);">';
				 data +='<option value="Monday">Monday </option>';
				 data +='<option value="Tuesday">Tuesday </option> ';
				 data +='<option value="Wednesday">Wednesday </option>'; 
				 data +='<option value="Thursday">Thursday </option>'; 
				 data +='<option value="Friday">Friday </option>'; 
				 data +='<option value="Saturday">Saturday </option> ';
				 data +='<option value="Sunday">Sunday </option>'; 
				
			 data +='</select></td>';
		 
			 data +='<td><input id="from_time'+k+'"  name="from_time"  onkeypress="return isNumber(event);"  type="text" class="timepicker validate valid"></td>';
			 data +='<td><input id="to_time'+k+'"  name="to_time"  onkeypress="return isNumber(event);"  type="text" class=" timepicker validate valid"></td>';
			 data +='<td>';
			 data +='<a class="btn cyan waves-effect waves-light" id="btn_fees"   onclick="save_timing('+k+','+branch+');" name="btn_fees" type="button" name="action">Submit</a>';
			 data +='<a style="display:none; " class="btn primary blue waves-effect waves-light" id="update_time"   onclick="save_timing('+k+','+branch+');" name="update_time" type="button" name="action">Updated</a>';
			
			 data +='<a class="btn red waves-effect waves-light" id="btn_fees"   onclick="delete_timing('+k+');" name="btn_fees" type="button" name="action">Delete</a>';
			 data +='</td>';
			data +='</tr>'; 
			 $('#adddata').append(data);
			    $('select').not('.disabled').formSelect();
			$('.timepicker').timepicker();
			k++;		
		}	
		
</script>		
		<script>
			$(function() {
			  $(".js-example-placeholder-multiple").select2({
				   placeholder: "Select Speciality"
    });
			
			$.validator.setDefaults({
				ignore: []
			});
				$("#block-validate").validate({
					rules: {
						fullname: {
							required: true
						},
						age: {
							required: true,					
							minlength: 2
						},
						email: {
							required: true,
							email: true
						},
						address: {
							required: true
						},
						contact_no: {
							required: true,
							minlength: 10
						},
						role_id: {
							required: true 
						},
						speciality: {
							required: true 
						},
						branch_id: {
							required: true 
						}
					},
					//For custom messages
					messages: {
						fullname: {
							required: "Enter a Full Name"
						}, 
						age: {
							required: "Enter a Age"
						}, 
						email: {
							required: "Enter a Email"
						}, 
						address: {
							required: "Enter a Address"
						},
						contact_no: {
							required: "Enter a Contact No."
						},
						role_id: {
							required: "Select Department"
						},
						speciality: {
							required: "Enter a speciality."
						},
						branch_id: {
							required: "Select Branch"
						}
					},
					errorElement: 'div',
					errorPlacement: function(error, element) {
						var placement = $(element).data('error');
						if (placement) {
							$(placement).append(error)
							} else {
							error.insertAfter(element);
						}
					},
					invalidHandler: function(e, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {
							$('.error-alert-bar').show();
						}
					},
				});
			});
		</script>
		<script>
		function delete_spec(m)
		{
			$('#chk'+m).remove();
		}
			function getBranches(doc_id)
			{
				var base_url='<?php echo base_url(); ?>';
				$.ajax({
					url: base_url+'index.php/getbranchesByDocId/',
					type: 'post',
					data: {doc_id:doc_id},
					success: function(result){ 
						location.reload();
					}
					
				});
			}	
			function delete_timing(j)
			{
			
				var time_id=$('#time_id'+j).val();
				var base_url='<?php echo base_url(); ?>';
				$.ajax({
					url: base_url+'index.php/deletetiming/',
					type: 'post',
					data: {time_id:time_id},
					success: function(result){ 
						location.reload();
					}
					
				});
			}
			function save_fees(branch_id)
			{
			var newvisit=$('#new_visit').val();
			var no_of_days=$('#no_of_days').val();
			var recurring_visit=$('#recurring_visit').val(); 
			var fee_type=$('#fee_type').val(); 
			var immediate_fee=$('#immediate_fee').val(); 
			var doc=$('#doc').val();
			var clinic=$('#clinic').val();
				var base_url='<?php echo base_url(); ?>';
 $.ajax({
					url: base_url+'index.php/saveFeeInsert/',
					type: 'post',
					data: {newvisit:newvisit,no_of_days:no_of_days,recurring_visit:recurring_visit,doc:doc,clinic:clinic,branch_id:branch_id,fee_type:fee_type,immediate_fee:immediate_fee},
					success: function(result){ 
						 alert('INSERT SUCCSSFULLY');
					}
					
				});   
			}
			function save_timing(cnt,branch_id)
			{
			
				var from_time=$('#from_time'+cnt).val();
				var to_time=$('#to_time'+cnt).val();
				var day=$('#day'+cnt).val(); 
				var doc=$('#doc').val();
				var clinic=$('#clinic').val();
				var time_id=$('#time_id'+cnt).val();
				var base_url='<?php echo base_url(); ?>'; 
				console.log(from_time+""+to_time+""+day+""+doc );
				
				 $.ajax({
					url: base_url+'index.php/saveTiming/',
					type: 'post',
					data: {from_time:from_time,to_time:to_time,day:day,doc:doc,clinic:clinic,branch_id:branch_id,time_id:time_id},
					success: function(result){ 
						 if($('#time_id'+cnt)=='')
						 {
							$('#time_id'+cnt).val(result.trim());
						 
							$('#btn_add').hide();
							$('#update_time').show();
						 }
						 else  
						 {
							alert('edited successfult');
					}
					}
					
				}); 
			}
			function check_if_exists_user(btnval) {
		if(btnval=='email')
		{
			var fieldval = $("#email").val();
		}
		else{
			var fieldval = $("#contact_no").val();
		}
		$.ajax(
		{
			type:"post",
			url: "<?php echo base_url(); ?>index.php/patientController/Patient/filename_exists",
			data:{ fieldval:fieldval,btnval:btnval,pageval:'doctor'},
			success:function(response)
			{
			console.log(response);
			
				if (response !=0) 
				{
					if(btnval=='email')
					{
						var msg='User is already exists';
						$('#msg').html('<span style="color:#ff0700;">'+msg+"</span>");
						$('#email').val('');
						$('#email').focus();
						}
					else{
						var msg='User Contact is already exists';
						$('#contact_nomsg').html('<span style="color:#ff0700;">'+msg+"</span>");
						$('#contact_no').val('');
						$('#contact_no').focus();
					}
				}
				else
				{
					
					if(btnval=='email')
					{
						$('#msg').html("");
					}
					else{
						$('#contact_nomsg').html("");
					}
				} 
			 
				
			}
		});
	}
	
		</script>				