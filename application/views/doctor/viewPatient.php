<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");
?>
	
     <div class="page-wrapper">
       
            <div class="container-fluid">
                <div class="row">
                    <div class="col s12 l12">
                        <div class="card">
                            <div class="card-content">		 <?php
										 
                    foreach ($viewClinic as $key => $data) {
					?>
					 			
                    <h5 class="card-title activator">View Registration 
					<a  href = "javascript:window.history.go(-1);"  class="btn waves-effect waves-light grey darken-4 pull-right" type="submit" name="action">Back</a>
								</h5>
                 		<div class="card-content">
								
						<form class="h-form">
                                <div class="form-body">
                                    <div class="divider"></div>
                                    <div class="card-content">
                                        <h6 class="font-medium">Personal Details</h6>
                                        <div class="row">
                                            <div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s3">
                                                        <div class="h-form-label">
                                                            <label for="f-nameh">First Name : </label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s9">
                                                        <label><?php echo $data['fullName']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s4">
                                                        <div class="h-form-label">
                                                            <label for="l-nameh">DOB/Age :</label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s7">
                                                       <label><?php echo date("d-m-Y",strtotime($data['dob']))."( ".$data['age']." )"; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      <div class="row">
                                            <div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s3">
                                                        <div class="h-form-label">
                                                            <label for="email3">Email :</label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s9">
                                                       <label><?php echo $data['email']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s4">
                                                        <div class="h-form-label">
                                                            <label for="con_no1">Contact No.:</label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s7">
                                                         <label><?php echo $data['contact_no']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="card-content">
                                        <h6 class="font-medium">Other Details</h6>
										  <div class="row">
                                            <div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s6">
                                                        <div class="h-form-label">
                                                            <label for="u_name1">Systematic History : </label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s6" style="word-break: break-all;">
                                                        <label><?php echo $data['systemic_history']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s4">
                                                        <div class="h-form-label">
                                                            <label for="n_nameh">Allergy History :</label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col l6" style="word-break: break-all;">
                                                         <label><?php echo $data['allergy_history']; ?></label>
                                                    </div>
                                                </div>
                                            </div><div class="col s12 l6">
                                                <div class="row">
                                                    <div class="col s4">
                                                        <div class="h-form-label">
                                                            <label for="n_nameh">Reviews :</label>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s6" style="word-break: break-all;">
                                                         <label><?php echo $data['reviews']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div> 
							  </div>
                                
					<?php } ?>
						
						</div>
					</div>
                         </div>
                    </div>
                  </div>
           </div>
             
          </div>
    
<?php
	$this->load->view("admin_includes/footer");
?>				
</div>
