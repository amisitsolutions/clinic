<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
	$this->load->view("admin_includes/sidebar");

?>  
<div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Doctor Details
							 </h5><a href="<?php echo base_url(); ?>index.php/doctorRegistration" class="btn btn-primary btn-sm pull-right">
								 <i class="material-icons"></i> Doctor Registration</a>
                                                 </div>
                                                <div class="card-block"> 
                                	
							  <div class="dt-responsive table-responsive">
                                <table id="dom-table" class="table table-striped table-bordered nowrap">
								     <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Name</th>
                                       
                                            <th>Email/Contact</th>  
                                            <th>Qualification/Affillation</th>                                 
											<th>Reg date</th> 
											<th>Speciality</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cnt = 1;
								 
									for ($i = 0; $i < count($doctorData);$i++) {
						 			?>
									
									<tr class="odd gradeX">
									<td><?php echo $cnt++; ?></td>
                                        <td><?php echo $doctorData[$i]->doctorName; ?></td>
                                       
                                        <td><?php echo $doctorData[$i]->doctorEmail."</br>". $doctorData[$i]->doctorContact; ?></td>
                                      	   <td><?php echo $doctorData[$i]->doctorQualification."</br><b>Affilation :</b>". $doctorData[$i]->doctorAffilation; ?></td>
                                         
                                        
									   <td><?php echo date("d-M-Y",strtotime($doctorData[$i]->dorcorRegDate)); ?></td>
                                       <td><?php  
										  for ($k = 0; $k < count($specalist); $k++)
												{
										 
												    $sp=explode(",",$doctorData[$i]->doctorSpeciality);
												   
												 	if(in_array($specalist[$k]->specialityID,$sp))
													{
												//echo $sp[$i];
														echo   $specalist[$k]->specialityName."</br>"; 
													} 
												} 
												?> </td>
                                      
									  <td>
									  <div class="btn-group btn-group-sm" style="float: none;">
                                      
                                        <a href="<?php echo base_url('index.php/editDoctor/' . $doctorData[$i]->doctorID); ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light"><span class="icofont icofont-ui-edit"></span></a>
									 
									 
										<a onclick="delete_data('<?php echo $doctorData[$i]->doctorID; ?>');"class="tabledit-delete-button btn btn-danger waves-effect waves-light " > <span class="icofont icofont-ui-delete"></span></a>
										</div>
										</td>
										
									</tr>
									<?php } ?>
                                    </tbody>
                                </table>
                    	
					</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
<?php
	$this->load->view("admin_includes/footer");
?>				
</div><!-- ./wrapper -->	
 
	<script>	
		 function delete_data(catid){	
			bootbox.confirm("Are you sure,You want to Delete", function (msg) {
					 if (msg == true) {
							location='<?php echo base_url('index.php/deleteDoctor/'); ?>'+catid;
						}
						
					}); 
					 
		 }
		
</script>