<?php
	defined('BASEPATH') OR exit('No direct script access allowed');	
	$this->load->view("admin_includes/head");
	$this->load->view("admin_includes/header");
 
			$this->load->view("admin_includes/sidebar");
		?>		
		<!-- Content Wrapper. Contains page content -->
		<div class="page-wrapper">
      
    <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                 
                                <div class="page-body">
                                    <div class="row">
                                        <div class="col-sm-12"> 
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Update Profile</h5>
                                                 </div>
                                                <div class="card-block"> 
                                                <form action="<?php echo base_url(); ?>index.php/save_update_profile" class="form-horizontal" id="block-validate" method="POST" enctype="multipart/form-data" >
                                           			
												<?php
											 
												foreach ($profile_data as $key => $data) {
												?>	<input type="hidden" name="tbl_user_id" value="<?php echo $data['userID']; ?>" id="tbl_user_id">
                                                    <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">  Name <span class="required">*</span></label>
                                                                <div class="col-sm-6">
                                                               	<input type="text" required name="super_name" id="super_name" class="form-control" placeholder="Enter Name" value="<?php echo $data['userName']; ?>">
                                                                    <span class="messages"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Email  <span class="required">*</span></label>
                                                                <div class="col-sm-6">
                                                              <input type="text" required name="super_email_id" id="super_email_id" class="form-control" placeholder="Enter Email ID" value="<?php echo $data['userEmail']; ?>">
                                                                    <span class="messages"></span>
                                                                </div>
																
                                                            </div>
															      <div class="form-group row">
                                                                <label class="col-sm-3 col-form-label">Contact  <span class="required">*</span></label>
																   <div class="col-sm-6">
                                                             <input type="text" onkeypress="return isNumber(event);"  maxlength="10" minlength="10"  required name="super_contact" id="super_contact" class="form-control" placeholder="Enter Contact No." value="<?php echo $data['userContact']; ?>">
                                                                    <span class="messages"></span>
                                                                </div>
																
                                                            </div>
															      
														  <div class="form-group row">
													<label class="col-md-3 control-label">UserName :<span class="required">*</span></label>
											   <div class="col-sm-6">
														<input type="text" required name="userName" id="userName" class="form-control" placeholder="Enter UserName" value="<?php echo $data['username']; ?>">
													</div>
												</div>	
										 
												  <div class="form-group row">
													<label class="col-md-3 control-label">Password :<span class="required">*</span></label>
													<div class="col-md-6">
														<input type="password" required name="password" id="password" class="form-control" value="<?php echo $data['password']; ?>" />
													</div>
												</div>
															
															<?php } ?>
                                                            <div class="row">
                                                                <label class="col-sm-3"></label>
                                                                <div class="col-sm-6">
                                                                <?php if(!empty($profile_data)) 
                                                                {
								                                	?>
										<button class="btn btn-sm btn-primary m-b-0" type="submit" name="update_type_btn" value="update_type_btn">Update</button> 
									<?php } else { ?>
								 <button class="btn btn-sm btn-primary m-b-0" type="submit" name="add_type_btn" value="add_type_btn">Submit
									</button>
									 <?php } ?>
									 
									 
                                                                </div>
                                                            </div>
                                                    </form>
</div>
</div>
</div>  
        </div><!-- /.c /.con-->	
	<?php
		$this->load->view("admin_includes/footer");
	?>				
</div><!-- ./wrapper -->	

 <script>
    $(function() {
        $("#block-validate").validate({
            rules: {
                super_name: {
                    required: true
                },
                 clinic_name: {
                    required: true
                },
                super_email_id: {
                    required: true,
                    email: true
                },
                userName: {
                    required: true
                },
                  password: {
                    required: true
                },
               
                super_contact: {
                    required: true,
                    minlength: 10
                }
            },
            //For custom messages
            messages: {
                super_name: {
					required: "Enter a Full Name"
                }, 
				  clinic_name: {
					required: "Enter a Clinic Name"
                }, 
				super_email_id: {
                    required: "Enter a Email"
                }, 
				userName: {
                    required: "Enter a User Name"
                },
               	password: {
                    required: "Enter a Password"
                }, 
				super_contact: {
                    required: "Enter a Contact No."
                }
            },
            errorElement: 'div',
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(e, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('.error-alert-bar').show();
                }
            },
        });
    });
    </script>