<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include "includes/head.php";
?>
 <style>
 body{
     text-transform: none !important;
 }
 .box {
    border-radius: 3px;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    padding: 10px;
    text-align: right;
    display: block;
    margin-top: 60px;
	background:#2E363F;
}
.box-icon {
    background-color: #57a544;
    border-radius: 50%;
    display: table;
    height: 100px;
    margin: 0 auto;
    width: 100px;
    margin-top: -61px;
}
.box-icon span {
    color: #fff;
    display: table-cell;
    vertical-align: middle;
}
.info h4 {
    font-size: 26px;
    letter-spacing: 2px;
    text-transform: uppercase;
}
.info > p {
       color: #fff;
    font-size: 16px;
    padding-top: 10px;
    text-align: left;
    margin-left: 15%;
    margin-bottom: 50px;
}
.info > a {
    background-color: #03a9f4;
    border-radius: 2px;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    color: #fff;
    transition: all 0.5s ease 0s;
}
.info > a:hover {
    background-color: #0288d1;
    box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.12);
    color: #fff;
    transition: all 0.5s ease 0s;
}
 </style>
<body class="login-body">        
		    <div id="loginbox"> 
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="box">
             
                <div class="info">
                    <div class="control-group normal_text"> <h3><img src="<?php echo base_url(); ?>assets/img/logo-appeat.png" style="height: 130px;" alt="Logo" /></h3></div>
        
                    <p>Thank you for password has been changed.</br></br>You can go back to the app to log in.</p>
                    <!-- <a href="" class="btn"> You can go back to the app to log in.</a> -->
                </div>
            </div>
        </div>
        
    </div>
    </div>
</div>