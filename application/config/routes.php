<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin_dashboard'] = 'dashboardController/Dashboard'; 
//configuration

$route['branchLogin'] = 'Login/branchLogin';
$route['checkLogin'] = 'Login/checkLogin';
$route['admin_dashboard'] = 'dashboardController/Dashboard'; 
$route['appointmentLink/(.*)'] = 'Welcome/appointmentLink/$1'; 
 

$route['appointment_link'] = 'appointmentController/Appointment/appointmentLinkPanel';
$route['appointment_link'] = 'appointmentController/Appointment/appointmentLinkPanel';

$route['getDoctorsByspecSess'] = 'appointmentController/Appointment/getDoctorsByspecialitysessionBranches'; 

$route['AppointmentAvailability'] = 'appointmentController/Appointment/AppointmentAvailability'; 
$route['timingAvailability'] = 'appointmentController/Appointment/timingAvailability'; 


$route['saveAppointment'] = 'appointmentController/Appointment/save_appointment';
$route['addAppointment'] = 'appointmentController/Appointment/addAppointment';
$route['editappointment/(.*)'] = 'appointmentController/Appointment/editappointment/$1';  
$route['deleteappointment/(.*)'] = 'appointmentController/Appointment/deleteappointment/$1';

$route['saveAppointmentLinkPanel'] = 'appointmentController/Appointment/saveAppointmentLink_panel'; 
$route['saveAppointmentLink'] = 'Welcome/saveAppointmentLink'; 
$route['getDoctorsByspec'] = 'Welcome/getDoctorsByspecialityBranches'; 
$route['checkAppointmentAvailability'] = 'Welcome/checkAppointmentAvailability'; 
$route['getExistingPatientdata'] = 'patientController/Patient/getExistingPatientdata'; 

$route['set_patient/(.*)'] = 'patientController/Patient/set_patient/$1';
$route['prescriptionDetails'] = 'appointmentController/Appointment/prescriptionDetails';
$route['appointmentView/(.*)'] = 'appointmentController/Appointment/appointmentView/$1';

$route['get_PatientData'] = 'appointmentController/Appointment/get_PatientData';
$route['save_prescription'] = 'appointmentController/Appointment/save_prescription';

$route['addPrescription'] = 'appointmentController/Appointment/addPrescription';
$route['saveVisit'] = 'appointmentController/Appointment/saveVisit';
$route['invoiceDetails'] = 'appointmentController/Appointment/invoiceDetails';
$route['patientInvoice'] = 'appointmentController/Appointment/patientInvoice';

//profile
$route['myprofile'] = 'Myprofile'; 
$route['save_update_profile'] = 'Myprofile/save_update_profile';
 
$route['clinicprofile'] = 'Myprofile/clinicprofile'; 
$route['save_clinic_profile'] = 'Myprofile/save_clinic_profile';
 
 //Patient
$route['patientDetails'] = 'patientController/Patient/patientDetails';
$route['newRegistration/(.*)'] = 'patientController/Patient/newRegistration/$1';
$route['patientRegistration'] = 'patientController/Patient/patientRegistration';
$route['viewPatient/(.*)'] = 'patientController/Patient/viewPatient/$1';

$route['editPatient/(.*)'] = 'patientController/Patient/editPatient/$1';
$route['deletePatient/(.*)'] = 'patientController/Patient/deletePatient/$1';
$route['savepatientRegistration'] = 'patientController/Patient/savepatientRegistration';
$route['getPatientdata'] = 'patientController/Patient/getPatientdata';
$route['getPatientDataMRD'] = 'patientController/Patient/getPatientDataMRD';
$route['PatientDataMRD'] = 'patientController/Patient/PatientDataMRD';
$route['savePatientInvoice'] = 'appointmentController/Appointment/savePatientInvoice';

$route['getMRDData'] = 'appointmentController/Appointment/getMRDData';

$route['addSpeciality'] = 'masterController/Master/addSpeciality'; 
$route['editSpeciality/(.*)'] = 'masterController/Master/editSpeciality/$1';
$route['deleteSpeciality/(.*)'] = 'masterController/Master/deleteSpeciality/$1';
$route['saveSpecialityRegistration'] = 'masterController/Master/saveSpecialityRegistration';


//role

$route['addRole'] = 'masterController/Master/addRole'; 
$route['editRole/(.*)'] = 'masterController/Master/editRole/$1';
$route['deleteRole/(.*)'] = 'masterController/Master/deleteRole/$1';
$route['saveRoleRegistration'] = 'masterController/Master/saveRoleRegistration';

//servie type


$route['serviceTypes'] = 'masterController/Master/serviceTypes';
$route['saveserviceTypes'] = 'masterController/Master/saveserviceTypes';
$route['editserviceTypes/(.*)'] = 'masterController/Master/editserviceTypes/$1';  
$route['deleteserviceTypes/(.*)'] = 'masterController/Master/deleteserviceTypes/$1';
//servies
$route['servicesDetails'] = 'masterController/Master/servicesDetails';
$route['saveservices'] = 'masterController/Master/saveservices';
$route['editservices/(.*)'] = 'masterController/Master/editservices/$1';  
$route['deleteservices/(.*)'] = 'masterController/Master/deleteservices/$1';


$route['timingDetails'] = 'masterController/Master/timingDetails';  
$route['saveTiming'] = 'masterController/Master/saveTiming';
$route['editTiming/(.*)'] = 'masterController/Master/editTiming/$1/$2';   
$route['deletetiming/(.*)'] = 'masterController/Master/deletetiming/$1';


$route['insuranceDetails'] = 'masterController/Master/insuranceDetails';
$route['saveinsurance'] = 'masterController/Master/saveinsurance';


$route['editInsurance/(.*)'] = 'masterController/Master/editInsurance/$1'; 

 

//$route['appointmentLink/(.*)'] = 'Welcome/appointmentLink/$1'; 

$route['appointment_link'] = 'appointmentController/Appointment/appointmentLinkPanel';



 
 //user

 $route['userDetails'] = 'patientController/Patient/userDetails';
 $route['userRegistration'] = 'patientController/Patient/userRegistration';
 $route['viewUser/(.*)'] = 'patientController/Patient/viewUser/$1';
 $route['editUser/(.*)'] = 'patientController/Patient/editUser/$1'; 
 
 $route['deleteUser/(.*)'] = 'patientController/Patient/deleteUser/$1'; 
 
 $route['saveUserRegistration'] = 'patientController/Patient/saveUserRegistration';
 

 
 //Docotr
$route['doctorDetails'] = 'patientController/Patient/doctorDetails';
$route['doctorRegistration'] = 'patientController/Patient/doctorRegistration';
$route['doctorRegistered/(.*)'] = 'patientController/Patient/doctorRegistered/$1';
$route['viewDoctor/(.*)'] = 'patientController/Patient/viewDoctor/$1';
$route['editDoctor/(.*)'] = 'patientController/Patient/editDoctor/$1'; 

$route['deleteDoctor/(.*)'] = 'patientController/Patient/deleteDoctor/$1';


$route['saveDoctorRegistration'] = 'patientController/Patient/saveDoctorRegistration'; 


$route['getAppointment'] = 'appointmentController/Appointment/getAppointment';

$route['set_patient/(.*)'] = 'patientController/Patient/set_patient/$1';
$route['appointmentsDetails'] = 'appointmentController/Appointment/appointmentsDetails';
$route['appointmentView/(.*)'] = 'appointmentController/Appointment/appointmentView/$1';

$route['get_PatientData'] = 'appointmentController/Appointment/get_PatientData';
$route['save_prescription'] = 'appointmentController/Appointment/save_prescription';

$route['addPrescription/(.*)'] = 'appointmentController/Appointment/addPrescription/$1/$2';
$route['saveVisit'] = 'appointmentController/Appointment/saveVisit';
$route['invoiceDetails'] = 'appointmentController/Appointment/invoiceDetails';
$route['patientInvoice'] = 'appointmentController/Appointment/patientInvoice';

$route['getmedicineTypeList'] = 'appointmentController/Appointment/getmedicineTypeList';

$route['get_medicines'] = 'appointmentController/Appointment/get_medicines';


$route['selectBranch'] = 'Login/selectBranch';
$route['getBranch'] = 'masterController/Master/getBranch';
$route['myformAjax'] = 'masterController/Master/myformAjax';
$route['branchPrefix'] = 'masterController/Master/branchPrefix';
$route['savePrefix'] = 'masterController/Master/savePrefix';

//servie types
$route['getservices'] = 'masterController/Master/getservicesById';
$route['getservicescharges'] = 'masterController/Master/get_services_charges';
$route['get_consultant_charges'] = 'masterController/Master/get_consultant_charges';

$route['serviceTypes'] = 'masterController/Master/serviceTypes';
$route['saveserviceTypes'] = 'masterController/Master/saveserviceTypes';
$route['editserviceTypes/(.*)'] = 'masterController/Master/editserviceTypes/$1';  
$route['deleteserviceTypes/(.*)'] = 'masterController/Master/deleteserviceTypes/$1';



//profile
$route['myprofile'] = 'Myprofile'; 
$route['save_update_profile'] = 'Myprofile/save_update_profile';
 
$route['clinicprofile'] = 'Myprofile/clinicprofile'; 
$route['save_clinic_profile'] = 'Myprofile/save_clinic_profile';
 

//online appointments
$route['doctorAppointments'] = 'appointmentController/Appointment/doctorAppointments';  
$route['getdoctorAppointments'] = 'appointmentController/Appointment/getdoctorAppointments';  
$route['get_onlinePatientData'] = 'appointmentController/Appointment/get_onlinePatientData';

$route['saveOnlineAppointment'] = 'appointmentController/Appointment/saveOnlineAppointment';
$route['getcityData'] = 'masterController/Master/getcityData';
$route['finish_meeting/(.*)'] = 'appointmentController/Appointment/finish_meeting/$1';

$route['joinMeeting/(.*)'] = 'appointmentController/Appointment/joinMeeting/$1';
