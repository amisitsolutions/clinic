<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	class ProfileModel extends CI_Model
	{
		function __construct() 
		{
        // Call the Model constructor
			parent::__construct(); 
		   $this->db2=$this->load->database('dynamicdb', TRUE);
			$this->load->library('session');
		}

    public function getProfile() 
	{
		if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
		 	$login_type = $this->session->userdata('admin_type');
		 	$login_id = $this->session->userdata('admin_id'); 
 			if ($login_type == 'admin' || $login_type == 'user')
			{
				  $sql = "select * from tblUserMaster u,tblLogin l  where u.userID=l.logUserID   and u.userID=".$login_id;
		 		
			}
			else if($login_type == 'doctor') {
				$sql = "select * from tblDoctorMaster d,tblLogin l  where d.doctorID=l.logUserID and l.loginType='doctor' and d.doctorID=".$login_id;
		 		
			}
			$query = $this->db2->query($sql);
			return $query->result_array();
		}
    }
    public function clinicprofile($sess_clinic_id) 
	{
		if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{ 
				$sess_clinic_id=1;
				$sql = "select * from tblClinicMaster  where clinicID=".$sess_clinic_id;
		 		
			$query = $this->db2->query($sql);
			return $query->result_array();
		}
    }
     public function save_update_profile($super_name,$super_email_id,$super_contact,$userName,$password,$tbl_user_id)
	{
				 

      		$sql = "update tblUserMaster set userName='".$super_name."' ,userContact='".$super_contact."', userEmail='".$super_email_id."' where userID=".$tbl_user_id;
      	
				$result = $this->db2->query($sql);
			
      		$sql2 = "update tblLogin set username='".$userName."', password='".$password."'    where logUserID=".$tbl_user_id;
      	
				$result = $this->db2->query($sql2);
			
				$_SESSION['success']='Profile  Updated Successfully';


       redirect(base_url() . 'index.php/admin_dashboard');
    }
	
	public function save_clinic_profile($clinic_name,$clinic_email,$clinic_contact,$clinic_info,$website,$clinic_id)
	{
				 
		    		$sql = "update tblClinicMaster set clinicName='".$clinic_name."' ,clinicContact='".$clinic_contact."', clinicInfo='".$clinic_info."' , clinicEmail='".$clinic_email."', clinicWebsite='".$website."' where clinicID=".$clinic_id;
      	
				$result = $this->db2->query($sql);
			
				$_SESSION['success']='Profile  Updated Successfully';
 

       redirect(base_url() . 'index.php/admin_dashboard');
    }
}

?>				