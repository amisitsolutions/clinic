<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AppointmentModel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
	$this->db2=$this->load->database('dynamicdb', TRUE);
        $this->load->library('session');
    }

   
  public function getappointments() {
	if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	{
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{
			$login_type = $this->session->userdata('admin_type');
			if ($login_type == 'admin') {
			 	
				
			$query = $this->db2->query("select * from tblAppointment app,tblDoctorMaster d where d.doctorID=app.doctorID order by appoID desc limit 1");
        return $query->result_array();
		}
		}
    } 
   public function gettodaysappointments($pid) {
		if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
				$cdate=date('Y-m-d');
				$login_type = $this->session->userdata('admin_type');
				if ($login_type == 'admin') {
					$sql = "select * from tblAppointment  where patientID=$pid and appoDate='$cdate'";
				}
				$query = $this->db2->query($sql);
				$result = $query->result();
				return $result;
		}
		
	
	}
	
	
	public function getappointmentsPatients($aid) {
		if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
			$cdate=date('Y-m-d');
			$login_type = $this->session->userdata('admin_type');
			 $sql = "select * from tblAppointment  where  appoID=$aid";
			 
			$query = $this->db2->query($sql);
			$result = $query->result_array();
			return $result;
		}
    }
     
	  public function checkDoctorAvailability($branch_id,$doctor_id,$app_date) {
		 if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
		  
			$login_type = $this->session->userdata('admin_type');
			$admin_id = $this->session->userdata('admin_id'); 
			 	
		 	$cdate=date('l',strtotime($app_date));
		 	       $sql = "select d.* ,t.* from tblDoctorMaster d,tbltimeschedule t where t.doctorID=d.doctorID and t.doctorID='$doctor_id'  and t.timingDay='$cdate' ";
		 	 	$query = $this->db2->query($sql);
			$result = $query->result();
			return $result;
			 
		}
    } 
	  public function gettodaysappointmentsDoctors($tdate) {
		 if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
			 echo $tdate;
		   	$cdate=date('l',strtotime($tdate));
			$login_type = $this->session->userdata('admin_type');
			$admin_id = $this->session->userdata('admin_id');
			if ($login_type == 'admin' || $login_type == 'user') {
			 	
				echo           $sql = "select d.* ,t.* from tblDoctorMaster d,tbltimeschedule t where      t.doctorID=d.doctorID and t.timingDay='$cdate'";
		 	   
			}
			else if ($login_type == 'doctor') {
			  $sql = "select app.*,d.*  from tbltimeschedule app,tblDoctorMaster d  where  app.timingDay='$cdate' and d.doctorID=app.doctorID   and app.doctorID='$admin_id' ";
			}
			 
			$parent = $this->db2->query($sql);
			$categories = $parent->result();
 
		 	$i=0;
			foreach($categories as $p_cat){
 
				$categories[$i]->appoint_patientData = $this->gettodaysappointmentsPatients($p_cat->doctorID,$tdate,$p_cat->fromTime,$p_cat->toTime);
				$i++;
			}  
			return $categories;
		}
    } 
	
	
	
       public function gettodaysappointmentsPatients($doctor_id,$tdate,$from_time,$to_time) {
	if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	{
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{ 
		$login_type = $this->session->userdata('admin_type');
		$admin_id = $this->session->userdata('admin_id');
	   	$cdate=date('Y-m-d',strtotime($tdate));
			if ($login_type == 'admin' || $login_type == 'user') {
			 	
		 	           $sql = "select *  from tblAppointment  where appoDate='$cdate' and doctorID=$doctor_id";// and STR_TO_DATE( appoTime, '%l:%i %p' ) between STR_TO_DATE( '$from_time', '%l:%i %p' ) and STR_TO_DATE( '$to_time', '%l:%i %p' )";
					
					//and a.appointment_time between '$from_time' and '$to_time'";
		 	   
			}
			else if ($login_type == 'doctor') {
			  $sql = "select app.*,d.*  from tblAppointment app,tblDoctorMaster d  where  app.appoDate='$cdate' and d.doctorID=app.doctor_id   and app.doctor_id='$admin_id' ";
			}
			$query = $this->db2->query($sql);
			$result = $query->result();
			return $result;
		}
    }
   
  public function getDiseaseList() {
	if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	{
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{
			$login_type = $this->session->userdata('admin_type');
			if ($login_type == 'admin') {
			 	$sql = "select * from tbl_disease_master";
			}
		
			$query = $this->db2->query($sql);
			$result = $query->result();
			return $result;
		}
    } 
 
   
     public function insertAppointment($doctor_id, $reschdule_val, $reschdule,$app_date,$app_time ,$appointment_id, $cat_button, $remarks, $patient_id)
	{
				if($reschdule=='cancel')
				{
					 $created_date=date('Y-m-d');
					 $app_time=date('h:i A');
					 $sql1 = "insert into  tblAppointmentHistory  (patientID,appoID,remark,appoHistoryDate,appoHistoryTime)values('".$patient_id."','".$appointment_id."' ,'".$remarks."','".$created_date."' ,'".$app_time."' )";	
					 $result = $this->db2->query($sql1);
				  $sql = "update tblAppointment set appoStatusFlag=2   where appoID=".$appointment_id;	
				}
				else{
						$app_date=date('Y-m-d',$app_date);
						$sql = "update tblAppointment set doctorID='".$doctor_id."',	appoDate='".	$app_date."' ,	appoTime='".$app_time."'   where appoID=".$appointment_id;	
			 	}
				$result = $this->db2->query($sql);
         
		  
        redirect(base_url() . 'index.php/appointmentsDetails');
    } 

	
	
     public function getPatientById($id) {
       $query = $this->db2->query("SELECT * FROM tblPatientRegistration WHERE patientID = $id");
        return $query->result_array();
    }
  
 
    public function insertAppointmentsFromPanel($patient_id,$patient_type,$clinic_id,$mrd_no,$fullname, $dob,$age,$email,$contact_no,$gender,$branch_id,  $speciality, $doctor_id,	$app_date, $app_time, $remarks)
	{
	    $created_date=date("Y-m-d");
	    $appointment_date=date("Y-m-d",strtotime($app_date));
		$sql = "insert into  tblAppointment (patientID,patientType,mrdNO,fullName,contactNO,emailID,branchID,	dob,age,gender,	specialityID,	doctorID,	problemDetails,appoDate,	appoTime,appoCreateDate) values ('".$patient_id."','".$patient_type."','".$mrd_no."','".$fullname."','".$contact_no."','".$email."','".$branch_id."','".$dob."','".$age."','".$gender."','".$speciality."', '".$doctor_id."',  '".$remarks."','".$appointment_date."','".$app_time."','".$created_date."' )";

	   	 $result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/appointmentsDetails');
    } 
	//get last invoice no.
	public function getInvoiceLastNo() {
	if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	{
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{
			$login_type = $this->session->userdata('admin_type');
			if ($login_type == 'admin') {
			 	  $sql = "select top 1 * from tbl_invoice order by tbl_invoice_id desc";
			}
			$query = $this->db2->query($sql);
			$result = $query->result_array();
					return $result;
		}
    }  
	
	 public function insertPatientInvoice($mrd_no ,$invoice_date ,$ref_by ,$visit_no,$visit_date,$insurance_no,$patient_id,$doctor_id,$insurance_id,$ref_no,$insurance_deatials,$type_id,$speciality,$qty,$rate,$total,$discount_type,$discount,$discount_ref_no,$service_charges,$final_total,$cash,$cc,$credit,$online_details,$credit_details,$cc_details,$cash_details ,$paid_total,$unpaid_total,$remarks)
	{
	    $created_date=date("Y-m-d");
	    $appointment_date=date("Y-m-d",strtotime($app_date));
         /* 
			    $sql = 'insert into  tbl_invoice (tbl_invoice_id,invoice_prefix,invoice_no,invoice_date,bill_amount,patient_mrd_no,patient_id,doctor_id,ref_by_id,visit_no,invoice_created_date) values ("'.$patient_id.'","'.$patient_type.'","'.$mrd_no.'","'.$fullname.'","'.$contact_no.'","'.$email.'","'.$branch_id.'","'.$dob.'","'.$age.'","'.$gender.'","'.$speciality.'", "'.$doctor_id.'",  "'.$remarks.'","'.$appointment_date.'","'.$app_time.'","'.$created_date.'" )';
		 $result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/prescriptionDetails'); */
	} 
	
	public function get_medicines($med) {
		  	 $sql = "select * from tblMedicine  where medicineName like '%$med%'";
			 	$query = $this->db2->query($sql);
				$result = $query->result();
				return $result;
		}
		 //prescription4

		 public function insertPrescription($patient_id,$branch_id,$type_of_dm,$complication_of_dm,$associated_illness, $present_complaints,$type_id,$medicine_name,$usage,$dosage,$advice, $target,$prescription_id)
		 {
			 $created_date=date("Y-m-d");
			  if ($cat_button=='add_time') 
			  {
					  for($k=0;$k<count($type_id);$k++)
					 {
						 $medtype=$type_id[$k];
						 $medicine=$medicine_name[$k];
						 $sql ="insert into   tblPrescription (patientID,mrdNO,typeOfDM,complicationofDM,assoIllness,presentComplaints,advice,target,medicineType,medicineName,usage,dosage,noOfDays,branchID) values ('".$patient_id."','".$mrdNO."','".$type_of_dm."','".$complication_of_dm."','".$present_complaints."','".$advice."','".$target."','".$medtype."','".$medicine."','".$usage."','".$dosage."','".$noOfDays."','".$branch_id."')";	$result = $this->db2->query($sql); 
						$time_id=$this->db2->insert_id();
					 }
					 
			 }
			 else
			 {
	  
				  for($k=0;$k<count($from_timedata);$k++)
					 {
						 if($direct_invoice_product_id[$i]=='')
							 {	
								for($k=0;$k<count($type_id);$k++)
								{
									$medtype=$type_id[$k];
									$medicine=$medicine_name[$k];
									 $sql ="insert into   tblPrescription (patientID,mrdNO,typeOfDM,complicationofDM,assoIllness,presentComplaints,advice,target,medicineType,medicineName,usage,dosage,noOfDays,branchID) values ('".$patient_id."','".$mrdNO."','".$type_of_dm."','".$complication_of_dm."','".$present_complaints."','".$advice."','".$target."','".$medtype."','".$medicine."','".$usage."','".$dosage."','".$noOfDays."','".$branch_id."')";
									$result = $this->db2->query($sql); 
									$time_id=$this->db2->insert_id();
								 }
							 }
						 else{
							 for($k=0;$k<count($type_id);$k++)
							 {
							$medtype=$type_id[$k];
							$medicine=$medicine_name[$k];
							$sql = "update   tblPrescription set patientID	='".$patient_id."',typeOfDM='".$type_of_dm."',complicationofDM='".$complication_of_dm."' ,presentComplaints='".$present_complaints."' ,advice='".$advice."',target='".$target."'   where prescriptionID=".$prescription_id;	 
							 $result = $this->db2->query($sql); 
						 }
					 }
			 }
			   redirect(base_url() . 'index.php/timingDetails');
			 
		 }
		}
	 //online appointments
	 
	 public function online_gettodaysappointmentsDoctors($tdate,$doc) {
		if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	   {
		   echo  header('location:'.base_url().'index.php/Login/logout');	
	   }
	   else
	   {
		 
			  $cdate=date('l',strtotime($tdate));
		   $login_type = $this->session->userdata('admin_type');
		   $admin_id = $this->session->userdata('admin_id');
		  if ($login_type == 'doctor') {
			 $sql = "select app.*,d.*  from tbltimeschedule app,tblDoctorMaster d  where  app.timingDay='$cdate' and d.doctorID=app.doctorID   and app.doctorID= $admin_id ";
		   }
		   else if ($login_type == 'admin' || $login_type == 'user') {
			  $sql = "select app.*,d.*  from tbltimeschedule app,tblDoctorMaster d  where  app.timingDay='$cdate' and d.doctorID=app.doctorID   and app.doctorID= '$doc' ";
		   }
			
		   $parent = $this->db2->query($sql);
		   $categories = $parent->result();
 
		   return $categories;
	   }
   } 
   
   
   
		  public function online_gettodaysappointmentsPatients($tdate,$doc)
		   {
   if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
   {
	   echo  header('location:'.base_url().'index.php/Login/logout');	
   }
   else
   { 
	   $login_type = $this->session->userdata('admin_type');
	   $admin_id = $this->session->userdata('admin_id');
	  	$cdate=date('Y-m-d',strtotime($tdate));
		    
		     if ($login_type == 'doctor') {
		 		   
			 $sql = "select app.*,d.*  from tblAppointment app,tblDoctorMaster d  where  app.appoDate='$cdate' and d.doctorID=app.doctorID  and app.appointmentType='Online'  and app.doctorID='$admin_id' ";
		   }
			 else if ($login_type == 'admin' || $login_type == 'user') {
			  $sql = "select app.*,d.*  from tblAppointment app,tblDoctorMaster d  where  app.appoDate='$cdate' and d.doctorID=app.doctorID  and app.appointmentType='Online'  and app.doctorID='$doc' ";
		   }
			
		   $query = $this->db2->query($sql);
		   $result = $query->result();
		   return $result;
	   }
   }


   public function insertOnlineAppointment($app_date,$app_time ,$appointment_id, $cat_button, $remarks, $patient_id)
	{
		 $p_query 	        = 	 "select * from tblAppointment where appoID='$appointment_id'";
	 
		$query = $this->db2->query($p_query);
		$res = $query->result();
		 
	  	$email_id           =	$res[0]->emailID;
	  	$patient_name       =   $res[0]->fullName;
	  	$patient_no         =   $res[0]->contactNO;	
	  	$room               =   substr($patient_no, 0, 5);	
	  	 	$link               =   $room.''.$appointment_id;
	  	$new_url            =   base_url().'/clinic/join_meeting.php?id='.$link;
		//echo $new_url = get_tiny_url($linkks);
		
			
		$app_date=date('Y-m-d',strtotime($app_date));
	   	 $sql = "update tblAppointment set meeting_link='".$link."',	appoDate='".$app_date."' ,	appoTime='".$app_time."'   where appoID=".$appointment_id;	
		 $result = $this->db2->query($sql);
         $data = array();
				
					$data['reset_key'] = $new_url;
					$data['app_dt'] = $app_date;
					$data['app_time'] = $app_time;
					$data['patient_name'] = $patient_name;
				
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from('admin@ratnapriyaclap.com', 'Clinic Management');
					$this->email->to($email_id); 
					$this->email->subject('Register successfully');
				
					$message="register";
					$message=$this->load->view('meeting_map_mail_format',$data,TRUE);
					$this->email->message($message);
					$this->email->send(); 
					
					
					$message1   =   "Your appointment scheduled date : ".$app_date." and time : ".$app_time." to connect meeting check this link : ".$new_url; 
    
					$msg       =   str_replace(" ",'%20',$message1);
					$mob       =   "91".$patient_no;
				//	$url        =   "http://173.45.76.227/send.aspx?username=aspirationalbiz&pass=Amis12345&route=trans1&senderid=CDECLI&numbers=".$mob1."&message=".$msg1;
			$url   =   "http://sms2.iissms.co.in/V2/http-api.php?apikey=eYYP9s7k6qHjRiKX&senderid=VAIDYA&number=".$mob."&message=".$msg;	
					
					$urls       =   preg_replace("/ /", "%20", $url);
				
					$p1         =   file_get_contents($urls);
				  
        redirect(base_url() . 'index.php/doctorAppointments');
	} 
	 public function finishMeeting($id) {
			$query = $this->db2->query("update  tblAppointment set meetingFlag=1  WHERE appoID = '$id' ");
       redirect(base_url() . 'index.php/doctorAppointments');
    }
	function get_tiny_url($url)  {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}

?>				