<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MasterModel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
      // 	$this->load->database();
		 $this->db2=$this->load->database('dynamicdb', TRUE);
       // $this->load->library('session');
    }

   
  public function getSpecialityList() {
	 
		//	$login_type = $this->session->userdata('admin_type');
		 
				$sql = "Select *, (Select Count(*) from tblDoctorMaster WHERE (',' + tblDoctorMaster.doctorSpeciality + ',' like '%,' + cast(tblSpecialityMaster.specialityID as nvarchar(20)) + ',%' ))cnt  from tblSpecialityMaster";
				$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;

	 
    }
   
 
  public function getfeesDetails() {
	 
			 	$sql = "select * from tblFeeMaster"; 
			$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
		 
    }
      public function getdoctorsList() {
  
			 	$sql = "select * from tblDoctorMaster"; 
			$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }
     public function getstateList() {
  
			 	$sql = "select * from tblStateMaster where stateFlag=0 order by stateName asc"; 
			$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
			
    }
	public function getcityList() {
  
		$sql = "select * from tblCityMaster order by cityName asc"; 
		$query =  $this->db2->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getcityListByID($state) {
  
		$sql = "select * from tblCityMaster where stateID=$state order by cityName asc"; 
		$query =  $this->db2->query($sql);
		$result = $query->result();
		return $result;
	}
	public function getmedicineTypeList()
{
		$sql = "select * from tblmedicineType"; 
		$query =  $this->db2->query($sql);
		$result = $query->result();
		return $result; 
}
public function getmedicineList()
{
		$sql = "select * from tblMedicine"; 
		$query =  $this->db2->query($sql);
		$result = $query->result();
		return $result; 
}
     public function insertSpecialityRegistration($speciality_name, $spec_id, $cat_button)
	{
		 
          if ($this->input->post('add_spec_btn')) 
		 {
			 	
			echo     $sql = "insert into tblSpecialityMaster(specialityName) values ('".$speciality_name."')";
			//	$_SESSION['success']='Speciality Added Successfully';
        }
		else
		{
				
			$sql = "update tblSpecialityMaster set specialityName='".$speciality_name."'  where specialityID=".$spec_id;
			//$_SESSION['success']='Speciality Updated Successfully';
		}  
		$result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/addSpeciality');
    } 
   

    public function editSpeciality($id) {
       $query =  $this->db2->query("select * from tblSpecialityMaster WHERE specialityID = $id");
        return $query->result_array();
    }
   

   	 public function deleteSpeciality($id) {
			$query =  $this->db2->query("delete FROM tblSpecialityMaster  WHERE specialityID = '$id' ");
      
    }
   	  
	public function editfee($id) {
			$query =  $this->db2->query("select  *  from tblFeeMaster  WHERE fees_id = '$id' ");
       return $query->result_array();
    }
   	 public function deletefee($id) {
			$query =  $this->db2->query("delete FROM tblFeeMaster  WHERE fees_id = '$id' ");
      
    }
    
    //role
	
 	public function getroleList() {
  
			 	$sql = "select r.*,(SELECT COUNT(*) FROM tblDoctorMaster WHERE tblDoctorMaster.roleID=r.roleID) as cnt from tblRoleMaster r "; 
			$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }
     public function insertRoleRegistration($role_name, $role_id, $cat_button)
	{
	    $created_date=date("Y-m-d");
         if ($this->input->post('add_role_btn')) 
		 {
			 	
			   $sql = "insert into   tblRoleMaster ( roleName) values ('".$role_name."')";
        }
		else
		{
				
			$sql = "update   tblRoleMaster set roleName	='".$role_name."'   where roleID=".$role_id;
      
		}
		$result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/addRole');
    }

    public function editRole($id) {
       $query =  $this->db2->query("select * from   tblRoleMaster WHERE roleID = $id");
        return $query->result_array();
    }
  
   	 public function deleteRole($id) {
			$query =  $this->db2->query("delete FROM  tblRoleMaster  WHERE roleID = $id ");
      
    }
    
  
     //PRefix
    public function getPrefixDetails() {
 
			 	$sql = "select * from  tblPrefixMaster";
			 $query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
		 
    }
    public function getPrefixByType() {
		$sql = "select * from  tblPrefixMaster";
			 $query =  $this->db2->query($sql);
			$result = $query->result_array();
			return $result;
		 
    }

      public function insertPrefix(  $clinic_type, $branch_id, $prefix1, $branch_code, $year,$start_from, $prefix_id, $cat_button)
	{
	    $created_date=date("Y-m-d");
         if ($this->input->post('add_pre_btn')) 
		 {
			
			   $sql = "insert into   tblPrefixMaster (prefixType,	prefix1	,yearWise,	startFrom,	 branchID,branchCode) values (  '".$clinic_type."','".$prefix1."','".$year."','".$start_from."', '".$branch_id."', '".$branch_code."')";
        }
		else
		{
				
			$sql = "update   tblPrefixMaster set  prefixType='".$clinic_type."',prefix1='".$prefix1."' ,yearWise='".$year."'  ,startFrom='".$start_from."'  ,branchID='".$branch_id."',branchCode='".$branch_code."'   where tblPrefixMaster_id=".$prefix_id;
      
		}
		$result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/branchPrefix');
    }

    public function editPrefix($id) {
       $query =  $this->db2->query("SELECT * FROM  tblPrefixMaster WHERE tblPrefixMaster_id = $id");
        return $query->result_array();
    }
  
   	 public function deletePrefix($id) {
			$query =  $this->db2->query("delete FROM  tblPrefixMaster  WHERE tblPrefixMaster_id = '$id' ");
      
    }
    //branches
    public function getbranches() {
	 
			  $login_type = $this->session->userdata('admin_type');
			 
			  	$sql = "select * from tblBranchMaster";
			 
			$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }     public function getbranchesbyID($reg_no) {
	 
			  $login_type = $this->session->userdata('admin_type');
			 
			  	$sql = "select * from tblBranchMaster";
			 
			$query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    } 
	public function check_branch($branch_id, $sess_clinic_id, $admin_id, $admin_type)
	{
	 
			  $login_type = $this->session->userdata('admin_type');
			 
			  	$sql = "select * from tblBranchMaster";
			 
			$query =  $this->db2->query($sql);
			$result = $query->result_array();
			return $result;
	 
    }
   //service Types
    public function serviceTypesList() {
	  $sql = "SELECT u.*,(SELECT COUNT(*) FROM tblServiceMaster WHERE tblServiceMaster.serviceTypeID=u.serviceTypeID) as cnt
	  FROM tblServiceTypeMaster u";
		 $query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }
     public function insertServiceType($service_type, $service_type_code, $service_type_id, $cat_button)
	{
	    $created_date=date("Y-m-d");
         if ($this->input->post('add_type_btn')) 
		 {
			 	
			   $sql = "insert into    tblServiceTypeMaster (serviceType,serviceTypeCode) values ('".$service_type."','".$service_type_code."')";
        }
		else
		{
				
			$sql = "update   tblServiceTypeMaster set  serviceType='".$service_type."',serviceTypeCode='".$service_type_code."'   where serviceTypeID=".$service_type_id;
      
		}
		$result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/serviceTypes');
    }

    public function editserviceTypes($id) {
       $query =  $this->db2->query("SELECT * FROM  tblServiceTypeMaster WHERE serviceTypeID = $id");
        return $query->result_array();
    }
  
   	 public function deleteserviceTypes($id) {
		$query =  $this->db2->query("delete FROM  tblServiceTypeMaster  WHERE serviceTypeID = '$id' ");
      
    }
	
	//service Types
    public function getservicesById($type_id) {
	  $sql = "select * from tblServiceMaster m join tblServiceTypeMaster sy on sy.serviceTypeID =m.serviceTypeID and sy.serviceTypeID=".$type_id;
	   
		 $query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }//service Types
    public function servicesList() {
	 // $sql = "select m.*, group_concat(c.branch_name)branch,sy.* from tblServiceMaster m join tblServiceTypeMaster sy on sy.service_type_id =m.service_type_id join tblBranchMaster c on find_in_set(c.branch_id, m.branches) group by m.serviceID";
	  
	  
	  $sql = "select * from tblServiceMaster s,tblServiceTypeMaster sy where sy.serviceTypeID =s.serviceTypeID";
		 $query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }
	public function getServicesLastNo() 
	{
	  
					$sql = 'select * from  tblServiceMaster order by serviceID desc limit 1';
					$query =  $this->db2->query($sql);
					$result = $query->result_array();
					return $result;
		 
	} 
     public function insertServices($branches, $branchcharges,$service_name, $type_id,$service_code,$description,$notes,$remarks, $service_id, $cat_button, $unit, $multiply, $male_range, $female_range, $kid_range)
	{
	    $created_date=date("Y-m-d");
         if ($this->input->post('add_ser_btn')) 
		 {
			 	$branch=implode(",",$branches);
			 	$branch_charges=implode(",",$branchcharges);
				 $sql = "insert into   tblServiceMaster (serviceBranches,	serviceTypeID	,serviceName,	serviceCode,	branchCharges,serviceDesc,	serviceNote,	serviceRemark,
				 serviceUnit, serviceMultiply, serviceMaleRange, serviceFemaleRange, serviceKidRange) values (  '".$branch."','".$type_id."','".$service_name."','".$service_code."','".$branch_charges."', '".$description."', '".$notes."', '".$remarks."', '".$unit."', '".$multiply."', '".$male_range."', '".$female_range."', '".$kid_range."')";
			
			}
		else
		{ 
		 			$branch=implode(",",$branches);
				  $branch_charges=implode(",",$branchcharges);
				  $sql = "update   tblServiceMaster set  branchCharges='".$branch_charges."',serviceTypeID='".$type_id."' ,serviceName='".$service_name."' ,serviceCode='".$service_code."'    ,serviceBranches='".$branch."'  ,serviceDesc='".$description."'  ,serviceNote='".$notes."'   ,serviceRemark='".$remarks."',serviceUnit='".$unit."',serviceMultiply= '".$multiply."', serviceMaleRange='".$male_range."',serviceFemaleRange='".$female_range."' ,serviceKidRange='".$kid_range."'   where serviceID=".$service_id;

		}
		$result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php/servicesDetails');
    }
	
    public function editservices($id) {
       $query =  $this->db2->query("SELECT * FROM  tblServiceMaster WHERE serviceID = $id");
        return $query->result_array();
    }
  
   	 public function deleteservices($id) {
		$query =  $this->db2->query("delete FROM  tblServiceMaster  WHERE serviceID = '$id' ");
		
		 redirect(base_url().'index.php/servicesDetails');
      
    }
	
		 public function getbranchesByClinicID($clinic)
		{ 
		
		  	  $sql = "select * from tblBranchMaster";
			
			$query = $this->db2->query($sql);
			$result = $query->result();
			return $result;
			
		} 
		 	public function getDoctorsByspecialityBranches($branch,$spec)
		{ 
			$sql = "select * from tblDoctorMaster where  ( ',' + tblDoctorMaster.branchID + ',' like '%,' + cast('$branch' as nvarchar(20)) + ',%')
			and ( ',' + tblDoctorMaster.doctorSpeciality + ',' like '%,' + cast('$spec'as nvarchar(20)) + ',%') ";
		  
			
			//  $sql = "select * from tblDoctorMaster where  FIND_IN_SET('$branch',branch_id) and FIND_IN_SET('$spec',speciality)";
			
			$query = $this->db2->query($sql);
			$result = $query->result();
			return $result;
			
		}
	 
	 	public function checkAvailability($branch_id,$doctor_id,$app_date,$app_time)
		{ 
			$day=date('l',strtotime($app_date));
		  	 echo    $sql = "select * from tbltimeschedule where  doctorID=$doctor_id and timingDay='$day' and   str_to_date(fromTime,'%h:%i %p') < str_to_date('$app_time','%h:%i %p')
					or     str_to_date(toTime,'%h:%i %p') > str_to_date('$app_time','%h:%i %p')";

			//'$app_time' between from_time and to_time";
			
			$query = $this->db2->query($sql);
			 $cnt=$query->num_rows();
			//$result = $query->result();
			return $cnt;
			
		}
	 public function insertAppointments($appointmentType,$patient_id,$patient_type,$clinic_id,$mrd_no,$fullname, $dob,$age,$email,$contact_no,$gender,$branch_id,  $speciality, $doctor_id,	$app_date, $app_time, $remarks)
	{
	    $created_date=date("Y-m-d");
	    $appointment_date=date("Y-m-d",strtotime($app_date));
		$app_type=$appointmentType;
		$status_flag=0;
		$appo_flag=0;
		$meeting_flag='';
		
		$sql = "insert into tblAppointment (patientID,patientType,mrdNO,fullName,contactNO,emailID,branchID,	dob,age,gender,	specialityID,	doctorID,	problemDetails,appoDate,	appoTime,appoCreateDate,appointmentType,appoStatusFlag,appoFlag,meeting_link) values ('".$patient_id."','".$patient_type."','".$mrd_no."','".$fullname."','".$contact_no."','".$email."','".$branch_id."','".$dob."','".$age."','".$gender."','".$speciality."', '".$doctor_id."',  '".$remarks."','".$appointment_date."','".$app_time."','".$created_date."','".$app_type."','".$status_flag."','".$appo_flag.",'".$meeting_flag."' )";		  
			 
		$result =  $this->db2->query($sql);
        redirect(base_url() . 'index.php');
    } 
	//timing details
	 public function gettimingDetails() {
		//$sql = "select t.doctorID,t.branchID,t.timingDay,t.*,d.* from tbltimeschedule t,tblDoctorMaster d where t.doctorID=d.doctorID";
		 	$sql = "select t.doctorID,t.branchID,t.timingDay,t.*,d.doctorName from tbltimeschedule t inner join tblDoctorMaster d on t.doctorID=d.doctorID ";//,tblDoctorMaster d where d.doctorID=t.doctorID group by t.timingDay"; 
			$query =  $this->db2->query($sql);
			$categories = $query->result();
			$i=0;
			foreach($categories as $p_cat){
 
				$categories[$i]->daysData = $this->getDaysRecordds($p_cat->doctorID,$p_cat->branchID,$p_cat->timingDay);
				$i++;
			} 
		 
			return $categories;
			
		}
		  //timing
    public function get_services_charges($service_id) {
		 if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	{
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{
			$login_type = $this->session->userdata('admin_type');
			$branch_id = $this->session->userdata('sess_branch_id');
			if ($login_type == 'admin') {
			   	$sql = "select find_in_set('$branch_id', s.serviceBranches)as branch_index from    tblServiceMaster s where   s.serviceID=$service_id ";
			  
			}
			$query = $this->db2->query($sql);
			$row = $query->result(); 
			  $chrgesIndex=$row[0]->branch_index-1;
 
		 	$sql2 = "select SUBSTRING_INDEX(s.branchCharges, ',', $chrgesIndex) as charges from  tblServiceMaster s where   s.serviceID=$service_id";
		
			$query1 = $this->db2->query($sql2);
			$result1 = $query1->result(); 
			
			if(count($result1)==0)
			{
					$chargers=0;
			}
			else{
				$chargers=$result1[0]->charges;
			}
			
			 
			return $chargers;
		}
		
		
    }
	public function get_consultant_charges($doc_id) 
	{
		 if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		 {
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{
			$login_type = $this->session->userdata('admin_type');
			$branch_id = $this->session->userdata('sess_branch_id');
			if ($login_type == 'admin') {
			   	$sql = "select * from  tblFeeMaster s where  s.branchID=$branch_id and docID=$doc_id ";
			}
		
			$query = $this->db2->query($sql);
			$result = $query->result();
			if(count($result)==0)
			{
			$chargers=0;
			}
			else{
			  $chargers=$result[0]->consultant_fee;
			}
			return $chargers;
		}
		
		
    }
	
	public function getTiming() {
		 if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
	{
		echo  header('location:'.base_url().'index.php/Login/logout');	
	}
	else
	{
			$login_type = $this->session->userdata('admin_type');
			if ($login_type == 'admin') {
			 	$sql = "select * from  tbltimeschedule";
			}
		
			$query = $this->db2->query($sql);
			$result = $query->result();
			return $result;
		}
    }

      public function insertTiming($from_time, $to_time,$day,$doc,$clinic,$branch_id, $timing_id, $cat_button)
	{
	    $created_date=date("Y-m-d");
         if ($cat_button=='add_time') 
		 {
			 	
			   $sql = "insert into   tbltimeschedule (	 timingDay,doctorID,branchID,fromTime,toTime) values ('".$day."','".$doc."','".$branch_id."','".$from_time."','".$to_time."' )";
			   $result = $this->db2->query($sql); 
			   
				$time_id=$this->db2->insert_id();
        }
		else
		{
			 
			$sql = "update   tbltimeschedule set timingDay	='".$day."',doctorID='".$doc."',branchID='".$branch_id."' ,fromTime='".$from_time."' ,toTime='".$to_time."'  where timingID=".$timing_id;
     		 $result = $this->db2->query($sql); 
		}
		return $time_id;
		
    }   
	public function insertMasterTiming($from_timedata, $to_timeData,$day,$doc,$clinic,$branch_id, $timing_id, $cat_button)
	{
	    $created_date=date("Y-m-d");
         if ($cat_button=='add_time') 
		 {
			 	for($k=0;$k<count($from_timedata);$k++)
				{
					$from_time=$from_timedata[$k];
					$to_time=$to_timeData[$k];
				   $sql ="insert into   tbltimeschedule (timingDay,doctorID,branchID,fromTime,toTime) values ('".$day."','".$doc."','".$branch_id."','".$from_time."','".$to_time."')";
				   $result = $this->db2->query($sql); 
				   $time_id=$this->db2->insert_id();
				}
				
        }
		else
		{
 
			 for($k=0;$k<count($from_timedata);$k++)
				{
					if($direct_invoice_product_id[$i]=='')
						{	
							for($k=0;$k<count($from_timedata);$k++)
							{
								$from_time=$from_timedata[$k];
								$to_time=$to_timeData[$k];
								$sql ="insert into   tbltimeschedule (timingDay,doctorID,branchID,fromTime,toTime) values ('".$day."','".$doc."','".$branch_id."','".$from_time."','".$to_time."')";
							   $result = $this->db2->query($sql); 
							   $time_id=$this->db2->insert_id();
							}
						}
					else{
						$from_time=$from_timedata[$k];
						$to_time=$to_timeData[$k];
						  $timing=$timing_id[$k];
						  $sql = "update   tbltimeschedule set timingDay	='".$day."',doctorID='".$doc."',branchID='".$branch_id."' ,fromTime='".$from_time."' ,toTime='".$to_time."'   where timingID=".$timing;
							
						$result = $this->db2->query($sql); 
					}
				}
		}
		  redirect(base_url() . 'index.php/timingDetails');
		
    }

    public function editTiming($id,$day) {
       $query = $this->db2->query("SELECT * FROM  tbltimeschedule WHERE doctorID = $id and timingDay='$day'");
        return $query->result_array();
    }
  
   	 public function deletetiming($id) {
			$query = $this->db2->query("delete FROM  tbltimeschedule  WHERE timingID = '$id' ");
      
    }
		
		
	  	public function getDaysRecordds($doc_id,$branch_id,$day)
		{ 
	 
			$login_type = $this->session->userdata('admin_type');
				$admin_id = $this->session->userdata('admin_id'); 
				 if ($login_type == 'admin' || $login_type == 'user') {
					
					    $sql = " select * from tbltimeschedule t where  t.doctorID='$doc_id' and t.branchID='$branch_id' and t.timingDay='$day'";
				   
				}
				else if ($login_type == 'doctor') {
				  $sql = "select * from tbltimeschedule t where  t.doctorID='$admin_id'  and t.branchID='$branch_id' and t.timingDay='$day'";
				}
				$query = $this->db2->query($sql);
				$result = $query->result();
				return $result; 
			} 
	 
	
	
	public function insertInsurance($insurance_name, $insurance_provider,$type_id,$speciality,$service_charges,$remarks, $insurance_id, $cat_button)
	{
	    $created_date=date("Y-m-d");
         if ($cat_button=='add_btn') 
		 {
			 	 
					$type=implode(",",$type_id);
					$spec=implode(",",$speciality);
					$charges=implode(",",$service_charges);
					$sql = "insert into   tblInsuranceMaster (insuranceName,insuranceProvider,typeID,serviceID,serviceCharges,remark) values ('".$insurance_name."','".$insurance_provider."','".$type."','".$spec."','".$charges."','".$remarks."' )";
					$result = $this->db2->query($sql); 
				   $time_id=$this->db2->insert_id();
			 
        }
		else
		{
				$type=implode(",",$type_id);
					$spec=implode(",",$speciality);
					$charges=implode(",",$service_charges);
					$sql ="update   tblInsuranceMaster set insuranceName	='".$insurance_name."',insuranceProvider='".$insurance_provider."',typeID='".$type."' ,serviceID='".$spec."' ,serviceCharges='".$charges."' ,remark='".$remarks."'   where insuranceID=".$insurance_id;		
						$result = $this->db2->query($sql); 
			 
		}
		  redirect(base_url() . 'index.php/insuranceDetails');
		
    }
	
	 public function getInsuranceList() {
  $sql = "select m.* from tblInsuranceMaster m";
	  
	   
		 $query =  $this->db2->query($sql);
			$result = $query->result();
			return $result;
	 
    }
	   public function editInsurance($id) {
       $query = $this->db2->query("SELECT * FROM  tblInsuranceMaster WHERE insuranceID = $id");
        return $query->result_array();
    }
  
		
	
}

?>				