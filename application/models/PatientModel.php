<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PatientModel extends CI_Model {

 function __construct() {
        // Call the Model constructor
        parent::__construct();
    
		 $this->db2=$this->load->database('dynamicdb', TRUE);
        $this->load->library('session');
		
			$this->load->library('email');
    }

    public function getPatients() 
	{
		/* if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
				$login_type = $this->session->userdata('admin_type');
				if ($login_type == 'admin')
				{ */
					$session_login_id = $_SESSION['admin_id'];
					$sql = 'select * from  tblPatientRegistration';
					$query = $this->db2->query($sql);
					$result = $query->result();
					return $result;
		//	}
		//}
	} 
	public function getPatientsLastNo() 
	{
	  
					$sql = 'select  top 1 * from  tblPatientRegistration order by patientID desc ';
					$query = $this->db2->query($sql);
					$result = $query->result_array();
					return $result;
		 
	} 
	public function getPatientsTodays() 
	{
	  $session_login_id = $_SESSION['admin_id'];
	  $cdate = date('Y-m-d');
		$sql = "select * from  tblPatientRegistration where last_appointment_date='".$cdate."'";
					$query = $this->db2->query($sql);
					$result = $query->result();
					return $result;
			 
	}
	public function getUsers() 
	{
	 		//$login_type = $this->session->userdata('admin_type');
				
				//	$session_login_id = $_SESSION['admin_id'];
					$sql = 'select * from  tblUserMaster d,tblRoleMaster sm where d.roleID=sm.roleID' ;
					$query = $this->db2->query($sql);
					$result = $query->result();
					return $result;
				}

    public function insertPatientRegistration($prefix,$patient_reg_no,$fullname, $dob,$age,$email,$address,$contact_no,$accupation,$gender,  $patient_id, $cat_button,	$state, $city,$zipcode,$remarks,$relation,$relative_name,$relative_contact,  $patient_type, $ref_no,$photo_file,$tbl_appointment_id)
	 {
		if ($cat_button=='add')
		 {
			 	$created_date=date('Y-m-d'); 
				
				 
			  	$psql = 'select top 1 * from  tblPatientRegistration order by patientID desc';
					$query2 = $this->db2->query($psql);
					$result = $query2->result(); 
					if($query2->num_rows()==0)
					{
						$preqsql = "select  * from  tblPrefixMaster ";
						$prequery =  $this->db2->query($preqsql);
						$preres = $prequery->result(); 
						 $prefix=$preres[0]->startFrom;
						 $mrd_no=$prefix+1;
					}
					else{ 
						$mrd_no=$result[0]->patientRegNO+1;
					}
					// $mrd_no = str_pad($patientRegNO, 5, '0', STR_PAD_LEFT);
					 
					$sql = "insert into tblPatientRegistration (fullName, gender,address, dob, age, occupation, emailID, contactNO, patientRegNO,   createDate,lastAppoDate,remark,stateID,cityID,zipcode,photoFile,relWithPatient,relativeName,relativeContact,patientType,refNO) values ('".$fullname."','".$gender."','".$address."','".$dob."','".$age."','".$accupation."','".$email."', '".$contact_no."', '".$mrd_no."',  '".$created_date."','".$created_date."','".$remarks."','".$state."','".$city."','".$zipcode."','".$photo_file."','".$relation."','".$relative_name."','".$relative_contact."','".$patient_type."','".$ref_no."')";
			  
			  	$result = $this->db2->query($sql);
				$patient_id=$this->db2->insert_id();

				$created_date=date("Y-m-d");
				 
				$_SESSION['success']='Patient Registration Submitted Successfully'; 
				if($tbl_appointment_id!='')
				{
					$sqls = "update tblAppointment set appoStatusFlag=1,mrdNO='$mrd_no',patientID='$patient_id',patientType='Existing'  where appoID=".$tbl_appointment_id;						$result = $this->db2->query($sqls); 
				}
      	
				
				
				
				  
		}
		else
		{
				 $this->db2->from('tblPatientRegistration');	
				$this->db2->where('patientID',$patient_id);
				$query = $this->db2->get();
				$row=$query->row();
				$sql = "update tblPatientRegistration set fullName='".$fullname."', address='".$address."', gender='".$gender."',dob='".$dob."', age='".$age."' , occupation='".$accupation."' , contactNO='".$contact_no."' , emailID='".$email."'  where patientID=".$patient_id;
				$result = $this->db2->query($sql);
				
				
			
				$_SESSION['success']='Patient Details Updated Successfully';
			
				
		} 

       redirect(base_url() . 'index.php/patientDetails');
    }
public function insertuserRegistration($fullname,  $email,$qua,$contact_no,$gender,$branch_id,$description ,$doc_file, $doc_id, $cat_button,$user_address,$role_id,$photo_file,$tbl_user_id)
	 {
	    
	    	
		if ($cat_button=='add_user')
		 {
			$branch=implode(",",$branch_id);
			 	$created_date=date('Y-m-d'); 
			 	$login_flag='user'; 
				 $sql = "insert into tblUserMaster ( userName ,  userGender ,  userContact ,  userEmail ,  photoFile ,  userQualification ,  userDesc ,  branchID ,  userAddress ,  userDocument ,  regDate ,  roleID    ) values ('".$fullname."','".$gender."', '".$contact_no."',  '".$email."', '".$photo_file."', '".$qua."', '".$description."', '".$branch."',  '".$user_address."', '".$doc_file."', '".$created_date."', '".$role_id."')";
			  
			  	$result = $this->db2->query($sql);
				$user_id=$this->db2->insert_id();

				$created_date=date("Y-m-d");
				 
				$_SESSION['success']='User Registration Submitted Successfully';
				  
				//mail detauls
				
					$data = array();
				
				$data['reset_key'] = base64_encode('user|'.$user_id.'|'.$clinic_id.'|'.(time() + (2 * 24 * 60 * 60)));
				
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from('admin@ratnapriyaclap.com', 'Clinic Management');
					$this->email->to($email); 
					$this->email->subject('Register successfully');
				
					$message="register";
					$message=$this->load->view('map_mail_format',$data,TRUE);
					$this->email->message($message);
					$this->email->send(); 
					//databse
					
					
			
		}
		else
		{
					 
				$this->db2->from('tblUserMaster');	
				$this->db2->where('userID',$tbl_user_id);
				$query = $this->db2->get();
				$row=$query->row();
					$branch=implode(",",$branch_id);
					$sql = "update tblUserMaster set userName='".$fullname."', photoFile='".$photo_file."', userGender='".$gender."',  userQualification='".$qua."' , userContact='".$contact_no."' , userEmail='".$email."', userDesc='".$description."' , roleID='".$role_id."', branchID='".$branch."', userDocument='".$doc_file."', userAddress='".$user_address."' where userID=".$tbl_user_id;
				$result = $this->db2->query($sql);
				$_SESSION['success']='User Details Updated Successfully';
			
				
		} 

       redirect(base_url() . 'index.php/userDetails');
    }
    
    	 public function editUser($id) {
		   $query = $this->db2->query("select * from  tblUserMaster where  userID = $id ");
			return $query->result_array();
		}
	 public function viewUser($id) {
		   $query = $this->db2->query("select * from  tblDoctorMaster where  userID = $id ");
			return $query->result_array();
		}
	 public function deleteUser($id) {
			$query = $this->db2->query("delete FROM tblUserMaster  WHERE userID = '$id' ");
			redirect(base_url() . 'index.php/userDetails');
    }
   
    public function chk_exist_vendor($vid,$mobile) {
		if($vid=='')
		{
				$query = $this->db2->query("SELECT * FROM vendor_master WHERE vendor_contact_no = '$mobile'");
			//	echo "SELECT * FROM vendor_master WHERE vendor_contact_no = '$mobile'";
		}
		else{
				$query = $this->db2->query("SELECT * FROM vendor_master WHERE vendor_contact_no = '$mobile' and  vendor_id = $id ");			
		}
		//echo $query->num_rows();
		 if($query->num_rows()>=1)
		 {
				$str="1";
		 }
		 else{
				$str="0";
		 }
		 return $str;
    }

 

	 public function getExistingPatientData($contact) {
		 
			$query = $this->db2->query("select * from  tblPatientRegistration where  contactNO = $contact ");
			return $query->result();
				 
		}
	 public function getPatientData($patient_id) {
		 
			$this->db2->from('tblPatientRegistration');	
				$this->db2->where('patientID',$patient_id);
				$query = $this->db2->get();
				$rows=$query->row(); 
				if(count($rows)==0)
				{
					echo "0";
				}
				else{
					echo $str=$rows->fullName."#".$rows->contactNO."#".$rows->patientRegNO."#".$rows->patientID;
				}
				 
		}	
		public function getPatientDataMRD($mrd_no) {
		 
			$this->db2->from('tblPatientRegistration');	
				$this->db2->where('patientRegNO',$mrd_no);
				$query = $this->db2->get();
				$rows=$query->row(); 
				

				if(count($rows)==0)
				{
					echo  $str="0";
				}
				else{
					echo $str=$rows->fullName."#".$rows->contactNO."#".$rows->emailID."#".$rows->dob."#".$rows->age."#".$rows->patientID."#".$rows->gender;
				}
				 return $str;
		}	
		public function PatientDataMRD($mrd_no,$contact_no) {
		 if($mrd_no!='')
		 {
			$query = $this->db2->query("select p.*,(p.patientType)ptype,d.*,a.* from tblPatientRegistration p,tblAppointment a,tblDoctorMaster d where p.patientRegNO = '$mrd_no' and p.patientID=a.patientID and a.doctorID=d.doctorID ");
		 }
		 else{
		   	$query = $this->db2->query("select p.*,(p.patientType)ptype,d.*,a.* from tblPatientRegistration p,tblAppointment a,tblDoctorMaster d where p.contactNO = '$contact_no' and p.patientID=a.patientID and a.doctorID=d.doctorID ");
		 }
			$rows=$query->row(); 
				if(count($rows)==0)
				{
					$res=0;
				}
				else{
					 
					$res=$query->result_array();
				}
				 return $res;
		}
		public function getMRDData($mrd_no,$contact_no) {
			if($mrd_no!='')
			{
			   $query = $this->db2->query("select p.* from tblPatientRegistration p where p.patientRegNO = '$mrd_no' ");
			}
			else{
				  $query = $this->db2->query("select p.* from tblPatientRegistration p where p.contactNO = '$contact_no' ");
			}
	 
			   $res=$query->result(); 
					return $res;
		   }
		 public function editPatient($id) {
		   $query = $this->db2->query("select * from  tblPatientRegistration where  patientID = $id ");
			return $query->result_array();
		}
	 public function viewPatient($id) {
		   $query = $this->db2->query("select * from  tblPatientRegistration where  patientID = $id ");
			return $query->result_array();
		}
	 public function deletePatient($id) {
			$query = $this->db2->query("delete FROM tblPatientRegistration  WHERE patientID = '$id' ");
			redirect(base_url() . 'index.php/patientDetails');
    }
	//Fees Doctor
	public function saveFeesInsert($doctor_id,  $doc_branch_id,$new_visit,$no_of_count,$recurring_visit,$clinic,$fees_id,$cat_button,$fee_type,$immediate_fee)
	{ 
		if ($cat_button=='btn_fees')
		 {
	  	 	  $created_date=date('Y-m-d');
			 	   $sql = 'insert into tbl_fees_master (	tbl_doc_id,tbl_branch_id,consultant_fee,after_days,recurring_fee,clinic_id,fee_type,immediate_fee) values ("'.$doctor_id.'","'.$doc_branch_id.'","'.$new_visit.'","'.$no_of_count.'", "'.$recurring_visit.'",   "'.$clinic.'",   "'.$fee_type.'",   "'.$immediate_fee.'" )';
			   
			  	$result = $this->db2->query($sql); 
					
			
		}
		else
		{
	 
	 
				$this->db2->from('tbl_fees_master');	
				$this->db2->where('fees_id',$fees_id);
				$query = $this->db2->get();
				$row=$query->row();
			 	$sql = 'update tbl_fees_master set tbl_doc_id="'.$doctor_id.'", tbl_branch_id="'.$doc_branch_id.'",  consultant_fee="'.$new_visit.'", after_days="'.$no_of_count.'", recurring_fee="'.$recurring_visit.'" where fees_id='.$fees_id;
      	
				$result = $this->db2->query($sql);
				$_SESSION['success']='Fees Details Updated Successfully';  
		 } 
 
    }
    
		 public function getbranchesByDocId($id) {
	 $query = $this->db2->query("select * from  tblDoctorMaster where  doctorID = $id ");
			return $query->result_array();
		}	
 		 public function getbranchesById($id) {
	 $query = $this->db2->query("select * from  tblBranchMaster where  branchID = $id ");
			return $query->result_array();
		}
    
    //doctors
		public function getDoctors() 
	{
		if(!isset($_SESSION["admin_type"]) && !isset($_SESSION["admin_id"]))
		{
			echo  header('location:'.base_url().'index.php/Login/logout');	
		}
		else
		{
				$login_type = $this->session->userdata('admin_type');
				if ($login_type == 'admin')
				{
					$session_login_id = $_SESSION['admin_id'];
					$sql = 'select * from  tblDoctorMaster' ;
					$query = $this->db2->query($sql);
					$result = $query->result();
					return $result;
			}
		}
	}

	public function insertDoctorRegistration($fullname,  $email,$qua,$contact_no,$affilation,$gender,$branch_id,$description ,$doc_file, $doc_id, $cat_button,$speciality,$role_id,$doctor_address,$photo_file,$mci_id,$mci_no,$patient_time)
	 {
	    if ($this->input->post('add_doctor'))
		 {
				$sp=implode(",",$speciality);
				$branch=implode(",",$branch_id); 
			 	$created_date=date('Y-m-d');
			 	//$clinic_id=$_SESSION['clinic_id'];
				  $sql = "insert into tblDoctorMaster (doctorMciID,doctorMciNo,doctorName,	doctorGender,	doctorContact,	doctorEmail,	doctorAffilation,	doctorQualification,	doctorDesc,	branchID,	doctorDocuments,	dorcorRegDate,doctorSpeciality,doctorPatientTime,roleID,doctorAddress) values ('".$mci_id."','".$mci_no."','".$fullname."','".$gender."', '".$contact_no."',  '".$email."', '".$affilation."', '".$qua."', '".$description."', '".$branch."', '".$doc_file."', '".$created_date."', '".$sp."', '".$patient_time."', '".$role_id."', '".$doctor_address."')";	$doc_id=$this->db2->insert_id();
				  $result = $this->db2->query($sql);
				$created_date=date("Y-m-d");
				 
				$_SESSION['success']='Doctor Registration Submitted Successfully';
					 
				/* 	$data = array();
				 
					$data['reset_key'] = base64_encode('doctor|'.$doc_id.'|'.$clinic_id.'|'.(time() + (2 * 24 * 60 * 60)));
				
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from('admin@ratnapriyaclap.com', 'Clinic Management');
					$this->email->to($email); 
					$this->email->subject('Register successfully');
				
					$message="register";
					$message=$this->load->view('map_mail_format',$data,TRUE);
					$this->email->message($message);
					$this->email->send(); */ 
					
					redirect(base_url() . 'index.php/doctorDetails');
			
	//	redirect(base_url() . 'index.php/doctorRegistered/'.$data['reset_key']);
		}
		else
		{
				$sp=implode(",",$speciality);
				$branch=implode(",",$branch_id); 
		
				$this->db2->from('tblDoctorMaster');	
				$this->db2->where('doctorID',$doc_id);
				$query = $this->db2->get();
				$row=$query->row();
				$sql = "update tblDoctorMaster set doctorMciID='".$mci_id."', doctorMciNo='".$mci_no."',  doctorName='".$fullname."', doctorAffilation='".$affilation."', doctorGender='".$gender."',  doctorQualification='".$qua."' , doctorContact='".$contact_no."' , doctorEmail='".$email."', doctorDesc='".$description."' , roleID='".$role_id."', branchID='".$branch."', doctorDocuments='".$doc_file."', doctorSpeciality='".$sp."',doctorPatientTime='".$patient_time."' ,doctorAddress='".$doctor_address."' where doctorID=".$doc_id;
				$result = $this->db2->query($sql);
				$_SESSION['success']='Doctor Details Updated Successfully';
			
				redirect(base_url() . 'index.php/doctorDetails');
		} 

       
    }
    
    	 public function editDoctor($id) {
	$query = $this->db2->query("select * from  tblDoctorMaster where  doctorID = $id ");
			return $query->result_array();
		} 
	 public function viewDoctor($id) {
		   $query = $this->db2->query("select * from  tblDoctorMaster where  doctorID = $id ");
			return $query->result_array();
		}
	 public function deleteDoctor($id) {
			$query = $this->db2->query("delete FROM tblDoctorMaster  WHERE doctorID = '$id' ");
			redirect(base_url() . 'index.php/doctorDetails');
    }
	
	
	//tim ing
	
	
	
		function filename_exists($rest_email,$btnval,$pageval)
			{
			if($pageval=='user')
			{
				if($btnval=='email')
				{
					$this->db2->select('*'); 
					$this->db2->from('tblUserMaster');
					$this->db2->where('email_id', $rest_email);
				}
				else{
					$this->db2->select('*'); 
					$this->db2->from('tblUserMaster');
					$this->db2->where('contact_no', $rest_email);
				}
			}
			else 	if($pageval=='doctor'){
			
			if($btnval=='email')
				{
					$this->db2->select('*'); 
					$this->db2->from('tblDoctorMaster');
					$this->db2->where('email_id', $rest_email);
				}
				else{
					$this->db2->select('*'); 
					$this->db2->from('tblDoctorMaster');
					$this->db2->where('contact_no', $rest_email);
				}
			}
			$query = $this->db2->get();
			$result = $query->result_array();
			return $result;
		}
		
}

?>				