<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH."controllers/Secure_area.php";

	class Patient extends  Secure_area 
		{
		 public $dynamicDB;
		function __construct() {
			// Call the Model constructor
			parent::__construct();
		 
       	 $this->db2=$this->load->database('dynamicdb', TRUE);
			$this->load->model('PatientModel');
			$this->load->model('MasterModel'); 
			$this->load->model('AppointmentModel'); 
		//	$this->load->helper('url');
			//$this->load->library('parser');
		}
		
		public function index() {
	
		}
		function filename_exists()
		{
			$fieldval = $this->input->post('fieldval');
			$btnval = $this->input->post('btnval');
			$pageval = $this->input->post('pageval');
			$exists = $this->PatientModel->filename_exists($fieldval,$btnval,$pageval);			
			$count = count($exists);
			echo $count; 
		}
		public function newRegistration($appointment_id) {
		 
			 $app_List = $this->AppointmentModel->getappointmentsPatients($appointment_id);
			$data['appointmentData'] = $app_List; 
			 $state_List = $this->MasterModel->getstateList();
			$data['stateList'] = $state_List; 
			$cityList = $this->MasterModel->getcityList();
			$data['cityList'] = $cityList;
			$this->load->view('patient/app_patientRegistration',$data);
		}
		
		public function patientRegistration() {
			 
			$predata = $this->MasterModel->getPrefixByType();
			$data['prefix'] = $predata; 	
			 $state_List = $this->MasterModel->getstateList();
			$data['stateList'] = $state_List;
			$cityList = $this->MasterModel->getcityList();
			$data['cityList'] = $cityList;
			$padata = $this->PatientModel->getPatientsLastNo();
			$data['patientDataNumber'] = $padata; 
			$this->load->view('patient/patientRegistration',$data);
		}
		
			public function patientDetails() {
			$type='patient';
			$predata = $this->MasterModel->getPrefixByType($type);
			$data['prefix'] = $predata; 	
			$listdata = $this->PatientModel->getPatients();
			$data['PatientData'] = $listdata;
 
			$this->load->view('patient/patientDetails',$data);
		}
		
			
		
		public function savepatientRegistration() {
			$prefix = $this->input->post("prefix");
			$patient_reg_no = $this->input->post("patient_reg_no");
			$fullname = $this->input->post("fullname");
			$dob = date("Y-m-d",strtotime($this->input->post('dob')));
			$age = $this->input->post('age');
			$email = $this->input->post('email');
		 	$address = $this->input->post('address');
			$contact_no = $this->input->post('contact_no');
			$accupation = $this->input->post('accupation');
			$gender = $this->input->post('gender');
			$patient_reg_no = $this->input->post('patient_reg_no');
			
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$zipcode = $this->input->post('zipcode');
			$remarks = $this->input->post('remarks');
			
			$relation = $this->input->post('relation');
			$relative_name = $this->input->post('relative_name');
			$relative_contact = $this->input->post('relative_contact'); 
			
			$patient_type = $this->input->post('patient_type'); 
			$ref_no = $this->input->post('ref_no');
			$tbl_appointment_id = $this->input->post('tbl_appointment_id');
	
		
		if(!empty($_FILES['photo_file']['name']))
			{
				$ext = pathinfo($_FILES['photo_file']['name'], PATHINFO_EXTENSION);
				
				$profileconfig['upload_path'] = './patient_docs/';
				$profileconfig['allowed_types'] = 'gif|jpg|png|jpeg';
				$profileconfig['max_size'] = '2000000';
				$profileconfig['remove_spaces'] = false;
				$profileconfig['overwrite'] = false;
				$profileconfig['max_width'] = '';
				$profileconfig['max_height'] = '';
				$new_name  =rand(10,10000);
				$profileconfig['file_name'] = $new_name;
				$this->upload->initialize($profileconfig);
				if (!$this->upload->do_upload('photo_file')) {
					$error = array('error' => $this->upload->display_errors());
					$error['error'];
				}
				
				$photo_file =$new_name.".".$ext;   
			}
		
		
			$patient_id = $this->input->post('patient_id'); 
			$cat_button='';
			if ($this->input->post('add')) {
				  $cat_button = 'add';
				} else if ($this->input->post('update')) {
				$cat_button = 'update';
			}
			$res = $this->PatientModel->insertPatientRegistration($prefix,$patient_reg_no,$fullname, $dob,$age,$email,$address,$contact_no,$accupation,$gender,  $patient_id, $cat_button,	$state, $city,$zipcode,$remarks,$relation,$relative_name,$relative_contact,  $patient_type, $ref_no,$photo_file,$tbl_appointment_id);
		}
		
		public function editPatient($id) {
			
			$data['edit_data'] = $this->PatientModel->editPatient($id);
			 $state_List = $this->MasterModel->getstateList();
			$data['stateList'] = $state_List; 
			$city_List = $this->MasterModel->getcityList();
			$data['cityList'] = $city_List; 
			$this->load->view('patient/editpatient', $data, FALSE);
		}
		public function getPatientdata() {
			
			  $patient_id = $this->input->post("patient_id");
		 	echo $this->PatientModel->getPatientData($patient_id); 
			
		}
		public function getPatientDataMRD() {
			
			   $mrdNo = $this->input->post("mrdNo");
		 	echo $this->PatientModel->getPatientDataMRD($mrdNo); 
			
		}	
		public function PatientDataMRD() {
			
			   $mrdNo = $this->input->post("mrdNo");  
			   $contact_no = $this->input->post("contact_no"); 
		 	echo json_encode($this->PatientModel->PatientDataMRD($mrdNo,$contact_no));
			
		}
		
	 
		
		public function getExistingPatientdata() {
			
			  $contact = $this->input->post("contact");
		 	  $expredata=$this->PatientModel->getExistingPatientData($contact); 
			  	$data['existingpatientData'] = $expredata;
			$this->load->view('patient/existingpatient', $data, FALSE);
		}
		
	 
		public function deletePatient($id) {
			$t=$this->PatientModel->deletePatient($id);
			
		}
			public function viewPatient($id) {
			$data['viewClinic'] = $this->PatientModel->viewPatient($id);
			 		
			$this->load->view('patient/viewPatient', $data, FALSE);
		}	
		
		public function doctorDetails() {
			$docdata = $this->PatientModel->getDoctors();
			$data['doctorData'] = $docdata;	
			$spec_list = $this->MasterModel->getSpecialityList();
			$data['specalist'] = $spec_list;
			
			$this->load->view('doctor/doctorDetails',$data);
		}
		
			public function doctorRegistration() {
		    $state_List = $this->MasterModel->getstateList();
			$data['stateList'] = $state_List;
			  $masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist;
			
			$rolemasterlist = $this->MasterModel->getroleList();
			$data['role_list'] = $rolemasterlist;
			 $doc_list = $this->MasterModel->getSpecialityList();
			$data['spec_data'] = $doc_list;
			
			$this->load->view('doctor/doctorRegistration',$data);
		}
			public function doctorRegistered ($key=false) {
			 // $url_param = rtrim($key, '=');
			//$base_64a = $url_param . str_repeat('=', strlen($url_param) % 4); 
			  $datas  = base64_decode($key);
			$data2 = explode("|",$datas);
			  $data['check_flag']=$data2[0];
			  $data['doc_id']=$data2[1];
			  $data['clinic_id']=$data2[2];  
			
			  $masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist;
		    $state_List = $this->MasterModel->getstateList();
			$data['stateList'] = $state_List;
			  $doc_data = $this->PatientModel->editDoctor($data2[1]);
				$data['docData'] = $doc_data; 
					$branch_id=explode(",",$data['docData'][0]['branch_id']);
				 $categories = array();
					foreach ( $branch_id as $post ){  
					 
					$categories[] = $this->PatientModel->getbranchesById($post);
					 $data['branchData'] = $categories;
				}
			$rolemasterlist = $this->MasterModel->getroleList();
			$data['role_list'] = $rolemasterlist;
			 $doc_list = $this->MasterModel->getSpecialityList();
			$data['spec_data'] = $doc_list; 
			$this->load->view('doctor/doctorRegistration',$data);
		}
		public function saveDoctorRegistration() {
			$fullname = $this->input->post("fullname");
			
			$email = $this->input->post('email');
		 	$qua = $this->input->post('qua');
			$contact_no = $this->input->post('contact_no');
			$affilation = $this->input->post('affilation');
			$gender = $this->input->post('gender'); 
			$branch_id = $this->input->post('branch_id'); 
					$description = $this->input->post('description'); 
			$speciality = $this->input->post('speciality'); 
			$mci_id = $this->input->post('mci_id'); 
			$mci_no = $this->input->post('mci_no'); 
			$doc_id = $this->input->post('doc_id'); 
			$patient_time = $this->input->post('patient_time'); 
			
			$role_id = $this->input->post('role_id');
			$photo_file = $this->input->post('photo_file');
			$doctor_address = $this->input->post('doctor_address');
			$cat_button='';
			if ($this->input->post('add_doctor')) {
				  $cat_button = 'add_doctor';
				} else if ($this->input->post('update_doc')) {
				$cat_button = 'update_doc';
			}
			if(!empty($_FILES['photo_file']['name']))
			{
				$ext = pathinfo($_FILES['photo_file']['name'], PATHINFO_EXTENSION);
				
				$profileconfig['upload_path'] = './doc_docs/';
				$profileconfig['allowed_types'] = 'gif|jpg|png|jpeg';
				$profileconfig['max_size'] = '2000000';
				$profileconfig['remove_spaces'] = false;
				$profileconfig['overwrite'] = false;
				$profileconfig['max_width'] = '';
				$profileconfig['max_height'] = '';
				$new_name  =rand(10,10000);
				$profileconfig['file_name'] = $new_name;
				$this->upload->initialize($profileconfig);
				if (!$this->upload->do_upload('photo_file')) {
					$error = array('error' => $this->upload->display_errors());
					$error['error'];
				}
				
				$photo_file =$new_name.".".$ext;   
			}
			
			if(!empty($_FILES['doc_id_proof']['name']))
			{
				$ext = pathinfo($_FILES['clinic_id_proof']['name'], PATHINFO_EXTENSION);
				
				$profileconfig['upload_path'] = './doc_docs/';
				$profileconfig['allowed_types'] = 'gif|jpg|png|jpeg';
				$profileconfig['max_size'] = '2000000';
				$profileconfig['remove_spaces'] = false;
				$profileconfig['overwrite'] = false;
				$profileconfig['max_width'] = '';
				$profileconfig['max_height'] = '';
				$new_name  =rand(10,10000);
				$profileconfig['file_name'] = $new_name;
				$this->upload->initialize($profileconfig);
				if (!$this->upload->do_upload('clinic_id_proof')) {
					$error = array('error' => $this->upload->display_errors());
					$error['error'];
				}
				
				$doc_file =$new_name.".".$ext;   
				}
			$res = $this->PatientModel->insertDoctorRegistration($fullname,  $email,$qua,$contact_no,$affilation,$gender,$branch_id,$description ,$doc_file, $doc_id, $cat_button,$speciality,$role_id,$doctor_address,$photo_file,$mci_id,$mci_no,$patient_time);
		}
		
		
		 
			public function editDoctor($id) {
			
			$data['edit_data'] = $this->PatientModel->editDoctor($id);
			$masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist;
		    $state_List = $this->MasterModel->getstateList();
			$data['stateList'] = $state_List; 
					$branch_id=explode(",",$data['edit_data'][0]['branchID']);
				 $categories = array();
					foreach ( $branch_id as $post ){  
					 
					$categories[] = $this->PatientModel->getbranchesById($post);
					 $data['branchData'] = $categories;
				}
			$rolemasterlist = $this->MasterModel->getroleList();
			$data['role_list'] = $rolemasterlist;
			 $doc_list = $this->MasterModel->getSpecialityList();
			$data['spec_data'] = $doc_list; 
			$this->load->view('doctor/editDoctor', $data, FALSE);
		}
		
	 
		public function deleteDoctor($id) {
			$t=$this->PatientModel->deleteDoctor($id);
			 
		}
			public function viewDoctor($id) {
			$data['viewClinic'] = $this->PatientModel->viewDoctor($id);
			 		
			$this->load->view('doctor/viewDoctor', $data, FALSE);
		}	
//doctor fees
		public function saveFeeInsert() {
		 	 $doctor_id = $this->input->post("doc"); 
		 	 $doc_branch_id = $this->input->post('branch_id');
		  	 $new_visit = $this->input->post('new_visit');
		 	 $no_of_count = $this->input->post('no_of_days');
		 	 $recurring_visit = $this->input->post('recurring_visit');
			  $clinic = $this->input->post('clinic'); 
			  $immediate_fee = $this->input->post('immediate_fee'); 
			  $fee_type = $this->input->post('fee_type'); 
			 
			 
			   $fees_id = $this->input->post('fees_id'); 
			$cat_button='';
			if ($fees_id=='') {
				  $cat_button = 'btn_fees';
				} else if ($this->input->post('update_fees')) {
				$cat_button = 'update_fees';
			}  
		 
		    	$res = $this->PatientModel->saveFeesInsert($doctor_id,  $doc_branch_id,$new_visit,$no_of_count,$recurring_visit,$clinic,$fees_id,$cat_button,$fee_type,$immediate_fee);
		}
		
		
		public function getbranchesByDocId() {
		
			$doctor_id = $this->input->post("doc_id");
		    $branchdatalist = $this->MasterModel->getbranchesByDocId($doctor_id);
			$data['branchdata'] = $branchdatalist;  
			
			
			$this->load->view('doctor/userRegistration',$data);
		}
		
		
				//user
		public function userRegistration() {
		     $masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist; 
			
			$rolemasterlist = $this->MasterModel->getroleList();
			$data['role_list'] = $rolemasterlist;
				$doc_list = $this->MasterModel->getSpecialityList();
						$data['spec_data'] = $doc_list; 
			$this->load->view('user/userRegistration',$data);
		}
		
			public function userDetails() {
			$docdata = $this->PatientModel->getUsers();
			$data['userData'] = $docdata;	
		
			$this->load->view('user/userDetails',$data);
		}
		
		
		public function saveUserRegistration() {
			$fullname = $this->input->post("fullname");
			
			$email = $this->input->post('email');
		 	$qua = $this->input->post('qua');
			$contact_no = $this->input->post('contact_no');
			$affilation = $this->input->post('affilation');
			$gender = $this->input->post('gender'); 
			$branch_id = $this->input->post('branch_id'); 
					$description = $this->input->post('description'); 
			$speciality = $this->input->post('speciality'); 
			$doc_id = $this->input->post('doc_id'); 
			$role_id = $this->input->post('role_id');
			$photo_file = $this->input->post('photo_file');
			$user_address = $this->input->post('user_address');
			$tbl_user_id = $this->input->post('tbl_user_id');
			
			
			$cat_button='';
			if ($this->input->post('add_user')) {
				  $cat_button = 'add_user';
				} else if ($this->input->post('update_user')) {
				$cat_button = 'update_user';
			}
			
			if(!empty($_FILES['photo_file']['name']))
			{
				$ext = pathinfo($_FILES['photo_file']['name'], PATHINFO_EXTENSION);
				
				$profileconfig['upload_path'] = './doc_docs/';
				$profileconfig['allowed_types'] = 'gif|jpg|png|jpeg';
				$profileconfig['max_size'] = '2000000';
				$profileconfig['remove_spaces'] = false;
				$profileconfig['overwrite'] = false;
				$profileconfig['max_width'] = '';
				$profileconfig['max_height'] = '';
				$new_name  =rand(10,10000);
				$profileconfig['file_name'] = $new_name;
				$this->upload->initialize($profileconfig);
				if (!$this->upload->do_upload('photo_file')) {
					$error = array('error' => $this->upload->display_errors());
					$error['error'];
				}
				
				$photo_file =$new_name.".".$ext;   
			}
			
			if(!empty($_FILES['user_id_proof']['name']))
			{
				$ext = pathinfo($_FILES['user_id_proof']['name'], PATHINFO_EXTENSION);
				
				$profileconfig['upload_path'] = './doc_docs/';
				$profileconfig['allowed_types'] = 'gif|jpg|png|jpeg';
				$profileconfig['max_size'] = '2000000';
				$profileconfig['remove_spaces'] = false;
				$profileconfig['overwrite'] = false;
				$profileconfig['max_width'] = '';
				$profileconfig['max_height'] = '';
				$new_name  =rand(10,10000);
				$profileconfig['file_name'] = $new_name;
				$this->upload->initialize($profileconfig);
				if (!$this->upload->do_upload('user_id_proof')) {
					$error = array('error' => $this->upload->display_errors());
					$error['error'];
				}
				
				$doc_file =$new_name.".".$ext;   
			}
			$res = $this->PatientModel->insertuserRegistration($fullname,  $email,$qua,$contact_no ,$gender,$branch_id,$description ,$doc_file, $doc_id, $cat_button,$user_address,$role_id,$photo_file,$tbl_user_id);
		}
		
		
		
		
			public function editUser($id) {
			
			$data['edit_data'] = $this->PatientModel->editUser($id);
			 $masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist;
				$doc_list = $this->MasterModel->getSpecialityList();
						$data['spec_data'] = $doc_list;
						$rolemasterlist = $this->MasterModel->getroleList();
			$data['role_list'] = $rolemasterlist;
			$this->load->view('user/userRegistration', $data, FALSE);
		}
		
	 
		public function deleteUser($id) {
			$t=$this->PatientModel->deleteUser($id);
			 
		}
			public function viewUser($id) {
			$data['viewClinic'] = $this->PatientModel->viewUser($id);
			 		
			$this->load->view('user/viewUser', $data, FALSE);
		}	

		public function set_patient($id) {
		 
				$this->session->set_userdata('sess_patient_id', $id);
		}	
		
		 
		
	 
	
		
		
	}
	
?>
