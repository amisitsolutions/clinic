<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Welcome extends CI_Controller {
		function __construct() { 		
			parent::__construct();		
			$this->load->database();
			$this->db2=$this->load->database('dynamicdb', TRUE);
			$this->load->library('session');	
			$this->load->helper('form');		
			$this->load->library('form_validation'); 		
			$this->load->model('Login_check');		
			$this->load->model('MasterModel');		
			$this->load->model('PatientModel');		
			$this->load->helper('url');			
			//$this->load->library('parser');	
			//$this->load->library('encrypt');
		}		 
		public function appointmentLink($key=false)
		{
			$url_param = rtrim($key, '=');
			$base_64a = $url_param . str_repeat('=', strlen($url_param) % 4);
		  	$data = base64_decode($base_64a);
			$data2 = explode("|",$data);
			$restdata['check_flag']=$data2[0];
			$restdata['clinic']=$data2[1];
			$restdata['check_id']=$data2[2];
			$clinic=$data2[1]; 
			 $this->Login_check->checkDatabase($clinic);
			
			
		  	$this->db2=$this->load->database('dynamicdb', TRUE);
			$dbmasterlist = $this->MasterModel->getbranchesByClinicID($clinic);
				$restdata['branch_list'] = $dbmasterlist;  
			$sp_list = $this->MasterModel->getSpecialityList();
			$restdata['spec_data'] = $sp_list;  
			 
			$this->load->view('appointmentLink',$restdata);
			
		}
			 public function getDoctorsByspecialityBranches()
		{ 
		
			$branch_id = $this->input->post("branch_id");
			$sp_id = $this->input->post("sp_id");
			 $doc_data = $this->MasterModel->getDoctorsByspecialityBranches($branch_id,$sp_id);
				$restdata['docData'] = $doc_data; 
				 
			
			$this->load->view('getDoctorsData',$restdata);
		} 
		
		public function checkAppointmentAvailability()
		{ 
		
			$branch_id = $this->input->post("branch_id");
			$doctor_id = $this->input->post("doctor_id");
			$app_date = $this->input->post("app_date");
			 $app_time = $this->input->post("app_time");
			echo $res = $this->MasterModel->checkAvailability($branch_id,$doctor_id,$app_date,$app_time);
			
			 
		} 
		public function saveAppointmentLink()
		{ 
			$clinic_id = $this->input->post("clinic_id");
			$branch_id = $this->input->post("branch_id");
			$speciality = $this->input->post("speciality");
			$doctor_id = $this->input->post("doctor_id");
			$app_date = $this->input->post("app_date");
			$app_time = $this->input->post("app_time");
			$mrd_no = $this->input->post("mrd_no");
			$patient_type = $this->input->post("patient_type");
			$patient_id = $this->input->post("patient_id");
			$appointmentType = $this->input->post("appointmentType");
			$fullname = $this->input->post("fullname");
			$dob = date("Y-m-d",strtotime($this->input->post('dob')));
			$age = $this->input->post('age');
			$email = $this->input->post('email'); 
			$contact_no = $this->input->post('contact_no'); 
			$gender = $this->input->post('gender');
			$remarks = $this->input->post('remarks');
			 
				$res = $this->MasterModel->insertAppointments($appointmentType,$patient_id,$patient_type,$clinic_id,$mrd_no,$fullname, $dob,$age,$email,$contact_no,$gender,$branch_id,  $speciality, $doctor_id,	$app_date, $app_time, $remarks);
		 
	
		
		}
		
	}
