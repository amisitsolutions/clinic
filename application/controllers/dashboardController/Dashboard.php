<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH."controllers/Secure_area.php";

	class Dashboard   extends  Secure_area
		{
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			
		  $this->db2=$this->load->database('dynamicdb', TRUE);
			$this->load->model('DashboardModel');	 		
			 $this->load->model('MasterModel'); 
			
			$this->load->model('ProfileModel');
			$this->load->helper(array('form', 'url'));
			$this->load->library('parser');  
					
		}
		
		public function index() { 
		 	$this->load->view('dashboardView');
	 	 	
		}
		public function dashboard_data() {
		
		
			$this->load->view('dashboardView');
		} 
		
	}
	
?>
