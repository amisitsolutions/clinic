<?php
	
	class Secure_area extends CI_Controller 
	{
		var $module_id;
		var $data='';
		/*
			Controllers that are considered secure extend Secure_area, optionally a $module_id can
			be set to also check if a user can access a particular module in the system.
		*/
		function __construct()
		{
			parent::__construct();
			
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->helper('url');
			//$this->load->library('parser');
			//$this->load->library('encrypt');
			$this->load->model('ProfileModel');
			
			 
			$profileDataList = $this->ProfileModel->getProfile();
			$data['profileData'] = $profileDataList; 
		 	$this->load->vars($data);
		}
		
		
		
		
	}
?>