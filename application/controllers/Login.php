<?php
	
	class Login extends CI_Controller {
		
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->library('form_validation');
			//$this->load->model('LogCheckModel');
			$this->load->model('Login_check');
		//	$this->load->model('MasterModel');  
			//$this->load->model('PatientModel');  
			//$this->load->helper('url');
			//$this->load->library('parser');
			//$this->load->library('encrypt');
			
		}
		
		function index() { 
			$this->load->view('login');
		} 
		function selectBranch() { 
			  $log_id=$_SESSION['admin_id'];
			  $masterlist = $this->Login_check->getbranchesId($log_id);
			$data['Branchdata'] = $masterlist; 
			 
			$this->load->view('selectBranch',$data);
		} 
		 function branchLogin() { 
			
			$branch_id = $this->input->post("branch_id");	
			$_SESSION['sess_branch_id']=$branch_id;
			
			redirect('admin_dashboard');
		} 
		
		public	function checkLogin() {
			
		 	$reg_no = $this->input->post("reg_no");		
		 	$username = $this->input->post("username");			
		 	$password = $this->input->post("password"); 
		 	
			$n=$this->Login_check->chklogin($username, $password,$reg_no);
			
			if(trim($n)==true)
			{
				redirect('selectBranch');
				
			} 
			else{
				redirect('Login');
				
			} 
		}
		public	function submitLogin()
		{
			$this->load->view('dashboardView');
		}
		function duplicate_contact()
		{
			$custContact = $this->input->post("custContact");
			$cust_flag=$this->Login_check->duplicate_contact($custContact);
			echo $cust_flag;
			
		}
		
	 	function reset_password() {
			$this->load->view('reset_password_enter_password');
		}
		
		 
	 	function forgot_password() {
			$this->load->view('forgot_password');
		}
		
		 
	 
		
		public	function do_reset_password_notify()
		{	
			$curr_url = $this->input->post("curr_url");
			$new_reg_no = $this->input->post("new_reg_no");
			if($this->input->post('clinic_username'))
			{
				$custrow = $this->Login_check->get_details_username_or_email($this->input->post('clinic_username'),$new_reg_no);
			 
				if($custrow)
				{  
					$data = array();
					$data['rest'] = $custrow;
				 	$tbl_login_id=$custrow[0]['tbl_login_id'];
				 	$login_type=$custrow[0]['login_type'];
				 	$clinic_id=$custrow[0]['clinic_id'];
					$rest_list_array=array();
					$chkdata=array();
					$rest_list_array['chkdata'] = base64_encode($login_type.'|'.$tbl_login_id.'|'.$clinic_id.'|'.(time() + (2 * 24 * 60 * 60)));
					 
					$this->load->library('email');
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					$this->email->from('admin@ratnapriyaclap.com', 'Clinic Management');
					$this->email->to($custrow[0]['email_id']); 
					$this->email->subject('Forgot Password');
					
					$message=$this->load->view('forgot_map_mail_formatview',$rest_list_array,TRUE);
					$this->email->message($message);
					$this->email->send();
				 
					$succdata['success']='Reset Password Link has been sent.';
					//$this->load->view('reset_password',$succdata);
					redirect($curr_url);
				}
				else 
				{
					$errdata['error']='Username/Email ID does not exist';
					//$this->load->view('reset_password',$errdata);
				}
			}
			else
			{
				$errdata['error']= 'Field Can not be Empty';
				//$this->load->view('reset_password',$errdata);
			}
		}
		
		function reset_password_enter_password($key=false)
		{
			$url_param = rtrim($key, '=');
			$base_64a = $url_param . str_repeat('=', strlen($url_param) % 4);
			$data = base64_decode($base_64a);
			$data2 = explode("|",$data);
			$restdata['check_flag']=$data2[0];
			$restdata['clinic']=$data2[1];
			$restdata['check_id']=$data2[2];
			
			$this->load->view('reset_password_enter_password', $restdata, FALSE);			
			
		}
		function forgot_password_reset_password($key=false)
		{
			$base_64a = $key;// . str_repeat('=', strlen($url_param) % 4);
		 	$data = base64_decode($base_64a);
			$data2 = explode("|",$data);
 
			$restdata['log_type']=$data2[0];
			$restdata['log_user_id']=$data2[1];
			$restdata['clinic_id']=$data2[2];
			$userlist=$this->Login_check->get_clinic_info($data2[0],$data2[1],$data2[2]);
			$restdata['userData']=$userlist;
			$this->load->view('forgot_password', $restdata, FALSE);			
			
		}
		function do_reset_password()
		{
			
			$check_id = $this->input->post('check_id');
		 	$check_flag = $this->input->post('check_flag');
			$password = $this->input->post('rest_new_password');
			$clinic_id = $this->input->post('clinic_id');		
			$clinic_username = $this->input->post('clinic_username');		
			$confirm_password = $this->input->post('rest_confirm_password');
			$this->Login_check->save_reset_password($check_id,$clinic_username,$password,$check_flag,$clinic_id);
		}
		
		
		public function logout()
		{
			$this->session->unset_userdata('admin_id');
			$this->session->unset_userdata('admin_type');
			$this->session->unset_userdata('sess_clinic_id');
			$this->session->unset_userdata('clientdb');
			unset($_SESSION['clientdb']);
			$this->session->unset_userdata('session_onpage');
			$this->session->sess_destroy();
			redirect(base_url().'index.php'); 
		} 
		
		
	}
	
?>