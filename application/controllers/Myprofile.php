
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH."controllers/Secure_area.php";

	class Myprofile extends Secure_area // extends CI_Controller // 
	{
		
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('ProfileModel');
			
			$this->load->helper('url');
			//$this->load->library('parser');
			//$this->load->library('encrypt');
			
		}
		
			function index() {
				  	$login_id = $this->session->userdata('admin_id');
					$data['profile_data']  = $this->ProfileModel->getProfile($login_id);				
					$this->load->view('editProfile', $data, FALSE);
			}
		function clinicprofile() {
				 	$sess_clinic_id = $this->session->userdata('sess_clinic_id');
					$data['clinic_data']  = $this->ProfileModel->clinicprofile($sess_clinic_id);				
					$this->load->view('clinicProfile', $data, FALSE);
			}
		
		function save_update_profile() { 
				
			 $super_name = $this->input->post("super_name");
			$super_email_id = $this->input->post("super_email_id");
			$super_contact = $this->input->post("super_contact");
			$userName = $this->input->post("userName");
			$password = $this->input->post("password");
			$tbl_user_id = $this->input->post("tbl_user_id");
			 
			
			$res = $this->ProfileModel->save_update_profile($super_name,$super_email_id,$super_contact,$userName,$password,$tbl_user_id);
		
		}
		function save_clinic_profile() { 
				
			 $clinic_name = $this->input->post("clinic_name");
			$clinic_email = $this->input->post("clinic_email");
			$clinic_contact = $this->input->post("clinic_contact");
			$clinic_info = $this->input->post("clinic_info");
			$website = $this->input->post("website"); 
			$clinic_id = $this->input->post("clinic_id");
			 
			
			$res = $this->ProfileModel->save_clinic_profile($clinic_name,$clinic_email,$clinic_contact,$clinic_info,$website,$clinic_id);
		
		}
		
		
	}
	
?>	