<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH."controllers/Secure_area.php";
 
	class Appointment  extends  Secure_area  
	{
		
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			 $this->db2=$this->load->database('dynamicdb', TRUE);
			$this->load->model('AppointmentModel'); 
			$this->load->model('PatientModel'); 
			$this->load->model('MasterModel'); 
			$this->load->helper('url');
			$this->load->library('parser'); 	
		}
		
		public function index() {
			
		}
		public function appointmentLinkPanel( )
		{
			 
		 	$clinic = $this->session->userdata('sess_clinic_id');
			  
			$sp_list = $this->MasterModel->getSpecialityList();
			$restdata['spec_data'] = $sp_list;  
			
			$this->load->view('appointmentLinkPanel',$restdata);
			
		}
	 	public function saveAppointmentLink_panel()
		{ 
			$sess_branch_id =$this->session->userdata('sess_branch_id');
			$clinic_id = $this->input->post("clinic_id");
			$branch_id = $sess_branch_id;
			$speciality = $this->input->post("speciality");
			$doctor_id = $this->input->post("doctor_id");
			$app_date = $this->input->post("app_date");
			$app_time = $this->input->post("app_time");
			$mrd_no = $this->input->post("mrd_no");
			$patient_type = $this->input->post("patient_type");
			$patient_id = $this->input->post("patient_id");
			
			$fullname = $this->input->post("fullname");
			$dob = date("Y-m-d",strtotime($this->input->post('dob')));
			$age = $this->input->post('age');
			$email = $this->input->post('email'); 
			$contact_no = $this->input->post('contact_no'); 
			$gender = $this->input->post('gender');
			$remarks = $this->input->post('remarks');
			 
				$res = $this->AppointmentModel->insertAppointmentsFromPanel($patient_id,$patient_type,$clinic_id,$mrd_no,$fullname, $dob,$age,$email,$contact_no,$gender,$branch_id,  $speciality, $doctor_id,	$app_date, $app_time, $remarks);
		 
	
		
		}
		public function AppointmentAvailability()
		{ 
		
			$branch_id = $this->session->userdata('sess_branch_id');
			$doctor_id = $this->input->post("doctor_id");
			$app_date = $this->input->post("app_date");
			 $app_time = $this->input->post("app_time");
			echo $res = $this->MasterModel->checkAvailability($branch_id,$doctor_id,$app_date,$app_time);
			
			 
		}
		public function timingAvailability()
		{ 
		
			$branch_id =$this->session->userdata('sess_branch_id');
			$doctor_id = $this->input->post("doctor_id"); 
			$app_date = $this->input->post("app_date"); 
			  $doc_data = $this->AppointmentModel->checkDoctorAvailability($branch_id,$doctor_id,$app_date);
			$res['doctimeData'] = $doc_data; 
			$this->load->view('appointments/getDoctorTimingData',$res);
			 
		} 
			 public function getDoctorsByspecialitysessionBranches()
		{ 
		
		 	$branch_id = $this->session->userdata('sess_branch_id');
		 	$sp_id = $this->input->post("sp_id");
			 $doc_data = $this->MasterModel->getDoctorsByspecialityBranches($branch_id,$sp_id);
				$restdata['docData'] = $doc_data; 
				 
			
			$this->load->view('getDoctorsData',$restdata);
		} 
		
		public function addAppointment() {
		  	
			
			if(isset($_SESSION['sess_patient_id']))
			{
				$pid=$_SESSION['sess_patient_id'];
				$app_list = $this->AppointmentModel->getappointments();
				$data['appointment_data'] = $app_list;	
				
				$todaysList = $this->AppointmentModel->gettodaysappointments($pid);
				$data['appointment_todaysdata'] = $todaysList;
				$patient_list = $this->AppointmentModel->getPatientById($pid);
				$data['singlepatientData'] = $patient_list;	
				
			}
			$doc_list = $this->MasterModel->getdoctorsList();
			$data['docData'] = $doc_list;
		 	$pat_list = $this->PatientModel->getPatientsTodays();
			$data['todayspatientData'] = $pat_list;	
			
			$pat_list = $this->PatientModel->getPatients();
			$data['patientData'] = $pat_list;	
			//print_r($data['patientData']);
			$this->load->view('appointments/addAppointments',$data);
		}
		
		public function feesDetails() {
			//	$feesList = $this->AppointmentModel->getfeesDetails();
			//	$data['fees_data'] = $feesList;
			
			$this->load->view('appointments/feesappointments');
		}
		public function appointmentsDetails() {
		 	
			
			$doc_list = $this->MasterModel->getdoctorsList();
			$data['docData'] = $doc_list;
			$this->load->view('appointments/appointmentsDetails',$data);
		}
		public function addPrescription() {
			/* $pat_lists = $this->AppointmentModel->getappointmentsPatients($pid,$aid);
			$data['appoint_patientData'] = $pat_lists; 
			
			$patient_list = $this->AppointmentModel->getPatientById($pid);
			$data['singlepatientData'] = $patient_list;	 */

			$type_list = $this->MasterModel->getmedicineTypeList();
			$data['medtypeData'] = $type_list;
		$medlist = $this->MasterModel->getmedicineTypeList();
			$data['medicineData'] = $medlist;
			
			$this->load->view('appointments/addPrescription',$data);
		}
		public function get_medicines() {
			$contact_no = $this->input->post("query"); 
			echo json_encode($this->AppointmentModel->get_medicines($query));
 
		}
		public function getMRDData() {
			
			$mrdNo = $this->input->post("mrdNo");  
			$contact_no = $this->input->post("contact_no"); 
		   $profileDataList=$this->PatientModel->getMRDData($mrdNo,$contact_no);
		  $data['getpatientData'] = $profileDataList; 
		  $type_list = $this->MasterModel->getmedicineTypeList();
			$data['medtypeData'] = $type_list;
			$medlist = $this->MasterModel->getmedicineTypeList();
			$data['medicineData'] = $medlist;
			
		//	$medlist = $this->MasterModel->getmedicineList();
	//		$data['medicineData'] = $medlist;
		  $this->load->view('appointments/addPrescription', $data, FALSE);
	 }
		
	 public function save_prescription()
	 { 
		 $sess_branch_id =$this->session->userdata('sess_branch_id');
		
		 $branch_id = $sess_branch_id; 
		  $type_of_dm = $this->input->post("type_of_dm");
		 $complication_of_dm = $this->input->post("complication_of_dm");
		 $associated_illness = $this->input->post("associated_illness");
		 $present_complaints = $this->input->post("present_complaints");
		 $type_id = $this->input->post("type_id");
		 $medicine_name = $this->input->post("medicine_name");
		 $patient_id = $this->input->post("patient_id");
		 
		 $usage = $this->input->post("usage"); 
		 $dosage = $this->input->post('dosage');
		 $no_of_days = $this->input->post('no_of_days'); 
		 $advice = $this->input->post('advice'); 
		 $target = $this->input->post('target'); 
		 $no_of_days = $this->input->post('no_of_days');
$res = $this->AppointmentModel->insertPrescription($patient_id,$branch_id,$type_of_dm,$complication_of_dm,$associated_illness, $present_complaints,$type_id,$medicine_name,$usage,$dosage,$advice, $target,$prescription_id);
	  
 
	 
	 }
		
		public function get_PatientData() {
		 	
		 	  $start_date =$this->input->post("start_date");
			$type='patient';
			$doc_lists = $this->AppointmentModel->gettodaysappointmentsDoctors($start_date);
			$data['appoint_DocData'] = $doc_lists;
			 
		 	$spec_list = $this->MasterModel->getSpecialityList();
			$data['specalist'] = $spec_list;
			$predata = $this->MasterModel->getPrefixByType($type);
			$data['prefix'] = $predata;
			$this->load->view('appointments/get_patientDetails',$data);
		}
		
		
		
		public function save_appointment() {
			$appointment_id = $this->input->post("app_id");
			$reschdule_val = $this->input->post('reschdule_val');
			$reschdule = $this->input->post('reschdule');
			$doctor_id = $this->input->post('doctor_id');
			$app_date = $this->input->post('app_date');
			$app_time = $this->input->post('app_time'); 
			$remarks = $this->input->post('remarks'); 
			$patient_id = $this->input->post('patient_id'); 
			$app_date = date("Y-m-d" );
			 
			$cat_button='';
			if ($this->input->post('add_app_btn')) {
				$cat_button = 'add_app_btn';
				} else if ($this->input->post('update')) {
				$cat_button = 'update';
			}
			
			
			$res = $this->AppointmentModel->insertAppointment($doctor_id, $reschdule_val, $reschdule,$app_date,$app_time ,$appointment_id, $cat_button,$remarks,$patient_id);
		}	
		
		
		
		
		public function editappointment($id) {
			
			
			$data['edit_fees_data'] = $this->AppointmentModel->editappointment($id);
			$app_data = $this->AppointmentModel->getappointments();
			$data['appointments_data'] = $app_data;
			$this->load->view('appointments/editFees', $data, FALSE);
		}
		
		
		public function deleteappointment($id) {
			$t=$this->AppointmentModel->deleteappointment($id);
			
		}	
		
		//view appointmnt
		public function appointmentView($aid) {
			$rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			
			$patient_list = $this->AppointmentModel->getappointmentsPatients($aid);
			$data['singlepatientData'] = $patient_list;  
			
			$this->load->view('appointments/appointmentView',$data);
		}
		public function patientInvoice() {
		/*  $rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			$patient_list = $this->AppointmentModel->getInvoiceLastNo();
			$data['invoiceNumber'] = $patient_list;  
			 $docdata = $this->PatientModel->getDoctors();
			$data['doctorData'] = $docdata;	
			$rlist = $this->MasterModel->getInsuranceList();
			$data['insuranceData'] = $rlist; */
			$this->load->view('appointments/patientInvoice');//,$data);
		}
	 
		
		
		
		
		public function savePatientInvoice() {   
			$mrd_no = $this->input->post('mrd_no');  
			$invoice_date = $this->input->post('invoice_date');  
			$ref_by = $this->input->post('ref_by');  
			$visit_no = $this->input->post('visit_no');  
			$visit_date = $this->input->post('visit_date');  
			$insurance_no = $this->input->post('insurance_no');  
			$patient_id = $this->input->post('patient_id');  
			$doctor_id = $this->input->post('doc');  
			$insurance_id = $this->input->post('insurance_id');  
			$ref_no = $this->input->post('ref_no');  
			$insurance_deatials = $this->input->post('insurance_deatials');  
			$type_id = $this->input->post('type_id');  
			$speciality = $this->input->post('speciality');  
			$qty = $this->input->post('qty');  
			$rate = $this->input->post('rate');  
			$total = $this->input->post('total');  
			$discount_type = $this->input->post('discount_type');  
			$discount = $this->input->post('discount');  
			$discount_ref_no = $this->input->post('discount_ref_no');  
			$service_charges = $this->input->post('service_charges');  
			$final_total = $this->input->post('final_total');   
			$cash = $this->input->post('cash');  
			$credit = $this->input->post('credit');  
			$cc = $this->input->post('cc');  
			$credit = $this->input->post('credit');  
			$online_details = $this->input->post('online_details');  
			$credit_details = $this->input->post('credit_details');  
			$cc_details = $this->input->post('cc_details');  
			$cash_details = $this->input->post('cash_details');  
			$paid_total = $this->input->post('paid_total');  
			$unpaid_total = $this->input->post('unpaid_total');  
			$remarks = $this->input->post('remarks');   
			 
			$res = $this->AppointmentModel->insertPatientInvoice($mrd_no ,$invoice_date ,$ref_by ,$visit_no,$visit_date,$insurance_no,$patient_id,$doctor_id,$insurance_id,$ref_no,$insurance_deatials,$type_id,$speciality,$qty,$rate,$total,$discount_type,$discount,$discount_ref_no,$service_charges,$final_total,$cash,$cc,$credit,$online_details,$credit_details,$cc_details,$cash_details ,$paid_total,$unpaid_total,$remarks);
		}	
		public function invoiceDetails() 
		{ 
		//$patient_list = $this->AppointmentModel->getappointmentsPatients($aid);
			//$data['singlepatientData'] = $patient_list;  
			
			$this->load->view('appointments/invoiceDetails');
		}

		//online appointments
		public function doctorAppointments() {
		 $start_date = date("Y-m-d"); 
		 $admin_type =$this->session->userdata('admin_type');
		 if($admin_type=='doctor')
		 {
			$doc =$this->session->userdata('admin_id');
		 }
		 else{
		 $doc ='';
		 }
		 $type='patient';
		 $doc_lists = $this->AppointmentModel->online_gettodaysappointmentsDoctors($start_date,$doc);
		 $data['appoint_DocData'] = $doc_lists;
		 $pat_lists = $this->AppointmentModel->online_gettodaysappointmentsPatients($start_date,$doc);
		 $data['appoint_patientData'] = $pat_lists;
		  $docdata = $this->PatientModel->getDoctors();
			$data['doctorData'] = $docdata;	
		  $spec_list = $this->MasterModel->getSpecialityList();
		 $data['specalist'] = $spec_list;
		 $predata = $this->MasterModel->getPrefixByType($type);
		 $data['prefix'] = $predata;
		 $this->load->view('appointments/doctorAppointments',$data);
	 }
		public function getdoctorAppointments() {
		 	 
		  	$start_date = $this->input->post("start_date"); 
		  	$doc = $this->input->post("doc"); 
			
		 $type='patient';
		 $doc_lists = $this->AppointmentModel->online_gettodaysappointmentsDoctors($start_date,$doc);
		 $data['appoint_DocData'] = $doc_lists;
		 $pat_lists = $this->AppointmentModel->online_gettodaysappointmentsPatients($start_date,$doc);
		 $data['appoint_patientData'] = $pat_lists;
		  $docdata = $this->PatientModel->getDoctors();
			$data['doctorData'] = $docdata;	
		  $spec_list = $this->MasterModel->getSpecialityList();
		 $data['specalist'] = $spec_list;
		 $predata = $this->MasterModel->getPrefixByType($type);
		 $data['prefix'] = $predata;
		 $this->load->view('appointments/doctorAppointments',$data);
	 }
	 public function saveOnlineAppointment() {
		$appointment_id = $this->input->post("app_id"); 
	  	$app_date = $this->input->post('app_date');
		$app_time = $this->input->post('app_time'); 
		$remarks = $this->input->post('remarks'); 
		$patient_id = $this->input->post('patient_id');  
		$cat_button='';
		if ($this->input->post('add_app_btn')) {
			$cat_button = 'add_app_btn';
			} else if ($this->input->post('update')) {
			$cat_button = 'update';
		}
		
		
		$res = $this->AppointmentModel->insertOnlineAppointment($app_date,$app_time ,$appointment_id, $cat_button,$remarks,$patient_id);
	}	

	public function joinMeeting($meeting)
	{
			$data['meeting']=$meeting;
		$this->load->view('appointments/joinMeeting',$data);
	}	
	public function finish_meeting($meeting)
	{ 
	 		 $doc_lists = $this->AppointmentModel->finishMeeting($meeting); 
	}
	
	}
	
	
	
?>
