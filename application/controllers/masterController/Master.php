<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once APPPATH."controllers/Secure_area.php";
	
	class Master extends Secure_area 
	{
		
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			// $this->load->database();
			$this->db2=$this->load->database('dynamicdb', TRUE);  
		 	$this->load->model('MasterModel');  
			$this->load->model('PatientModel');  
		 
		/*	$this->load->helper('url');
			$this->load->library('parser'); */
		}
		
		public function index() {
		}
		
		/* public function myformAjax() { 	 			
			$reg_no = $this->input->post("reg_no"); 
			$masterlist = $this->MasterModel->getbranchesbyID($reg_no);
			$data['branch_list'] = $masterlist; 
			$this->load->view('getbranchData', $data);
			
		} */
		
		
		public function branchPrefix() {
			$branchList = $this->MasterModel->getbranches();
			$data['branchList'] = $branchList;	
			$preList = $this->MasterModel->getPrefixDetails();
			$data['prefixData'] = $preList;	
			
			$this->load->view('master/prefixMaster',$data);
		}
		
		public function savePrefix() {  
			$clinic_type = $this->input->post("clinic_type"); 
			$branch_id = $this->input->post("branch_id"); 
			$prefix1 = $this->input->post("prefix1"); 
			$branch_id = $this->input->post("branch_id"); 
			$branch_code = $this->input->post("branch_code"); 
			$year = $this->input->post("year"); 
			$start_from = $this->input->post("start_from");  
			$prefix_id = $this->input->post('prefix_id'); 
			$cat_button='';
			if ($this->input->post('add_pre_btn')) {
				$cat_button = 'add_pre_btn';
				} else if ($this->input->post('update_pre_btn')) {
				$cat_button = 'update_pre_btn';
			}
			$res = $this->MasterModel->insertPrefix($clinic_type, $branch_id, $prefix1, $branch_code, $year, $start_from, $prefix_id, $cat_button);
		}
		public function addSpeciality() {
			
			$spec_list = $this->MasterModel->getSpecialityList();
			$data['specalist'] = $spec_list;	
			
			$this->load->view('master/specialtyMaster',$data);
		}
		public function saveSpecialityRegistration() {
		  	$speciality_name = $this->input->post("speciality_name"); 
		  	$spec_id = $this->input->post('spec_id'); 
			$cat_button='';
			if ($this->input->post('add_spec_btn')) {
				$cat_button = 'add_spec_btn';
				} else if ($this->input->post('update_spec_btn')) {
				$cat_button = 'update_spec_btn';
			}
			$res = $this->MasterModel->insertSpecialityRegistration($speciality_name, $spec_id, $cat_button);
		}
		
		public function getBranch() {
			$branch_id = $this->input->post("branch_id"); 
			$sess_clinic_id = $_SESSION("sess_clinic_id"); 
			$admin_id = $_SESSION("admin_id"); 
			$admin_type = $_SESSION("admin_type"); 
			
			$res = $this->MasterModel->check_branch($branch_id, $sess_clinic_id, $admin_id, $admin_type);
		}
		public function editSpeciality($id) {
			$spec_list = $this->MasterModel->getSpecialityList();
			$data['specalist'] = $spec_list; 
			$data['edit_data'] = $this->MasterModel->editSpeciality($id);
			$this->load->view('master/specialtyMaster', $data, FALSE);
		}
		
		
		public function deleteSpeciality($id) {
			$t=$this->MasterModel->deleteSpeciality($id);
			$spec_list = $this->MasterModel->getSpecialityList();
			$data['specalist'] = $spec_list;
			
			$this->load->view('master/specialtyMaster',$data);
		}	
		//disaease
	 
		
		//Role
		public function addRole() {
			$rlist = $this->MasterModel->getroleList();
			$data['roleList'] = $rlist;
			
			$this->load->view('master/departmentMaster',$data);
		}
		
		public function saveRoleRegistration() {
			$role_name = $this->input->post("role_name"); 
			
			$role_id = $this->input->post('role_id'); 
			$cat_button='';
			if ($this->input->post('add_role_btn')) {
				$cat_button = 'add_role_btn';
				} else if ($this->input->post('update_role_btn')) {
				$cat_button = 'update_role_btn';
			}
			
			
			$res = $this->MasterModel->insertRoleRegistration($role_name, $role_id, $cat_button);
		}	
		
		public function editRole($id) {
			
			$data['edit_data'] = $this->MasterModel->editRole($id); 
			$rlist = $this->MasterModel->getroleList();
			$data['roleList'] = $rlist;		
			$this->load->view('master/departmentMaster', $data, FALSE);
		}
		
		
		public function deleteRole($id) {
			$t=$this->MasterModel->deleteRole($id);
			$rlist = $this->MasterModel->getroleList();
			$data['roleList'] = $rlist;		
			$this->load->view('master/departmentMaster',$data);
		}	 
		//feesDetails
		public function feesDetails() {
			$feesList = $this->MasterModel->getfeesDetails();
			$data['fees_data'] = $feesList;
			
			$this->load->view('master/feesMaster',$data);
		}
		
		public function save_Feepackage() {
			$service_name = $this->input->post("service_name");
			$amount = $this->input->post('amount');
			
			$fees_id = $this->input->post('fees_id'); 
			$cat_button='';
			if ($this->input->post('add')) {
				$cat_button = 'add';
				} else if ($this->input->post('update')) {
				$cat_button = 'update';
			}
			
			
			$res = $this->MasterModel->insertFeepackage($service_name, $amount, $fees_id, $cat_button);
		}	
		
		
		
		
		public function editfee($id) {
			
			
			$data['edit_fees_data'] = $this->MasterModel->editfee($id);
			$dis_data = $this->MasterModel->getfeesDetails();
			$data['fees_data'] = $dis_data;
			$this->load->view('master/editFees', $data, FALSE);
		}
		
		
		public function deletefee($id) {
			$t=$this->MasterModel->deletefee($id);
			$feesList = $this->MasterModel->getfeesDetails();
			$data['fees_data'] = $feesList;
			
			$this->load->view('master/feesMaster',$data);
		}	
		
		
		
		
		
		
		//notifiacvtions
		public function notifications() {
			$notify_data = $this->MasterModel->getorders_notify();
			$data['notify_list'] = $notify_data;
			//print_R($data['notify_list'] );
			$this->load->view('all_notifications',$data);
			
			
		}	
		public function notifications_count() {
			$notify_data = $this->MasterModel->getorders_notify();
			$data['notify_list'] = $notify_data;
			echo count($data['notify_list']);
			
		}
		
		//service types
		public function serviceTypes() {
			$rlist = $this->MasterModel->serviceTypesList();
		$data['serviceTypeList'] = $rlist;
			
			$this->load->view('master/serviceTypes',$data);
		}
		
		public function saveserviceTypes() {
			$service_type = $this->input->post("service_type");
			$service_type_code = $this->input->post('service_type_code');
			
			$service_type_id = $this->input->post('service_type_id'); 
			$cat_button='';
			if ($this->input->post('add_type_btn')) {
				$cat_button = 'add_type_btn';
				} else if ($this->input->post('update_type_btn')) {
				$cat_button = 'update_type_btn';
			}
			
			$res = $this->MasterModel->insertServiceType($service_type, $service_type_code, $service_type_id, $cat_button);
		}	
		
		public function editserviceTypes($id) {
			$editdata = $this->MasterModel->editserviceTypes($id);
			$rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			$data['serviceTypes'] = $editdata;
			$this->load->view('master/serviceTypes', $data, FALSE);
		}
		
		
		public function deleteserviceTypes($id) {
			$t=$this->MasterModel->deleteserviceTypes($id);
			$rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;			
			$this->load->view('master/serviceTypes',$data);
		}		
		
		
		
		//servicesDetails
		public function servicesDetails() {
			 $slist = $this->MasterModel->servicesList();
			$data['servicesList'] = $slist;
		  	$rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			
			$masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist; 
			$data['model_obj'] = $this->PatientModel; 
			$this->load->view('master/servicesDetails',$data);
		}
		
		public function saveservices() { 
			$service_name = $this->input->post('service_name');
			$branches = $this->input->post('branches');
			$branch_charges = $this->input->post('branch_charges');
			$type_id = $this->input->post('type_id');
			$service_code = $this->input->post('service_code'); 
			$description = $this->input->post('description');
			$notes = $this->input->post('notes');
			$remarks = $this->input->post('remarks');
			
			$unit = $this->input->post('unit');
			$multiply = $this->input->post('multiply');
			$male_range = $this->input->post('male_range');
			$female_range = $this->input->post('female_range');
			$kid_range = $this->input->post('kid_range');
			
			$service_id = $this->input->post('service_id'); 
			$cat_button='';
			if ($this->input->post('add_ser_btn')) {
				$cat_button = 'add_ser_btn';
				} else if ($this->input->post('update_ser_btn')) {
				$cat_button = 'update_ser_btn';
			}
			$res = $this->MasterModel->insertServices($branches, $branch_charges,$service_name, $type_id,$service_code,$description,$notes,$remarks, $service_id, $cat_button, $unit, $multiply, $male_range, $female_range, $kid_range);
		}	
		
		public function editservices($id) {
			
			
			$data['editserviceData'] = $this->MasterModel->editservices($id);
			$slist = $this->MasterModel->servicesList();
			$data['servicesList'] = $slist;
			$rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			
			$masterlist = $this->MasterModel->getbranches();
			$data['branch_list'] = $masterlist;
			$data['model_obj'] = $this->PatientModel; 
			$this->load->view('master/servicesDetails', $data, FALSE);
		}
		public function getservicesById() {
			
			$type_id = $this->input->post('type_id');
			
			$rlist = $this->MasterModel->getservicesById($type_id);
			$data['editserviceData'] = $rlist;
		  	$this->load->view('master/getservices', $data);
		}
		
		
		public function deleteservices($id) {
			$this->MasterModel->deleteservices($id);  
		}	
		
		//Timing doctors
		public function timingDetails() {
			
		 
			 $docdata = $this->PatientModel->getDoctors();
			$restdata['doctorData'] = $docdata;	
			
			$sp_list = $this->MasterModel->getSpecialityList();
			$restdata['spec_data'] = $sp_list;  
			$timelist = $this->MasterModel->gettimingDetails();
			$restdata['timing_Data'] = $timelist;   
			$this->load->view('master/timingMaster',$restdata);
		}
		public function saveTiming() {
			$from_time = $this->input->post("from_time");
			$day = $this->input->post('day');
			$to_time = $this->input->post('to_time');
			$doc = $this->input->post('doc');
			$clinic = $this->input->post('clinic');
			
		    $branch_id = $_SESSION['sess_branch_id']; 
		 	$timing_id = $this->input->post('timing_id'); 
		 	$master_timingval = $this->input->post('master_timingval'); 
			$cat_button='';
			if ($timing_id=='') {
				$cat_button = 'add_time';
				} else  {
				$cat_button = 'update';
			}
			
			if($master_timingval!='master')
			{
				
				echo $res = $this->MasterModel->insertTiming($from_time, $to_time,$day,$doc,$clinic,$branch_id, $timing_id, $cat_button);
				}else{
				echo $res = $this->MasterModel->insertMasterTiming($from_time, $to_time,$day,$doc,$clinic,$branch_id, $timing_id, $cat_button);
			}
		}	 
		
		
		public function editTiming($id,$day) {
			
			
			$restdata['edit_timingdata'] = $this->MasterModel->editTiming($id,$day);
			/* $sp_list = $this->MasterModel->getSpecialityList();
			$restdata['spec_data'] = $sp_list;   */
			$docdata = $this->PatientModel->getDoctors();
			$restdata['doctorData'] = $docdata;	
			
			$timelist = $this->MasterModel->gettimingDetails();
			$restdata['timing_Data'] = $timelist;  
			$this->load->view('master/timingMaster',$restdata);
		}
		
		
		public function deletetiming($id) {
			$t=$this->MasterModel->deletetiming($id);
			$timingList = $this->MasterModel->getTiming();
			$data['timing_Data'] = $timingList;
			
			$this->load->view('master/feesMaster',$data);
		}
		
		public function get_services_charges() { 	 			
		 	$service_id = $this->input->post("service_id");  
		 echo	$masterlist = $this->MasterModel->get_services_charges($service_id);
			
		} 
		public function get_consultant_charges() { 	 			
		 	$doc_id = $this->input->post("doc_id");  
			echo $this->MasterModel->get_consultant_charges($doc_id);
			
		} 
		
		/* public function ajax_delete() { 	 			
		 	$deleteid = $this->input->post("deleteid"); 
		 	$dval = $this->input->post("dval"); 
			echo	$masterlist = $this->MasterModel->deleteData($deleteid,$dval);
		} 
		*/
		
		public function getcityData() { 	 			
			$state = $this->input->post("state");  
				$masterlist = $this->MasterModel->getcityListByID($state);
				$data['cityData'] = $masterlist; 
					$this->load->view('patient/getCityData',$data);
	   } 

		//insurance Detailks
		public function insuranceDetails() {
			 $rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			$rlist = $this->MasterModel->getInsuranceList();
			$data['InsuranceList'] = $rlist; 
			$data['model_obj'] =$this->MasterModel;
			$this->load->view('master/insuranceDetails',$data);
		}	
		
		public function saveinsurance() {
			$insurance_name = $this->input->post("insurance_name");
			$insurance_provider = $this->input->post('insurance_provider');
			$type_id = $this->input->post('type_id');
			$speciality = $this->input->post('service_name');
			$service_charges = $this->input->post('service_charges'); 
		    $remarks = $this->input->post('remarks');  
		 	$insurance_id = $this->input->post('insurance_id'); 
			$cat_button='';
			if ($insurance_id=='') {
				$cat_button = 'add_btn';
				} else  {
				$cat_button = 'update_btn';
			}
			
			
			
			echo $res = $this->MasterModel->insertInsurance($insurance_name, $insurance_provider,$type_id,$speciality,$service_charges,$remarks, $insurance_id, $cat_button);
			
		}	
			public function editInsurance($id) {
			$data['edit_insuranceData']= $this->MasterModel->editInsurance($id);
			$rlist = $this->MasterModel->serviceTypesList();
			$data['serviceTypeList'] = $rlist;
			$rlist = $this->MasterModel->getInsuranceList();
			$data['InsuranceList'] = $rlist; 
			$data['model_obj'] =$this->MasterModel;
			$this->load->view('master/insuranceDetails',$data);
		}	
		
		
	}
	
?>
